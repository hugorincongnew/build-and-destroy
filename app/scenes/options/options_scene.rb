class OptionsScene < MG::Scene
  include SceneMethods
  include MotionSocial::Sharing
  
  def initialize
    @buttons = []    
    GameHelper.add_background("main-bg.png", self)
    @buttons << GameHelper.add_back_button_to("StartScene-Clean", self)
    GameHelper.add_title("Options", self)

    add_options

    on_touch_begin do |touch|
      check_menu(touch)
    end    
  end

  def add_options
    options = [
      [25.5, "volume.png", 'Volume', 'set_volume-Call'], #volume
      [38, "invitefriends.png", "Invite friends on Facebook", 'invite_fb-Call'],
      [50.5, "rules.png", "Rules of game", 'RulesScene'],
      [63, "faq.png", "FAQ", 'FaqScene'],
      [75.5, "support.png", "Support", 'support-Call'],
      [88, "terms.png", "Terms", 'TermsScene']                       
    ]
    options.each{|o| add_option(o); }
  end  
  
  
  def add_option(options)
    volume_icon = MG::Sprite.new(ImageHelper.name_for_phone(options[1]))
    volume_icon.position = [ImageHelper.left_porcent(16), ImageHelper.top_porcent(options[0])]
    volume_icon.scale = 1.1
    add volume_icon, 1    
    
    title = MG::Text.new(options[2], 'OpenSans-Regular', font_size)
    title.position = [option_left, ImageHelper.top_porcent(options[0] - 0.5)]
    title.area_size = [ImageHelper.left_porcent(80), 40]   
    title.horizontal_align = :left        
    title.text_color = [1.0,1.0,1.0,1.0]
    add title, 1
    
    if options[0] < 88
      sep_line = MG::Draw.new
      sep_line.line([0, ImageHelper.top_porcent(options[0] + 5.5)], [ImageHelper.left_porcent(100), ImageHelper.top_porcent(options[0] + 5.5)], 2.0, [0.0, 0.0, 0.0, 0.1])
      add sep_line, 1
    end
    
    if options[3]
      #TODO add volumne switch
    end  
    
    add_action_for(options)
  end 
  
  def option_left
    if $iphone_ver == "iphone5" or $iphone_ver == "iphone4"
      ($visible_size.width / 2) + ImageHelper.left_porcent(18)
    else
      ($visible_size.width / 2) + ImageHelper.left_porcent(16)
    end      
  end
  
  def font_size
    if $iphone_ver == "iphone5" or $iphone_ver == "iphone4"
      30
    else
      42
    end    
  end  

  def set_volume
    SoundHelper.toogle_volumen
    $music.volume = SoundHelper.default_volume
  end  
  
  def support
    if MFMailComposeViewController.canSendMail() 
      NSLog("Sending email!")
      composeVC = MFMailComposeViewController.alloc.init
      composeVC.mailComposeDelegate = $root
      
      composeVC.setToRecipients(["sweetshopgames16@gmail.com"])
      composeVC.setSubject("BuildAndDestroy Question")
      composeVC.setMessageBody("Your Question Here!", isHTML:false)

      $root.presentViewController(composeVC, animated:true, completion:nil)
    else
      NSLog("Can't send email!")
    end  
    NSLog("click!")
  end  
  
  def add_action_for(option)
    
    
    action_button = MG::Text.new(' ', 'OpenSans-Bold', 29)
    action_button.area_size = [$visible_size.width, 50]
    action_button.position = [$visible_size.width / 2, ImageHelper.top_porcent(option[0])]
    add action_button, 3
    @buttons << [action_button, option[3], self]
  end    
  
  
  #Facebook
  
  def invite_fb
    post_to_facebook
  end     
  
  def sharing_message
    "I'm playing Build N Destroy, let's play, download!"
  end

  def sharing_url
    "http://www.softwarecriollo.com"
  end

  def sharing_image
    UIImage.imageNamed("Icon-60@3x.png")
  end

  def controller
    $root
  end

    

end