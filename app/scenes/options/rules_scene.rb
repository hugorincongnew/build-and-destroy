class RulesScene < MG::Scene
  include SceneMethods
  
  def initialize
    @top_val = 1650 
    @buttons = []

    GameHelper.add_background("main-bg.png", self)
    @scroll2 = UIScrollView.alloc.initWithFrame(CGRectMake(0, 50, $main_view.frame.size.width, $main_view.frame.size.height))
    @scroll2.contentSize = [$main_view.frame.size.width, image_height]
    $main_view.addSubview(@scroll2)
    $main_view_objs << @scroll2
        
    @back = MG::Button.new("<") 
    @back.font = 'OpenSans-Regular'
    @back.font_size = 60
    @back.position = [ImageHelper.left_porcent(6) + @back.size.width, ImageHelper.top_porcent_for_height(5, $visible_size.height)]
    @back.text_color = [1.0, 1.0, 1.0]
    @back.on_touch do |touch|
      TouchHelper.touch_action(["", "OptionsScene-Clean"])
    end    

    add @back, 5        
    add_main_items
    
  end
  
  def image_height
    if $iphone_ver == "iphone6plus"
      base1 = 1242
      base = 6000
    elsif $iphone_ver == "iphone6"
      base1 = 750      
      base = 3623
    else
      base1 = 640      
      base = 3092   
    end  
    ($main_view.frame.size.width * base) / base1
  end
  
  def add_main_items

    imagev = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, $main_view.frame.size.width, image_height))
    imagev.image = UIImage.imageNamed(ImageHelper.name_for_phone("rulesbigbg.png"))
    
    @scroll2.addSubview(imagev) 
 
  end  

end