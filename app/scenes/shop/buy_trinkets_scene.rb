class BuyTrinketsScene < MG::Scene
  include SceneMethods
  
  def initialize
    @buttons = []    
    @shop = Shop.new
    
    GameHelper.add_background("main-bg.png", self)
    @buttons << GameHelper.add_back_button_to("ShopScene-Clean", self)

    @scroll2 = UIScrollView.alloc.initWithFrame(CGRectMake(0, 120, $main_view.frame.size.width, $main_view.frame.size.height))
    @scroll2.contentSize = [$main_view.frame.size.width, (@shop.trinkets.size * (cal_height + 20))]
    $main_view.addSubview(@scroll2)
    $main_view_objs << @scroll2

    add_trinkets

    on_touch_begin do |touch|
      check_menu(touch)
    end        
  end
  
  def add_trinkets
    @shop.trinkets.each_with_index do |t, i|
      button = UIButton.buttonWithType(UIButtonTypeCustom)
      button.frame = CGRectMake(0, (i * (cal_height + 20)), $main_view.frame.size.width, cal_height)
      button.setImage(UIImage.imageNamed(t.image), forState: UIControlStateNormal)
      button.when(UIControlEventTouchUpInside) do
        buy_trinket(t)
      end
      @scroll2.addSubview(button) 
    end  
  end  
  
  def buy_trinket(trinket)
    
  end  

  def cal_height
    ($main_view.frame.size.width * 231) / 1242
  end  

end