class BuyCoinsScene < MG::Scene
  include SceneMethods
  
  def initialize
    @buttons = []    
    GameHelper.add_background("main-bg.png", self)
    GameHelper.add_title("Buy Coins", self)
    
    add_main_items

    on_touch_begin do |touch|
      check_menu(touch)
    end        
    
    #TODO remove this is just for testing
    App::Persistence["#{$gcm.local_player_id}-coins"] = 50000 
    GameHelper.alert("Current Coins", "You currently own #{App::Persistence["#{$gcm.local_player_id}-coins"]} coins.") 
  end
  
  def add_main_items
    add_tags
    add_buy_button
  end  
  
  def add_tags
    tag1 = MG::Sprite.new(ImageHelper.name_for_phone("500k.png"))
    tag1.position = [ImageHelper.left_porcent(50), ImageHelper.top_porcent(21.5) - (tag1.size.height / 2)]
    add tag1, 1    
    
    tag2 = MG::Sprite.new(ImageHelper.name_for_phone("pay.png"))
    tag2.position = [ImageHelper.left_porcent(50), ImageHelper.top_porcent(40.2) - (tag2.size.height / 2)]
    add tag2, 1      
  end  
  
  def add_buy_button
    @buy_button = MG::Sprite.new(ImageHelper.name_for_phone("buy_btn.png"))
    @buy_button.position = [ImageHelper.left_porcent(0) + (@buy_button.size.width / 2), ImageHelper.top_porcent(100) + (@buy_button.size.height / 2)]
    add @buy_button, 1 
    
    @action_button = MG::Text.new(' ', 'OpenSans-Bold', 29)
    @action_button.area_size = [@buy_button.size.width, @buy_button.size.height]
    @action_button.position = [@buy_button.position.x, @buy_button.position.y]
    add @action_button, 3
    @buttons = []
    @buttons << GameHelper.add_back_button_to("ShopScene-Clean", self)
    @buttons << [@action_button, 'buy_coins-Call', self]     
  end  
  
  def buy_coins
    NSLog("Buy Coins!")
    @helu = Helu.new("500KC")
    @helu.fail = lambda { |transaction| fail_transaction(transaction)}
    @helu.winning = lambda { |transaction| success_transaction(transaction)}
    @helu.buy
    remove_btn
  end  
  
  def remove_btn
    @buy_button.delete_from_parent
    @action_button.delete_from_parent
  end  
  
  def fail_transaction(transaction)
    NSLog("Buy Coins Fail!")
    mp transaction.error.code
    mp transaction.error.userInfo    
    @helu.close
    GameHelper.alert("Error", transaction.error.userInfo["NSLocalizedDescription"])
    add_buy_button
  end
  
  def success_transaction(transaction)
    NSLog("Buy Coins Success!")
    mp transaction.inspect
    @helu.close
    App::Persistence["#{$gcm.local_player_id}-coins"] += 500000 
    GameHelper.alert("Buy Complete", "Your buy was success, you have now #{App::Persistence["#{$gcm.local_player_id}-coins"]} coins.") 
    add_buy_button  
  end    

end