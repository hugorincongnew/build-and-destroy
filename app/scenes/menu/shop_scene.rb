class ShopScene < MG::Scene
  include SceneMethods
  
  def initialize
    @buttons = []    
    GameHelper.add_background("main-bg.png", self)
    @buttons << GameHelper.add_back_button_to("StartScene-Clean", self)
    GameHelper.add_title("Shop", self)
    add_main_items

    on_touch_begin do |touch|
      check_menu(touch)
    end        
  end
  
  def add_main_items
    add_buy_coins_btn
    add_buy_trinkets_btn
  end  
  
  def add_buy_coins_btn
    buy_coins_btn = MG::Sprite.new(ImageHelper.name_for_phone("buy_coins.png"))
    buy_coins_btn.position = [ImageHelper.left_porcent(50), ImageHelper.top_porcent(21.7) - (buy_coins_btn.size.height / 2)]
    add buy_coins_btn, 1    
    
    action_button = MG::Text.new(' ', 'OpenSans-Bold', 29)
    action_button.area_size = [buy_coins_btn.size.width, buy_coins_btn.size.height]
    action_button.position = [buy_coins_btn.position.x, buy_coins_btn.position.y]
    add action_button, 3
    
    @buttons << [action_button, 'BuyCoinsScene']    
  end
  
  def add_buy_trinkets_btn
    buy_trinkets_btn = MG::Sprite.new(ImageHelper.name_for_phone("buy_trinket.png"))
    buy_trinkets_btn.position = [ImageHelper.left_porcent(50), ImageHelper.top_porcent(62.22) - (buy_trinkets_btn.size.height / 2)]
    add buy_trinkets_btn, 1  
    
    action_button = MG::Text.new(' ', 'OpenSans-Bold', 29)
    action_button.area_size = [buy_trinkets_btn.size.width, buy_trinkets_btn.size.height]
    action_button.position = [buy_trinkets_btn.position.x, buy_trinkets_btn.position.y]
    add action_button, 3
    
    @buttons << [action_button, 'BuyTrinketsScene']     
  end  

end