class OpponentScene < MG::Scene
  include SceneMethods
  
  def initialize
    @buttons = []
    GameHelper.add_background("main-bg.png", self)
    @buttons << GameHelper.add_back_button_to("StartScene-Clean", self)
    @screen_width = $main_view.frame.size.width
    @screen_height = $main_view.frame.size.height
    add_oponnets_selection
    add_labels

    on_touch_begin do |touch|
      check_menu(touch)
    end
  end
  
  def add_oponnets_selection
    add_vs_human
    add_vs_computer
  end  
  
  def add_vs_human
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 30), ImageHelper.porcent_for(@screen_height, 17), ImageHelper.porcent_for(@screen_width, 40), ImageHelper.porcent_for(@screen_width, 40)))
    image_container.image = UIImage.imageNamed(ImageHelper.name_for_phone("user.png"))
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = 25
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    @buttons << [image_container, "prepare_match-Call", self]  
  end 
  
  def prepare_match
    $gcm.findMatch(self)
  end  
  
  def add_vs_computer
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 30), ImageHelper.porcent_for(@screen_height, 59), ImageHelper.porcent_for(@screen_width, 40), ImageHelper.porcent_for(@screen_width, 40)))
    image_container.image = UIImage.imageNamed(ImageHelper.name_for_phone("computer.png"))
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = 25
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    @buttons << [image_container, "VsComputerLoadingScene-Clean", true]  
  end   
  
  def add_labels
    add_title
    add_line
    add_user_label
    add_computer_label
  end  
  
  def add_title
    title = MG::Text.new('Select your opponent', 'OpenSans-Regular', 48)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(6.7)]
    title.text_color = [0,0,0,1.0]
    add title
  end  

  def add_line
    line = MG::Draw.new
    line.line([ImageHelper.left_porcent(27.4), ImageHelper.top_porcent(51.5)], [ImageHelper.left_porcent(72), ImageHelper.top_porcent(51.5)], 2.0, [1.0, 1.0, 1.0, 1.0])
    add line
  end  

  def add_user_label
    title = MG::Text.new('USER', 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(42)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
  end
  
  def add_computer_label
    title = MG::Text.new('COMPUTER', 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(84)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
  end    

end