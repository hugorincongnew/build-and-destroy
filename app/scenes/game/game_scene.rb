class GameScene < MG::Scene
  include SceneMethods  
  include GameMethods
  
  #hightlight_position(index)
  
  def initialize
    $n_round = 1
    GameHelper.add_background("main-bg.png", self)
    $time_out = 120 # in seconds
    $game_scene = self
    $s_trinket = false
    initial_vars
    init_game_vars
    add_board_main_items
    add_board_positions
    show_starting_player
    add_exit_btn

    on_touch_begin do |touch| 
      $new_action = false
      check_menu_new_action(touch) 
      check_menu_new_action2(touch) if @menu_visible
      check_menu_new_action3(touch) if $grupo_visible
      if $new_action != true
        check_menu_layout(touch)
        check_menu2(touch)
        if $turn and $showing_message == false 
          check_menu(touch)
          @card_t = check_cards_touch(touch) 
          @active = @card_t if @card_t
          @card_t2 = check_cards_touch_board_pos(touch)
          check_menu_action(touch) if $amenu
        end
      end      
    end     
    
    on_touch_move do |touch|
      if $turn and $amenu == [] and $showing_message == false
        @card_t[0].z_index = 10 if @card_t
        GameHelper.move_card_to(@card_t, touch) if @card_t and is_on_hand_or_board?(@card_t)
      end    
    end    
    
    on_touch_end do |touch|      
      
      mp "touch end"
      
      if @card_t2 and $turn and $amenu == [] and $showing_message == false and $new_action == false
        ptouch = check_cards_touch_board_pos(touch)
        if ptouch
          index =  $board_positions.find_index(ptouch)
          hightlight_position(index) if $positions_cards[index].count > 0  
        end  
        #check_available_builds(@card_t[2])
      end  
      
      $new_action = true if $new_action == false
      
      
      if @card_t and $turn and $amenu == [] and $showing_message == false
        clean_action_menu_move
        if is_on_hand_or_board?(@card_t)
          ptouch = check_cards_touch_board_pos(touch)
          if ptouch
            show_actions(@card_t, ptouch, self)
          else
            card = @card_t[0]
            card.move_to(original_position_of(card), 0.5) 
          end   
        end   
      end  
              
    end
    
    if !$turn
      computer_move
      if $versus == "user"
        restart_timer
      end  
    end  
    
    if $turn
      if $versus == "user"
        EM.cancel_timer($timeout_timer) if $timeout_timer
      end
    else
      restart_timer
    end  
    
    start_update 
  end
  
  def restart_timer
    mp "NEW TIMER!!"
    EM.cancel_timer($timeout_timer) if $timeout_timer
    if $versus == "user" and !$turn
      $timeout_timer = EM.add_timer($time_out) do
        opponent_leave_time_out
      end  
    end  
  end  
  
  def exit_message_time_out
    GameHelper.alert("Alert", "You've been disconnected for inactivity, the game is over.")
    $gcm.disconnect
    EM.cancel_timer($timeout_timer) if $timeout_timer     
    $message_image_view_back2.removeFromSuperview if $message_image_view_back2
    $main_view_objs.each{|o| o.removeFromSuperview}
      
    $director.replace(StartScene.new) 
  end  
  
  def opponent_leave_time_out
    GameHelper.alert("Alert", "Your opponent has left the game, The computer is taking his place!")
    replace_computer_base_elements
    if $versus == "user" 
      EM.cancel_timer($timeout_timer) if $timeout_timer
      $gcm.send_data_to_opponent("timeout:::true")
      computer_take_control
    end 

    #$message_image_view_back2.removeFromSuperview if $message_image_view_back2
    #$main_view_objs.each{|o| o.removeFromSuperview}
    #$director.replace(StartScene.new)    
  end  
  
  def replace_computer_base_elements
    $main_view_objs.delete($oppimage_container)
    $oppimage_container.removeFromSuperview
    
    @title3o.text = "COMPUTER"
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 5), ImageHelper.porcent_for(@screen_height, 8.6), ImageHelper.porcent_for(@screen_width, 11.5), ImageHelper.porcent_for(@screen_width, 11.5)))
    image_container.image = UIImage.imageNamed("computer-iphone6.png")
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = image_container.frame.size.width / 2
    $main_view.addSubview(image_container)
    $main_view_objs << image_container      
  end  
  
  def computer_take_control
    $versus = "compu"
    computer_move
  end  
  
  def add_exit_btn

    image_container = UIImageView.alloc.initWithFrame(CGRectMake(@screen_width * 0.90, 10, 30, 30))
    image_container.image = UIImage.imageNamed("cancel-iphone6plus.png")
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    @buttons_new_action << [image_container, "show_exit_menu-Call", self]
    
  end  
  
  def show_exit_menu
    $message_image_view_back2 = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2.image = UIImage.imageNamed(ImageHelper.name_for_phone("all_shadow.png"))            
    
    if $iphone_ver == "iphone4" 
      label = UILabel.alloc.initWithFrame(CGRectMake(0, 70, @screen_width, 50))
      label.setText("Are you sure you want to leave the game?")
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)      
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:13.0))  
    elsif $iphone_ver == "iphone5"
      label = UILabel.alloc.initWithFrame(CGRectMake(0, 90, @screen_width, 50))
      label.setText("Are you sure you want to leave the game?")
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)      
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:13.0))
    else
      label = UILabel.alloc.initWithFrame(CGRectMake(0, 100, @screen_width, 50))
      label.setText("Are you sure you want to leave the game?")
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)      
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:16.0))
    end    
        
    
    @yes_b = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, 50, 50))
    @yes_b.image = UIImage.imageNamed("yes.png") 
    @yes_b.position = [($main_view.frame.size.width * 0.70), ($main_view.frame.size.height * 0.30)]
   
    @no_b = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, 50, 50))
    @no_b.image = UIImage.imageNamed("no.png") 
    @no_b.position = [($main_view.frame.size.width * 0.30), ($main_view.frame.size.height * 0.30)]
       
    @buttons_new_action3 << [@yes_b, "exit_game-Call", self]       
    @buttons_new_action3 << [@no_b, "cancel_exit-Call", self]      
    
    items = [$message_image_back2, @yes_b, @no_b, label].flatten 
    items.each{|i| $message_image_view_back2.addSubview(i)}         
               
    $main_view.addSubview($message_image_view_back2) 
    $grupo_visible = true  
  end  
  
  def cancel_exit
    $grupo_visible = false
    $message_image_view_back2.removeFromSuperview if $message_image_view_back2
  end  
  
  def exit_game(send_info=true)
    if $versus == "user" and send_info
      $gcm.send_data_to_opponent("leave:::true")
    end 
    
    $grupo_visible = false
    $message_image_view_back2.removeFromSuperview if $message_image_view_back2
    $main_view_objs.each{|o| o.removeFromSuperview}
    $director.replace(StartScene.new)
  end  
  
  def exit_message
    GameHelper.alert("Alert", "Your opponent has left the game, The computer is taking his place!")
    unless $turn 
      replace_computer_base_elements
      computer_take_control 
    else
      $versus = "compu"
    end  
    #exit_game(false)
  end  
  
  def roman_round
    if $n_round == 1
      "I"
    elsif $n_round == 2
      "II"
    elsif $n_round == 3
      "III"      
    end
  end  
  
  def show_points_popup
    extra_i = [] 
    extra_i2 = [] 
    extra_top = 0
    extra_top2 = 0
    $message_image_view_back2 = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2.image = UIImage.imageNamed(ImageHelper.name_for_phone("all_shadow.png"))        
   
   
    #Label Score
    label = UILabel.alloc.initWithFrame(CGRectMake(0, 1, @screen_width, 50))
    label.setText("SCORE ROUND #{roman_round}")
    label.setTextColor(UIColor.colorWithRed(181/255.0, green:51/255.0, blue:31/255.0, alpha:0.95))
    label.setTextAlignment(NSTextAlignmentCenter)
    label.setFont(UIFont.fontWithName("OpenSans-Light", size:18.0))
    
    
    #Label local player and opponent points
    label2 = UILabel.alloc.initWithFrame(CGRectMake(0, 32, @screen_width, 50))
    label2.setText("#{$game.local_player.round_score} - #{$game.opponent_player.round_score}")
    label2.setTextColor(UIColor.whiteColor)
    label2.setTextAlignment(NSTextAlignmentCenter)
    label2.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label2.font = label2.font.fontWithSize(30)
    
    #linea gris 1
    line1 = UIView.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 85, (@screen_width * 0.8), 1))
    line1.backgroundColor = UIColor.colorWithRed(73/255.0, green:73/255.0, blue:73/255.0, alpha:0.95)
    
    
    #Label stats
    label3 = UILabel.alloc.initWithFrame(CGRectMake(0, 100, @screen_width, 50))
    label3.setText("STATS")
    label3.setTextColor(UIColor.colorWithRed(181/255.0, green:51/255.0, blue:31/255.0, alpha:0.95))
    label3.setTextAlignment(NSTextAlignmentCenter)
    label3.setFont(UIFont.fontWithName("OpenSans-Light", size:18.0))
        
    
    
    ##### PLAYER 1 SECTION ######  
    
    player1_points = $game.local_player.player_points_resume
    
    #My Score title
    label4 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 135, @screen_width, 50))
    label4.setText("Your Score")
    label4.setTextColor(UIColor.whiteColor)
    label4.setTextAlignment(NSTextAlignmentLeft)
    label4.setFont(UIFont.fontWithName("OpenSans-Bold", size:18.0))
    label4.font = label4.font.fontWithSize(18)        
    
    label5 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 175, @screen_width, 50))
    label5.setText("Cards: #{$game.local_player.filtered_cards_picked_up.count}")
    label5.setTextColor(UIColor.whiteColor)
    label5.setTextAlignment(NSTextAlignmentLeft)
    label5.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label5.font = label5.font.fontWithSize(16)        
    
    
    pfirst = player1_points.first
    
    label6 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 210, @screen_width, 50))
    label6.setText("Points: #{pfirst}")
    label6.setTextColor(UIColor.whiteColor)
    label6.setTextAlignment(NSTextAlignmentLeft)
    label6.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label6.font = label6.font.fontWithSize(16)  
    
    player1_points.delete(pfirst)      

   player1_points.each do |t|
      label8 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 55), 230, @screen_width, 50))
      label8.setText(t)
      label8.setTextColor(UIColor.whiteColor)
      label8.setTextAlignment(NSTextAlignmentLeft)
      label8.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label8.font = label8.font.fontWithSize(16)  
      extra_top += 15
      extra_i << label8
    end
        
    label7 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 245 + extra_top, @screen_width, 50))
    label7.setText("Spades: #{$game.local_player.spades_count}")
    label7.setTextColor(UIColor.whiteColor)
    label7.setTextAlignment(NSTextAlignmentLeft)
    label7.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label7.font = label7.font.fontWithSize(16)        
         
    if $game.local_player.spades2? > 0    
      label11 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 63), 265 + extra_top, @screen_width, 50))
      label11.setText("2 of Spades")
      label11.setTextColor(UIColor.whiteColor)
      label11.setTextAlignment(NSTextAlignmentLeft)
      label11.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label11.font = label11.font.fontWithSize(16)  
      extra_i << label11          
    end      
          
    #big total
    label9 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65), 210, @screen_width, 50))
    label9.setText("#{$game.local_player.round_score}")
    label9.setTextColor(UIColor.whiteColor)
    label9.setTextAlignment(NSTextAlignmentLeft)
    label9.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label9.font = label9.font.fontWithSize(30)
     
    label10 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65) + 35 , 215, @screen_width, 50))
    label10.setText("points")
    label10.setTextColor(UIColor.whiteColor)
    label10.setTextAlignment(NSTextAlignmentLeft)
    label10.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label10.font = label10.font.fontWithSize(16)    
      
    #linea gris 2
    line2 = UIView.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 330, (@screen_width * 0.8), 1))
    line2.backgroundColor = UIColor.colorWithRed(73/255.0, green:73/255.0, blue:73/255.0, alpha:0.95)
           
    
    ##### PLAYER 2 SECTION ######  
    
    pl2_top = 220
    
    
    player2_points = $game.opponent_player.player_points_resume
    
    #My Score title
    label42 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 135 + pl2_top, @screen_width, 50))
    label42.setText("Opponent Score")
    label42.setTextColor(UIColor.whiteColor)
    label42.setTextAlignment(NSTextAlignmentLeft)
    label42.setFont(UIFont.fontWithName("OpenSans-Bold", size:18.0))
    label42.font = label42.font.fontWithSize(18)        
    
    label52 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 175 + pl2_top, @screen_width, 50))
    label52.setText("Cards: #{$game.opponent_player.filtered_cards_picked_up.count}")
    label52.setTextColor(UIColor.whiteColor)
    label52.setTextAlignment(NSTextAlignmentLeft)
    label52.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label52.font = label52.font.fontWithSize(16)        
    
    
    pfirst2 = player2_points.first
    
    label62 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 210 + pl2_top, @screen_width, 50))
    label62.setText("Points: #{pfirst2}")
    label62.setTextColor(UIColor.whiteColor)
    label62.setTextAlignment(NSTextAlignmentLeft)
    label62.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label62.font = label62.font.fontWithSize(16)  

    player2_points.delete(pfirst2)      

    player2_points.each do |t|
      label82 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 55), 230 + pl2_top, @screen_width, 50))
      label82.setText(t)
      label82.setTextColor(UIColor.whiteColor)
      label82.setTextAlignment(NSTextAlignmentLeft)
      label82.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label82.font = label82.font.fontWithSize(16)  
      extra_top2 += 15
      extra_i2 << label82
    end
        
    label72 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 245 + extra_top2 + pl2_top, @screen_width, 50))
    label72.setText("Spades: #{$game.opponent_player.spades_count}")
    label72.setTextColor(UIColor.whiteColor)
    label72.setTextAlignment(NSTextAlignmentLeft)
    label72.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label72.font = label72.font.fontWithSize(16)        
          
          
    if $game.opponent_player.spades2? > 0     
      label112 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 63), 265 + extra_top2 + pl2_top, @screen_width, 50))
      label112.setText("2 of Spades")
      label112.setTextColor(UIColor.whiteColor)
      label112.setTextAlignment(NSTextAlignmentLeft)
      label112.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label112.font = label112.font.fontWithSize(16)  
      extra_i2 << label112          
    end         
          
    #big total
    label92 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65), 210 + pl2_top, @screen_width, 50))
    label92.setText("#{$game.opponent_player.round_score}")
    label92.setTextColor(UIColor.whiteColor)
    label92.setTextAlignment(NSTextAlignmentLeft)
    label92.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label92.font = label92.font.fontWithSize(30)
     
    label102 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65) + 35 , 215 + pl2_top, @screen_width, 50))
    label102.setText("points")
    label102.setTextColor(UIColor.whiteColor)
    label102.setTextAlignment(NSTextAlignmentLeft)
    label102.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label102.font = label102.font.fontWithSize(16)    
      
      
    @close_b = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, 50, 50))
    @close_b.image = UIImage.imageNamed("cancel-iphone6plus.png") 
    @close_b.position = [($main_view.frame.size.width * 0.87), ($main_view.frame.size.height * 0.92)]
    
    @buttons_new_action3 << [@close_b, "close_score-Call", self]      
          
    items = [$message_image_back2, label, label2, line1, line2, label3, label4, label5, label6, label7, label9, label10, extra_i, extra_i2, label42, label52, label62, label72, label92, label102, @close_b].flatten 
    items.each{|i| $message_image_view_back2.addSubview(i)}         
   
                  
    $main_view.addSubview($message_image_view_back2) 
    
    $grupo_visible = true   
  end  
  
  
  def show_points_popup_data(lpc, opc)
    extra_i = [] 
    extra_i2 = [] 
    extra_top = 0
    extra_top2 = 0
    lpc[2] = lpc[2].gsub('\\"', '').gsub('"', '').split(",")
    opc[2] = opc[2].gsub('\\"', '').gsub('"', '').split(",")  
    
    $message_image_view_back2 = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2.image = UIImage.imageNamed(ImageHelper.name_for_phone("all_shadow.png"))        
   
   
    #Label Score
    label = UILabel.alloc.initWithFrame(CGRectMake(0, 1, @screen_width, 50))
    label.setText("SCORE ROUND #{roman_round}")
    label.setTextColor(UIColor.colorWithRed(181/255.0, green:51/255.0, blue:31/255.0, alpha:0.95))
    label.setTextAlignment(NSTextAlignmentCenter)
    label.setFont(UIFont.fontWithName("OpenSans-Light", size:18.0))
    
    
    #Label local player and opponent points
    label2 = UILabel.alloc.initWithFrame(CGRectMake(0, 32, @screen_width, 50))
    label2.setText("#{lpc[0]} - #{opc[0]}")
    label2.setTextColor(UIColor.whiteColor)
    label2.setTextAlignment(NSTextAlignmentCenter)
    label2.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label2.font = label2.font.fontWithSize(30)
    
    #linea gris 1
    line1 = UIView.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 85, (@screen_width * 0.8), 1))
    line1.backgroundColor = UIColor.colorWithRed(73/255.0, green:73/255.0, blue:73/255.0, alpha:0.95)
    
    
    #Label stats
    label3 = UILabel.alloc.initWithFrame(CGRectMake(0, 100, @screen_width, 50))
    label3.setText("STATS")
    label3.setTextColor(UIColor.colorWithRed(181/255.0, green:51/255.0, blue:31/255.0, alpha:0.95))
    label3.setTextAlignment(NSTextAlignmentCenter)
    label3.setFont(UIFont.fontWithName("OpenSans-Light", size:18.0))
        
    
    
    ##### PLAYER 1 SECTION ######  
    
    player1_points = lpc[2]
    
    #My Score title
    label4 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 135, @screen_width, 50))
    label4.setText("Your Score")
    label4.setTextColor(UIColor.whiteColor)
    label4.setTextAlignment(NSTextAlignmentLeft)
    label4.setFont(UIFont.fontWithName("OpenSans-Bold", size:18.0))
    label4.font = label4.font.fontWithSize(18)        
    
    label5 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 175, @screen_width, 50))
    label5.setText("Cards: #{lpc[1]}")
    label5.setTextColor(UIColor.whiteColor)
    label5.setTextAlignment(NSTextAlignmentLeft)
    label5.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label5.font = label5.font.fontWithSize(16)        
    
    
    pfirst = player1_points.first
    
    label6 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 210, @screen_width, 50))
    label6.setText("Points: #{pfirst}")
    label6.setTextColor(UIColor.whiteColor)
    label6.setTextAlignment(NSTextAlignmentLeft)
    label6.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label6.font = label6.font.fontWithSize(16)  
    
    player1_points.delete(pfirst)      

    if player1_points.count > 0
      label8 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 55), 230, @screen_width, 50))
      label8.setText(player1_points.last)
      label8.setTextColor(UIColor.whiteColor)
      label8.setTextAlignment(NSTextAlignmentLeft)
      label8.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label8.font = label8.font.fontWithSize(16)  
      extra_top += 15
      extra_i << label8
    end
        
    label7 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 245 + extra_top, @screen_width, 50))
    label7.setText("Spades: #{lpc[3]}")
    label7.setTextColor(UIColor.whiteColor)
    label7.setTextAlignment(NSTextAlignmentLeft)
    label7.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label7.font = label7.font.fontWithSize(16)        
         
    if lpc[4] == "true" or lpc[4] == true    
      label11 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 63), 265 + extra_top, @screen_width, 50))
      label11.setText("2 of Spades")
      label11.setTextColor(UIColor.whiteColor)
      label11.setTextAlignment(NSTextAlignmentLeft)
      label11.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label11.font = label11.font.fontWithSize(16)  
      extra_i << label11          
    end      
          
    #big total
    label9 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65), 210, @screen_width, 50))
    label9.setText("#{lpc[0]}")
    label9.setTextColor(UIColor.whiteColor)
    label9.setTextAlignment(NSTextAlignmentLeft)
    label9.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label9.font = label9.font.fontWithSize(30)
     
    label10 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65) + 35 , 215, @screen_width, 50))
    label10.setText("points")
    label10.setTextColor(UIColor.whiteColor)
    label10.setTextAlignment(NSTextAlignmentLeft)
    label10.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label10.font = label10.font.fontWithSize(16)    
      
    #linea gris 2
    line2 = UIView.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 330, (@screen_width * 0.8), 1))
    line2.backgroundColor = UIColor.colorWithRed(73/255.0, green:73/255.0, blue:73/255.0, alpha:0.95)
           
    
    ##### PLAYER 2 SECTION ######  
    
    pl2_top = 220
    
    
    player2_points = opc[2]
    
    #My Score title
    label42 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 135 + pl2_top, @screen_width, 50))
    label42.setText("Opponent Score")
    label42.setTextColor(UIColor.whiteColor)
    label42.setTextAlignment(NSTextAlignmentLeft)
    label42.setFont(UIFont.fontWithName("OpenSans-Bold", size:18.0))
    label42.font = label42.font.fontWithSize(18)        
    
    label52 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 175 + pl2_top, @screen_width, 50))
    label52.setText("Cards: #{opc[1]}")
    label52.setTextColor(UIColor.whiteColor)
    label52.setTextAlignment(NSTextAlignmentLeft)
    label52.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label52.font = label52.font.fontWithSize(16)        
    
    
    pfirst2 = player2_points.first
    
    label62 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 210 + pl2_top, @screen_width, 50))
    label62.setText("Points: #{pfirst2}")
    label62.setTextColor(UIColor.whiteColor)
    label62.setTextAlignment(NSTextAlignmentLeft)
    label62.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label62.font = label62.font.fontWithSize(16)  

    player2_points.delete(pfirst2)      

    if player2_points.count > 0
 
      label82 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 55), 230 + pl2_top, @screen_width, 50))
      label82.setText(player2_points.last)
      label82.setTextColor(UIColor.whiteColor)
      label82.setTextAlignment(NSTextAlignmentLeft)
      label82.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label82.font = label82.font.fontWithSize(16)  
      extra_top2 += 15
      extra_i2 << label82
    end
        
    label72 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 245 + extra_top2 + pl2_top, @screen_width, 50))
    label72.setText("Spades: #{opc[3]}")
    label72.setTextColor(UIColor.whiteColor)
    label72.setTextAlignment(NSTextAlignmentLeft)
    label72.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label72.font = label72.font.fontWithSize(16)        
          
          
    if opc[4] == "true" or opc[4] == true   
      label112 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 63), 265 + extra_top2 + pl2_top, @screen_width, 50))
      label112.setText("2 of Spades")
      label112.setTextColor(UIColor.whiteColor)
      label112.setTextAlignment(NSTextAlignmentLeft)
      label112.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label112.font = label112.font.fontWithSize(16)  
      extra_i2 << label112          
    end         
          
    #big total
    label92 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65), 210 + pl2_top, @screen_width, 50))
    label92.setText("#{opc[0]}")
    label92.setTextColor(UIColor.whiteColor)
    label92.setTextAlignment(NSTextAlignmentLeft)
    label92.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label92.font = label92.font.fontWithSize(30)
     
    label102 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65) + 35 , 215 + pl2_top, @screen_width, 50))
    label102.setText("points")
    label102.setTextColor(UIColor.whiteColor)
    label102.setTextAlignment(NSTextAlignmentLeft)
    label102.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label102.font = label102.font.fontWithSize(16)    
      
      
    @close_b = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, 50, 50))
    @close_b.image = UIImage.imageNamed("cancel-iphone6plus.png") 
    @close_b.position = [($main_view.frame.size.width * 0.87), ($main_view.frame.size.height * 0.92)]
    
    @buttons_new_action3 << [@close_b, "close_score-Call", self]      
          
    items = [$message_image_back2, label, label2, line1, line2, label3, label4, label5, label6, label7, label9, label10, extra_i, extra_i2, label42, label52, label62, label72, label92, label102, @close_b].flatten 
    items.each{|i| $message_image_view_back2.addSubview(i)}         
   
                  
    $main_view.addSubview($message_image_view_back2) 
    
    $grupo_visible = true   
  end  
  
  
  
  def close_score
    mp "CLOSEEEE!!!"
    $grupo_visible = false
    $message_image_view_back2.removeFromSuperview if $message_image_view_back2
  end  
  
  def check_available_builds(card)
    available = $game.available_builds?(card)  
    $high_card = card
    build_actions(available) if available
  end  
  
  def build_actions(response)
     actions = ['cancel']
     case response.size
     when 2
       actions << "create_double_build"
     when 3
       actions << "create_triple_build"
     when 4      
       actions << "create_quadruple_build"
     else
       nil
     end
     show_build_actions(actions, response) if actions.size > 1      
  end  
  
  def hightlight_positions(response)
    response.each do |p|
      if p.is_a?(Array)
        p.each{|i| hightlight_position(i)}
      else
        hightlight_position(p)
      end    
    end  
  end  
  
  def hightlight_position(index)
    if position_is_not_hightlight?(index)
      $hl_positions << index
      hl = MG::Sprite.new("card_hightlight.png")
      layout = $board_positions[index]
      hl.scale = (ImageHelper.porcent_for($visible_size.width, 15) / $positions_cards[index].first.size.width)
      hl.position = [layout.position.x + (layout.size.width / 2), layout.position.y + (layout.size.height / 2)]
      add hl, 5
      @hightlight_items << hl
      set_hightlight_rotate_for_position(index)
    else
      clean_hightlight_from(index)
    end    
  end  
  
  def set_hightlight_rotate_for_position(position)
    
    index = $hl_positions.find_index(position)
    item = @hightlight_items[index]
    cards_on_position = $positions_cards[position].count

    item.rotation = 20 * (cards_on_position - 1)
    item.z_index = cards_on_position + 5

    m = item.size.width * 0.12
      
    defaultx = $positions_cards[position].first.position.x
    defaulty = $positions_cards[position].first.position.y

    item.position = [defaultx + ((cards_on_position - 1)*m), defaulty - ((cards_on_position - 1)*(m / 2))]    

  end    
  
  def position_is_not_hightlight?(index)
    !$hl_positions.include?(index)
  end  
  
  def clean_all_hightlights
    0.upto(15).each do |i|
      clean_hightlight_from(i)
    end  
  end  
  
  def clean_hightlight_from(position)
    if $hl_positions.include?(position)
      mp "Removing Highlight from position #{position}"
      index = $hl_positions.find_index(position)
      @hightlight_items[index].delete_from_parent
      @hightlight_items.delete(@hightlight_items[index])
      $hl_positions.delete($hl_positions[index])
    end  
  end  
  
  def show_build_actions(actions, response)
    $amenu = []
    $object = @card_t
    
    hightlight_positions(response)
    
    action_shadow = MG::Sprite.new(ImageHelper.name_for_phone("action_shadow.png"))
    action_shadow.position = [(action_shadow.size.width / 2), (action_shadow.size.height / 2)]
    @action_menu_items << action_shadow
    
    self.add action_shadow, 5
    actions.each_with_index{|a, i| add_action(a, i+1)}
  end  
  
  def is_on_hand_or_board?(card)
    is_on_hand?(card[2]) or is_on_board?(card[0])
  end  
  
  def is_on_board?(card)
   $positions_cards.each_with_index do |p, i| 
     return true if $positions_cards[i].include?(card) and $positions_cards[i].size == 1
    end 
    return false
  end  
  
  def new_round
    $rr += 1
    mp "NEW ROUND!!!"
    $game.deal_cards?
    $gcm.send_data_to_opponent("new_round:::#{$gcm.game_data}") if $versus == "user"
    add_base_cards   
  end  
  
  def computer_move
    #Computer turn
    if $versus == "compu"
      if $game.opponent_player.hand.size != 0
        move = $game.computers_turn
        index = $o_player_cards_objs.find_index(move[0])
  
        if move[1] == :computer_move_to_pick_up_build
          move_to_pick_up_build(move)
        elsif move[1] == :move_to_create_stack
          move_to_create_stack(move)
        else
          move_compu_card($o_player_cards[index], $board_positions[move[2]], move[2], index)
          evaluate_computer_action(move[3], move[2], move[0].rank)  
        end    
    
        EM.add_timer 1.2 do
          0.upto(15).each{|n| set_card_rotate_for_position(n) }
          @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

          @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
          @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
          $turn = true
      
          if $game.local_player.hand.size == 0
            if $game.deck.cards.count == 0
              mp "END OF DECK1"  
              new_deck 
            else
              new_round
              if $game.deck.cards.count == 0
                show_flashing_message("cards")
                EM.add_timer 1.8 do
                  hide_message
                end  
              end
            end     
          end 
        
        end 
      else
        $turn = true
        if $game.local_player.hand.size == 0
          mp "END OF DECK2"  
          new_deck
        end  
      end 
    else
      $gcm.send_data_to_opponent($my_turn_actions)
      restart_timer
      if $game.deck.cards.count == 0 and $game.opponent_player.hand.size == 0 and $game.local_player.hand.size == 0
        new_deck 
      elsif $game.opponent_player.hand.size == 0
        new_round
        if $game.deck.cards.count == 0
          show_flashing_message("cards")
          EM.add_timer 1.8 do
            hide_message
          end  
        end
      end   
    end  
    0.upto(15).each{|n| set_card_rotate_for_position(n) }
    #update_scores
  end  
  
  def update_turn
    clear_all_positions
    @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

    @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
    @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
    $turn = true
  end  
  
  def update_scores
    #computer scores
    @title5.delete_from_parent
    @title5 = MG::Text.new("#{$game.opponent_player.points} pts", 'OpenSans-Bold', 28)
    @title5.area_size = @coin_bar2.size  
    @title5.position = [@coin_bar2.position.x + 20, @coin_bar2.position.y - (@coin_bar2.size.height / 4)]
    @title5.text_color = [1.0,1.0,1.0,1.0]
    @title5.horizontal_align = :left
    add @title5, 1   
    #local score
    @title6.delete_from_parent
    @title6 = MG::Text.new("#{$game.local_player.points} pts", 'OpenSans-Bold', 28)
    @title6.area_size = @coin_bar1.size  
    @title6.position = [@coin_bar1.position.x + 20, @coin_bar1.position.y - (@coin_bar1.size.height / 4)]
    @title6.text_color = [1.0,1.0,1.0,1.0]
    @title6.horizontal_align = :left
    add @title6, 1      
  end  
  
  def new_deck
    $next_player = next_player
    $turn = !$turn
    
    $game.end_game
    #App::Persistence["#{$gcm.local_player_id}-mostcardsrow"] = $game.most_cards_last_hand? ? App::Persistence["#{$gcm.local_player_id}-mostcardsrow"] + 1 : 0 
    update_scores
    
    op_resumen = $game.opponent_player.player_points_resume.to_s.gsub("[", "").gsub("]", "").gsub('\\"', '').gsub('"', '')
    lp_resumen = $game.local_player.player_points_resume.to_s.gsub("[", "").gsub("]", "").gsub('\\"', '').gsub('"', '')
    op_resumen = " " if op_resumen == ""
    lp_resumen = " " if lp_resumen == ""
    
    ops = [$game.opponent_player.round_score, $game.opponent_player.filtered_cards_picked_up.count, "#{op_resumen}", $game.opponent_player.spades_count, $game.opponent_player.spades2? > 0]
    lps = [$game.local_player.round_score, $game.local_player.filtered_cards_picked_up.count, "#{lp_resumen}", $game.local_player.spades_count, $game.local_player.spades2? > 0]
    
    
    if is_a_winner?

      NSLog("Winner Player: #{$game.winner?.id}")
      $gcm.send_data_to_opponent("winner:::#{$game.winner?.id}::#{$game.local_player.points}::#{$game.opponent_player.points}") if $versus == "user"
      $main_view_objs.each{|o| o.removeFromSuperview}
      $director.replace(WinnerScene.new)
      
      $gcm.send_data_to_opponent("show_points_popup:::#{ops.to_s.gsub("[", "").gsub("]", "")}::#{lps.to_s.gsub("[", "").gsub("]", "")}") if $versus == "user"
      
      
    else
      
      #TODO send to show resumen
      show_points_popup
      
      $gcm.send_data_to_opponent("show_points_popup:::#{ops.to_s.gsub("[", "").gsub("]", "")}::#{lps.to_s.gsub("[", "").gsub("]", "")}") if $versus == "user"
      $game.new_round
      $round += 1
      $gcm.send_data_to_opponent("new_deck:::#{$gcm.game_data}") if $versus == "user"
      clear_all_positions
      set_round_title
      add_base_cards     
      
      if $turn
        $rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
        $rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for($main_view.frame.size.height, 100), $main_view.frame.size.width, ImageHelper.porcent_for($main_view.frame.size.height, 31))
        $rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      else
        $rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
        $rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)    
        $rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for($main_view.frame.size.height, 68.83), $main_view.frame.size.width, ImageHelper.porcent_for($main_view.frame.size.height, 31))
        $rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)     
      end  
      
      mp "RUNNING NEW ROUND"
      mp "Versus: #{$versus}"
      mp "Turn: #{$turn}" 
      
      if $versus == "compu" and $turn == false
        EM.add_timer 1.5 do
          computer_move 
        end
      end  
    end    
  end  
  
  def clear_all_positions
    0.upto(15).each{|n| clear_position(n) }
  end  
  
  def set_round_title
    #TODO Show big message
    $n_round += 1
    @round = @round.nil? ? 2 : @round + 1
    @titleX.delete_from_parent
    @titleX = MG::Text.new("#{ordinal_num(@round)} ROUND", 'OpenSans-Bold', 22)     
    @titleX.position = [$visible_size.width / 2, ImageHelper.top_porcent(24.4)]
    @titleX.text_color = [0.968, 0.725, 0.262,0.9]
    add @titleX, 3
  end  
  
  def ordinal_num(num)
    case num
    when 1
      "1ST"
    when 2
      "2ND"
    when 3
      "3RD"
    when 4
      "4TH"
    end  
  end  
  
  def is_a_winner?
    $game.winner? != "No Winner"
  end  
  
  def move_to_create_stack(move)

    puts "STACK"*80

    positions_to_move = move[3].flatten
    position_destination = move[2]
    positions_to_move.each do |p|
      $positions_cards[p].each do |c|      
        move_compu_card(c, $board_positions[position_destination], position_destination, nil)
      end
      $positions_cards[p] = []
    end  
    index = $o_player_cards_objs.find_index(move[0])
    move_compu_card($o_player_cards[index], $board_positions[move[2]], move[2], index)
    evaluate_computer_action(move[4], move[2], move[0].rank)
  end    
  
  def move_to_pick_up_build(move)
    positions_to_move = move[3].flatten
    position_destination = move[2]
    positions_to_move.each do |p|
      $positions_cards[p].each do |c|      
        move_compu_card(c, $board_positions[position_destination], position_destination, nil)
      end
      $positions_cards[p] = []
    end  
    index = $o_player_cards_objs.find_index(move[0])
    move_compu_card($o_player_cards[index], $board_positions[move[2]], move[2], index)
    evaluate_computer_action(move[4], move[2], move[0].rank)
  end  
  
  def move_compu_card(card, position, position_i, index=nil)
    if $versus == "user"
      $o_player_back_cards.select{|a| a[1].to_i == index }.last[0].delete_from_parent if index
    else
      if index
        basec = $o_player_back_cards[index]
        basec[0].delete_from_parent if basec     
      end  
    end

    card.z_index = 3
    center_card_on_position(card, position, 0.8, position_i)
  end    
  
  def evaluate_computer_action(action, pos, card_rank=nil)
    case action
    when "Cards Picked Up"
      clear_pos_timed(pos)
    when "Build"
      clear_pos_timed(pos, "obuild")
    when "Double Build"
      clear_pos_timed(pos)      
    when "Triple Build"
      clear_pos_timed(pos)      
    when "Quadruple Build"  
      clear_pos_timed(pos)   
    when "Destroy"  
      clear_pos_timed(pos)    
    when "Stacking Number"
      show_flashing_message("ostacking", card_rank)
      EM.add_timer 1.8 do
        hide_message
      end  
    when "Stack"
      clear_pos_timed(pos, "ostack", card_rank)   
    when "Steal"
      clear_pos_timed(pos, "ostack", card_rank) 
    else
      mp action.inspect
    end    
  end  
  
  def clear_pos_timed(num, message=nil, card_rank=nil)
    show_message(message, card_rank) if message
    EM.add_timer 1.5 do
      clear_position(num)
      hide_message if message
    end
  end  
  
  def clear_all_pos
    $positions_cards.each{|p| p.each{|c| c.delete_from_parent} }
  end  
  
  def is_on_hand?(card)
    !local_player.hand.find_index(card).nil?
  end  
  
  def initial_vars
    @shop = Shop.new
    @buttons = []
    @buttons2 = []    
    @buttonsl = []
    @buttons_new_action = []
    @buttons_new_action2 = []    
    @buttons_new_action3 = []
    $tobjs = []
    @buttons << GameHelper.add_back_button_to("StartScene-Clean", self)
    @screen_width = $main_view.frame.size.width
    @screen_height = $main_view.frame.size.height
  end  
  
  def start_id(player)
    player.id == $local_player[:id] ? 1 : 0
  end  
  
  def local_player
    $game.local_player
  end  
  
  def opponent_player
    $game.opponent_player
  end    
  
  def select_player(id)
    $game.players.select{|p| p.id == id}.last
  end  
  
  def update(delta)  
    if @deal_elements.size > 0
      ele = @deal_elements
      ele.each do |e|
        if (current_usecs > e[1])
          @deal_elements.delete(e)  
          e[0].move_to([e[0].position.x, e[0].position.y + 400], 0.5)
        end
      end  
    end  

    if @move_message and (current_usecs > @move_message_on)
      @move_message = false
      @message.move_to([($visible_size.width / 2) * -1, $visible_size.height / 2], 1)
    end  
  end    
  
  def show_starting_player
    @start_img = @start_player.id == $local_player[:id] ? "yourturn" : "opponentturn"   
    @message = MG::Sprite.new("#{@start_img}.png")
    @message.position = [$visible_size.width, $visible_size.height / 2]
    add @message, 2
    @message.move_to([$visible_size.width / 2, $visible_size.height / 2], 1)
    @move_message = true
    @move_message_on = current_usecs + 3000 #3 seconds
  end  

  def add_board_main_items
    add_board_bg
    add_hand_number_section
    add_local_player_elements
    add_new_actions_menu
    if $versus == "user"
      add_opponent_elements
    else
      add_computer_elements 
    end    
    add_base_cards
  end
  
  def add_new_actions_menu
    @action_place = MG::Sprite.new(ImageHelper.name_for_phone("action_placecard.png"))
    @action_place.scale = 0.8
    @action_place.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_place.size.width / 2), ImageHelper.porcent_for($visible_size.height, 58) + (@action_place.size.height)]    
    @action_place.visible = false
    
    @action_pickup = MG::Sprite.new(ImageHelper.name_for_phone("action_pickup.png"))
    @action_pickup.scale = 0.8
    @action_pickup.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_pickup.size.width / 2), ImageHelper.porcent_for($visible_size.height, 52) + (@action_pickup.size.height)]    
    @action_pickup.visible = false
    
    @action_build = MG::Sprite.new(ImageHelper.name_for_phone("action_build.png"))
    @action_build.scale = 0.8
    @action_build.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_build.size.width / 2), ImageHelper.porcent_for($visible_size.height, 46) + (@action_build.size.height)]
    @action_build.visible = false    
    
    @action_destroy = MG::Sprite.new(ImageHelper.name_for_phone("action_destroy.png"))
    @action_destroy.scale = 0.8
    @action_destroy.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_destroy.size.width / 2), ImageHelper.porcent_for($visible_size.height, 40) + (@action_destroy.size.height)]
    @action_destroy.visible = false

    @action_stack = MG::Sprite.new(ImageHelper.name_for_phone("action_stack.png"))
    @action_stack.scale = 0.8
    @action_stack.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_stack.size.width / 2), ImageHelper.porcent_for($visible_size.height, 34) + (@action_stack.size.height)]
    @action_stack.visible = false      
        
    @plus_icon = MG::Sprite.new(ImageHelper.name_for_phone("plus_icon.png"))
    @plus_icon.scale = 0.7
    @plus_icon.position = [ImageHelper.porcent_for($visible_size.width, 2.7) + (@plus_icon.size.width / 2), ImageHelper.porcent_for($visible_size.height, 30) + (@plus_icon.size.height / 2)]
    
    add @action_place, 37
    add @action_pickup, 38
    add @action_build, 39
    add @action_destroy, 40
    add @action_stack, 41
    add @plus_icon, 30
    
    @menu_visible = false
    
    @buttons_new_action << [@plus_icon, "trigger_new_action-Call", self]
    @buttons_new_action2 << [@action_stack, "call_create_stack-Call", self]
    @buttons_new_action2 << [@action_destroy, "call_destroy-Call", self]
    @buttons_new_action2 << [@action_build, "call_build-Call", self]
    @buttons_new_action2 << [@action_pickup, "call_pick_up-Call", self]                
    @buttons_new_action2 << [@action_place, "call_place-Call", self]
  end  
  
  def trigger_new_action
    @menu_visible ? hide_new_action_menu : show_new_action_menu
  end  
  
  def show_new_action_menu
    mp "SHOW!!"
    @menu_visible = true
    
    #@action_destroy.
    #@action_build.
    #@action_pickup.
    
    @action_place.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_stack.size.width / 2), ImageHelper.porcent_for($visible_size.height, 34) + (@action_stack.size.height)]
    @action_destroy.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_stack.size.width / 2), ImageHelper.porcent_for($visible_size.height, 34) + (@action_stack.size.height)]
    @action_build.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_stack.size.width / 2), ImageHelper.porcent_for($visible_size.height, 34) + (@action_stack.size.height)]
    @action_pickup.position = [ImageHelper.porcent_for($visible_size.width, 0) + (@action_stack.size.width / 2), ImageHelper.porcent_for($visible_size.height, 34) + (@action_stack.size.height)]
    @action_destroy.visible = true
    @action_build.visible = true
    @action_pickup.visible = true
    @action_stack.visible = true
    @action_place.visible = true
    
    @action_destroy.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_destroy.size.width / 2), ImageHelper.porcent_for($visible_size.height, 40) + (@action_destroy.size.height)], 0.1)
    @action_build.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_destroy.size.width / 2), ImageHelper.porcent_for($visible_size.height, 40) + (@action_destroy.size.height)], 0.1)
    @action_pickup.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_destroy.size.width / 2), ImageHelper.porcent_for($visible_size.height, 40) + (@action_destroy.size.height)], 0.1)
    @action_place.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_destroy.size.width / 2), ImageHelper.porcent_for($visible_size.height, 40) + (@action_destroy.size.height)], 0.1)
   
   
    EM.add_timer (0.16) do
      @action_build.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_build.size.width / 2), ImageHelper.porcent_for($visible_size.height, 46) + (@action_build.size.height)], 0.1)
      @action_pickup.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_build.size.width / 2), ImageHelper.porcent_for($visible_size.height, 46) + (@action_build.size.height)], 0.1)
      EM.add_timer (0.16) do
        @action_pickup.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_pickup.size.width / 2), ImageHelper.porcent_for($visible_size.height, 52) + (@action_pickup.size.height)]  , 0.1)
        @action_place.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_pickup.size.width / 2), ImageHelper.porcent_for($visible_size.height, 52) + (@action_pickup.size.height)]  , 0.1)   
        EM.add_timer (0.16) do
           @action_place.move_to([ImageHelper.porcent_for($visible_size.width, 0) + (@action_pickup.size.width / 2), ImageHelper.porcent_for($visible_size.height, 58) + (@action_pickup.size.height)]  , 0.1) 
        end     
      end  
    end
  end
  
  def call_place
    unless $showing_message    
      if $valid_actions.include? "place_card"
        place_card
      else
        show_message("invalid")   
        EM.add_timer 1.8 do
          hide_message
        end  
      end 
      hide_new_action_menu
    end  
  end  
  
  def call_pick_up
    unless $showing_message    
      if $valid_actions.include? "pick_up"
        pick_up
      elsif $valid_actions.include? "pick_up_build"  
        pick_up_build
      else
        show_message("invalid")   
        EM.add_timer 1.8 do
          hide_message
        end  
      end 
      hide_new_action_menu
    end  
  end  
  
  def call_build
    unless $showing_message    
      if $valid_actions.include? "build"
        create_build
      else
        if $hl_positions != [] and $hl_positions2 == [] and @card_t
          mp "*"*30
          mp $valid_actions
          mp "*"*30          
          create_build
        else
          show_message("invalid")    
          EM.add_timer 1.8 do
            hide_message
          end      
        end    
     
      end 
      hide_new_action_menu
    end  
  end  
  
  def call_destroy
    unless $showing_message
      if $valid_actions.include? "destroy" or $valid_actions.include? "create_destroy"
        create_destroy
      else
        show_message("invalid")
        EM.add_timer 1.8 do
          hide_message
        end        
      end  
      hide_new_action_menu
    end   
  end  
  
  def call_create_stack
    unless $showing_message    
      if $valid_actions.include? "create_stack"
        create_stack
      elsif $valid_actions.include? "create_steal"  
        create_steal
      else
        show_message("invalid")
        EM.add_timer 1.8 do
          hide_message
        end        
      end  
      hide_new_action_menu
    end  
  end  
  
  def hide_new_action_menu
    mp "HIDE!!"  
    @menu_visible = false
    @action_destroy.visible = false
    @action_build.visible = false
    @action_pickup.visible = false    
    @action_stack.visible = false    
    @action_place.visible = false       
  end    
  
  def add_hand_number_section
    #hand number
    start_alpha = $turn ? 0.5 : 0.0
    @rect2_over1= UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, ImageHelper.porcent_for(@screen_height, 22.9)))
    @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:start_alpha)
    
    if $turn
      start_alpha = 0.0
      start_top = ImageHelper.porcent_for(@screen_height, 100)
    else
      start_alpha = 0.5
      start_top = ImageHelper.porcent_for(@screen_height, 68.83)
    end  
    @rect2_over2= UIView.alloc.initWithFrame(CGRectMake(0, start_top, @screen_width, ImageHelper.porcent_for(@screen_height, 31)))
    @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:start_alpha)
        
    
    $rect2_over1 = @rect2_over1
    $rect2_over2 = @rect2_over2
    
    $main_view.addSubview(@rect2_over1)
    $main_view_objs << @rect2_over1
    
    $main_view.addSubview(@rect2_over2)
    $main_view_objs << @rect2_over2    
     
    @titleX = MG::Text.new("1ST ROUND", 'OpenSans-Bold', 22)     
    @titleX.position = [$visible_size.width / 2, ImageHelper.top_porcent(24.4)]
    @titleX.text_color = [0.968, 0.725, 0.262,0.9]
    add @titleX, 3
  end  
  
  def current_usecs
    now = Time.now
    current_usecs = (now.to_i * 1000) + (now.usec / 1000) #Get current time in microseconds
  end    
  
  def add_board_bg
    @board_bg = MG::Sprite.new(ImageHelper.name_for_phone("board-bg.png"))
    @board_bg.position = [$visible_size.width / 2, ImageHelper.top_porcent(22.5) - @board_bg.size.height / 2]
    add @board_bg, 2
    
    @board_bgl = MG::Sprite.new(ImageHelper.name_for_phone("game-line.png"))
    @board_bgl.position = [$visible_size.width / 2, ImageHelper.top_porcent(22.5) - @board_bgl.size.height / 2]
    add @board_bgl, 2    
  end      
  
  def add_local_player_elements
    @rect1 = MG::Draw.new
    @rect1 .rect([0, 0], [$visible_size.width, ImageHelper.top_porcent(69.5)], [0.93, 0.26, 0.15, 1.0], true)
    add @rect1, 1 
    
    @coin_bar1 = MG::Sprite.new(ImageHelper.name_for_phone("coinbar.png"))
    @coin_bar1.position = [$visible_size.width - (@coin_bar1.size.width / 2), ImageHelper.top_porcent(89.5) - @coin_bar1.size.height / 2]
    add @coin_bar1, 1
    
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 5), ImageHelper.porcent_for(@screen_height, 89.5), ImageHelper.porcent_for(@screen_width, 11.5), ImageHelper.porcent_for(@screen_width, 11.5)))
    image_container.image = $gcm.local_player_photo
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = image_container.frame.size.width / 2
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    
    @title1 = MG::Text.new(local_player.name, 'OpenSans-Bold', 29)
    @title1.area_size = [300, 40]    
    @title1.position = [ImageHelper.left_porcent(17.8) + (@title1.size.width / 2), ImageHelper.top_porcent(91.8)]
    @title1.text_color = [1.0,1.0,1.0,1.0]
    @title1.horizontal_align = :left
    add @title1, 1
    

    @title2 = MG::Text.new("#{$local_player[:points]} pts", 'OpenSans-Light', 28)
    @title2.area_size = [200, 40]     
    @title2.position = [ImageHelper.left_porcent(17.8) + (@title2.size.width / 2), ImageHelper.top_porcent(94.2)]
    @title2.text_color = [1.0,1.0,1.0,1.0]
    @title2.horizontal_align = :left
    add @title2, 1   
  
  
    @title6 = MG::Text.new("0 pts", 'OpenSans-Bold', 28)
    @title6.area_size = @coin_bar1.size  
    @title6.position = [@coin_bar1.position.x + 20, @coin_bar1.position.y - (@coin_bar1.size.height / 4)]
    @title6.text_color = [1.0,1.0,1.0,1.0]
    @title6.horizontal_align = :left
    add @title6, 1   
    
    
    if $versus == "user"
      if App::Persistence["#{$gcm.local_player_id}-lp-trinket"]
        $trinket_btn = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 100) - ImageHelper.porcent_for(@screen_height, 9), ImageHelper.porcent_for(@screen_height, 64), ImageHelper.porcent_for(@screen_width, 14), ImageHelper.porcent_for(@screen_width, 14)))
        $trinket_btn.image = UIImage.imageNamed(ImageHelper.name_for_phone(App::Persistence["#{$gcm.local_player_id}-lp-trinket"])) 
      else  
        $trinket_btn = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 100) - ImageHelper.porcent_for(@screen_height, 7), ImageHelper.porcent_for(@screen_height, 67.5), ImageHelper.porcent_for(@screen_width, 8), ImageHelper.porcent_for(@screen_width, 8)))
        $trinket_btn.image = UIImage.imageNamed(ImageHelper.name_for_phone("trinket-icon.png.png"))
      end
      $trinket_btn.contentMode = UIViewContentModeScaleAspectFill
      $trinket_btn.layer.masksToBounds = true
      $trinket_btn.layer.cornerRadius = $trinket_btn.frame.size.width / 2
      $main_view.insertSubview($trinket_btn, atIndex:100)
      $main_view_objs << $trinket_btn       
      @buttons2 << [$trinket_btn, "lp_show_trinkets_menu-Call", self]   
    end  
    
  end
  
  def lp_show_trinkets_menu
    @trinket_for = "local"
    show_trinkets_menu
  end  
  
  def op_show_trinkets_menu
    @trinket_for = "opponent"    
    show_trinkets_menu
  end  
  
  def show_trinkets_menu
    if $s_trinket == false
      mp "SHOW TRINKETS MENU"
      $s_trinket = true
      @trinkes_container = MG::Layout.new
      @trinkes_container.size = [ImageHelper.left_porcent(90), ImageHelper.left_porcent(70)]
      @trinkes_container.position = [ImageHelper.left_porcent(5) , ImageHelper.top_porcent(45) - @trinkes_container.size.height / 2 ]
      @trinkes_container.background_color = [0.9372, 0.2549, 0.1294]
    
      @close_btn = MG::Sprite.new(ImageHelper.name_for_phone("close.png"))
      @close_btn.position = [@trinkes_container.size.width - (@close_btn.size.width) + 10, @trinkes_container.size.height - @close_btn.size.height + 10 ]
      @trinkes_container.add @close_btn
    
      @buttonsl << [@close_btn, "hide_trinkets_menu-Call", self, @trinkes_container]    
    
      add_trinkets(@trinkes_container)
    
      add @trinkes_container, 10
    end
  end  
  
  def add_trinkets(container)
    $trinkets_images = []
    i = 0

    @shop.trinkets.each_slice(5) do |a|
      oi = 0
      puts i
      a.each do |tr|
        t = MG::Sprite.new(ImageHelper.name_for_phone(tr.image_icon))
        t.position = [((t.size.width + 20) * oi) + (t.size.width / 2) + 30, ((container.size.height - t.size.height) - (i * t.size.height)) - (i * 36) ]
        container.add t
        
        ttex = MG::Text.new(tr.price.to_s, 'OpenSans-Bold', 16)
        ttex.area_size = [t.size.width, 30]      
        ttex.position = [t.position.x, t.position.y - (t.size.height / 2) - 5]
        ttex.text_color = [0.0,0.0,0.0,1.0]
        ttex.horizontal_align = :center
        container.add ttex
        $tobjs << ttex
        @buttonsl << [t, "buy_#{tr.image.gsub('.png', '')}-Call", self, container] 
        oi += 1
      end  
      i+=1
    end  
  end  
  
  def buy_food
    buy_trinket("food", 50)
  end  
  
  def buy_diamond_ring
    buy_trinket("diamond_ring", 2500)
  end
  
  def buy_champagne_bottle
    buy_trinket("champagne_bottle", 500)
  end
  
  def buy_box_of_cigars
    buy_trinket("box_of_cigars", 250)
  end  
  
  def buy_cigarettes
    buy_trinket("cigarettes", 250)
  end
  
  def buy_gold_chain
    buy_trinket("gold_chain", 2500)
  end
  
  def buy_shoes
    buy_trinket("shoes", 300)
  end
  
  def buy_purse
    buy_trinket("purse", 750)
  end
  
  def buy_roses
    buy_trinket("roses", 100)
  end
  
  def buy_dessert
    buy_trinket("dessert", 50)
  end
  
  def buy_sneakers
    buy_trinket("sneakers", 300)
  end
  
  def buy_tnt
    buy_trinket("tnt", 100)
  end
  
  def buy_bulldozer
    buy_trinket("bulldozer", 500)
  end                  
  
  def buy_trinket(name, coins)
    puts name
    if !App::Persistence["#{$gcm.local_player_id}-coins"].nil?
      if App::Persistence["#{$gcm.local_player_id}-coins"] >= coins
        App::Persistence["#{$gcm.local_player_id}-coins"] -= coins
        hide_trinkets_menu
        if @trinket_for == "local"
          add_trinket_to_local(name)
        else
          add_trinket_to_opponent(name)
        end   
      else
        need_buy_coins_message     
      end
    else
      need_buy_coins_message    
    end  
  end      
  
  def need_buy_coins_message
    GameHelper.alert("Coins", "You don't have enough coins to buy this trinket. You can buy more coins in the shop.")
    puts "Player Dont Have Coins"
  end  
  
  def add_trinket_to_local(name, message=true)
    App::Persistence["#{$gcm.local_player_id}-lp-trinket"] = "#{name}-icon.png"

    trinket_btn = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 100) - ImageHelper.porcent_for(@screen_height, 9), ImageHelper.porcent_for(@screen_height, 64), ImageHelper.porcent_for(@screen_width, 14), ImageHelper.porcent_for(@screen_width, 14)))
    trinket_btn.image = UIImage.imageNamed(ImageHelper.name_for_phone("#{name}-icon.png")) 
    trinket_btn.contentMode = UIViewContentModeScaleAspectFill
    trinket_btn.layer.masksToBounds = true
    trinket_btn.layer.cornerRadius = trinket_btn.frame.size.width / 2
    $main_view.insertSubview(trinket_btn, atIndex:100)
    $main_view_objs << trinket_btn       
    
    $gcm.send_data_to_opponent("trinket:::opponent::#{name}") if message

  end  
  
  def add_trinket_to_opponent(name, message=true)
    App::Persistence["#{$op_id}-trinket"] = "#{name}-icon.png"
    
    $optrinket_btn = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 3), ImageHelper.porcent_for(@screen_height,20), ImageHelper.porcent_for(@screen_width, 14), ImageHelper.porcent_for(@screen_width, 14)))
    $optrinket_btn.image = UIImage.imageNamed(ImageHelper.name_for_phone("#{name}-icon.png")) 

    $optrinket_btn.contentMode = UIViewContentModeScaleAspectFill
    $optrinket_btn.layer.masksToBounds = true
    $optrinket_btn.layer.cornerRadius = $optrinket_btn.frame.size.width / 2
    $main_view.insertSubview($optrinket_btn, atIndex:100)
    $main_view_objs << $optrinket_btn       
    @buttons2 << [$optrinket_btn, "op_show_trinkets_menu-Call", self]   
    
    $gcm.send_data_to_opponent("trinket:::local::#{name}") if message
  end  
  
  def hide_trinkets_menu
    $tobjs.each{|to| to.delete_from_parent} 
    @trinkes_container.delete_from_parent
    @buttonsl = []
    $tobjs = []
    $s_trinket = false
  end  
  
  def add_opponent_elements
    @rect2 = MG::Draw.new
    @rect2 .rect([0, ImageHelper.top_porcent(22.9)], [$visible_size.width, $visible_size.height], [1.0, 1.0, 1.0, 1.0], true)
    add @rect2, 1  

    @coin_bar2 = MG::Sprite.new(ImageHelper.name_for_phone("coinbar.png"))
    @coin_bar2.position = [$visible_size.width - (@coin_bar2.size.width / 2), ImageHelper.top_porcent(8.6) - @coin_bar2.size.height / 2]
    add @coin_bar2, 1 
    
    $oppimage_container = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 5), ImageHelper.porcent_for(@screen_height, 8.6), ImageHelper.porcent_for(@screen_width, 11.5), ImageHelper.porcent_for(@screen_width, 11.5)))
    $oppimage_container.image = $gcm.opponent_player_photo
    $oppimage_container.layer.masksToBounds = true
    $oppimage_container.layer.cornerRadius = $oppimage_container.frame.size.width / 2
    $main_view.addSubview($oppimage_container)
    $main_view_objs << $oppimage_container   
     
    @title3o = MG::Text.new(opponent_player.name, 'OpenSans-Bold', 29)
    @title3o.area_size = [300, 40]      
    @title3o.position = [ImageHelper.left_porcent(17.8) + (@title3o.size.width / 2), ImageHelper.top_porcent(10.9)]
    @title3o.text_color = [0.0,0.0,0.0,1.0]
    @title3o.horizontal_align = :left
    add @title3o, 1
    
    @title4 = MG::Text.new("#{$op_score} pts", 'OpenSans-Light', 28)
    @title4.area_size = [200, 40]    
    @title4.position = [ImageHelper.left_porcent(17.8) + (@title4.size.width / 2), ImageHelper.top_porcent(13.3)]
    @title4.text_color = [0.0,0.0,0.0,1.0]
    @title4.horizontal_align = :left
    add @title4, 1 
    
    @title5 = MG::Text.new("#{0}", 'OpenSans-Bold', 28)
    @title5.area_size = @coin_bar2.size  
    @title5.position = [@coin_bar2.position.x + 20, @coin_bar2.position.y - (@coin_bar2.size.height / 4)]
    @title5.text_color = [1.0,1.0,1.0,1.0]
    @title5.horizontal_align = :left
    add @title5, 1 
    
    if $versus == "user"
      if App::Persistence["#{$op_id}-trinket"]
        $optrinket_btn = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 3), ImageHelper.porcent_for(@screen_height,20), ImageHelper.porcent_for(@screen_width, 14), ImageHelper.porcent_for(@screen_width, 14)))
        $optrinket_btn.image = UIImage.imageNamed(ImageHelper.name_for_phone(App::Persistence["#{$op_id}-trinket"])) 
      else  
        $optrinket_btn = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 11), ImageHelper.porcent_for(@screen_height, 22.5), ImageHelper.porcent_for(@screen_width, 8), ImageHelper.porcent_for(@screen_width, 8)))
        $optrinket_btn.image = UIImage.imageNamed(ImageHelper.name_for_phone("trinket-icon.png.png"))
      end
      $optrinket_btn.contentMode = UIViewContentModeScaleAspectFill
      $optrinket_btn.layer.masksToBounds = true
      $optrinket_btn.layer.cornerRadius = $optrinket_btn.frame.size.width / 2
      $main_view.insertSubview($optrinket_btn, atIndex:100)
      $main_view_objs << $optrinket_btn       
      @buttons2 << [$optrinket_btn, "op_show_trinkets_menu-Call", self]   
    end     
        
    
       
  end    
  
  def add_computer_elements
    @rect2 = MG::Draw.new
    @rect2 .rect([0, ImageHelper.top_porcent(22.9)], [$visible_size.width, $visible_size.height], [1.0, 1.0, 1.0, 1.0], true)
    add @rect2, 1  

    @coin_bar2 = MG::Sprite.new(ImageHelper.name_for_phone("coinbar.png"))
    @coin_bar2.position = [$visible_size.width - (@coin_bar2.size.width / 2), ImageHelper.top_porcent(8.6) - @coin_bar2.size.height / 2]
    add @coin_bar2, 1 
    
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 5), ImageHelper.porcent_for(@screen_height, 8.6), ImageHelper.porcent_for(@screen_width, 11.5), ImageHelper.porcent_for(@screen_width, 11.5)))
    image_container.image = UIImage.imageNamed("computer-iphone6.png")
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = image_container.frame.size.width / 2
    $main_view.addSubview(image_container)
    $main_view_objs << image_container   
     
    @title3c = MG::Text.new('Computer', 'OpenSans-Bold', 29)
    @title3c.area_size = [200, 40]      
    @title3c.position = [ImageHelper.left_porcent(17.8) + (@title3c.size.width / 2), ImageHelper.top_porcent(10.9)]
    @title3c.text_color = [0.0,0.0,0.0,1.0]
    @title3c.horizontal_align = :left
    add @title3c, 1
    
    @title4 = MG::Text.new('', 'OpenSans-Light', 28)
    @title4.area_size = [200, 40]    
    @title4.position = [ImageHelper.left_porcent(17.8) + (@title4.size.width / 2), ImageHelper.top_porcent(13.3)]
    @title4.text_color = [0.0,0.0,0.0,1.0]
    @title4.horizontal_align = :left
    add @title4, 1 
    
    @title5 = MG::Text.new('0 pts', 'OpenSans-Bold', 28)
    @title5.area_size = @coin_bar2.size  
    @title5.position = [@coin_bar2.position.x + 20, @coin_bar2.position.y - (@coin_bar2.size.height / 4)]
    @title5.text_color = [1.0,1.0,1.0,1.0]
    @title5.horizontal_align = :left
    add @title5, 1 

  end    
  
  def add_base_cards
    local_player_cards($game.local_player.hand)
    opponent_player_cards($game.opponent_player.hand)    
  end  
  
  def opponent_player_cards(cards)
    cards_pos = player_cards_position(18)

    cards.each_with_index do |c, i|
      @card = MG::Sprite.new("#{c.id}-2208-reg.png")
      @card.scale = (ImageHelper.porcent_for($visible_size.width, 15) / @card.size.width)
      @card.position = [cards_pos[i][0] + @card.size.width / 2, (cards_pos[i][1] - @card.size.height / 4)]
      add @card, 1

      @obcard = MG::Sprite.new("card-back-reg.png")
      @obcard.scale = (ImageHelper.porcent_for($visible_size.width, 15) / @obcard.size.width)
      @obcard.position = [cards_pos[i][0] + @obcard.size.width / 2, (cards_pos[i][1] - @obcard.size.height / 4)]
      add @obcard, 1

      $o_player_cards << @card
      $o_player_back_cards << [@obcard, c.id]
      $o_player_cards_objs << c
    end  
  end  
  
  def local_player_cards(cards)
    cards_pos = player_cards_position(74.5)
    
    cards.each_with_index do |c, i|
      @card = MG::Sprite.new("#{c.id}-2208-reg.png")
      @card.scale = (ImageHelper.porcent_for($visible_size.width, 15) / @card.size.width)
      @card.position = [cards_pos[i][0] + @card.size.width / 2, (cards_pos[i][1] - @card.size.height / 4) - 400]
      add @card, 3
      
      @bcards << [@card, 'select_card', c]
      $l_player_cards << @card
      $l_player_cards_objs << c
    end  
    deal_effect($l_player_cards)
  end  
  
  def original_position_of(card)
    cards_pos = player_cards_position(74.5)
    i = $l_player_cards.find_index(card)
    [cards_pos[i][0] + card.size.width / 2, (cards_pos[i][1] - card.size.height / 4)]
  end  
  
  def original_position_of_op(card)
    cards_pos = player_cards_position(18)
    i = $o_player_cards.find_index(card)
    [cards_pos[i][0] + card.size.width / 2, (cards_pos[i][1] - card.size.height / 4)]
  end  
  
  
  def deal_effect(cards)
    cards.each_with_index do |c,i|
      @deal_elements << [c, current_usecs + ((1000 * (i+1)) / 6), :top, false]
    end  
  end  
  
  def player_cards_position(top)
    top = ImageHelper.top_porcent(top)
    initial_left = ImageHelper.left_porcent(1)
    initial_left = ImageHelper.left_porcent(0.2) if $iphone_ver == "iphone5" or $iphone_ver == "iphone4"
    initial_left = ImageHelper.left_porcent(5) if $iphone_ver == "iphone6plus"
    (0..5).each_with_index.map{|n, i| [ initial_left + (ImageHelper.left_porcent(15) * i), top]}
  end  
  
  def add_board_positions
    btop = @board_bg.position.y + ImageHelper.porcent_for($visible_size.height, 6.5)
    bleft = 0
    
    add_pos_layout_on(bleft, btop)
    add_pos_layout_on(bleft + (p_size[0]), btop)
    add_pos_layout_on(bleft + (p_size[0] * 2), btop)
    add_pos_layout_on(bleft + (p_size[0] * 3), btop)
    add_pos_layout_on(bleft + (p_size[0] * 4), btop)   
    
    add_pos_layout_on(bleft, btop - p_size[1])
    add_pos_layout_on(bleft + (p_size[0]), btop - p_size[1])
    add_pos_layout_on(bleft + (p_size[0] * 2), btop - p_size[1])
    add_pos_layout_on(bleft + (p_size[0] * 3), btop - p_size[1])    
    add_pos_layout_on(bleft + (p_size[0] * 4), btop - p_size[1])    
    
    add_pos_layout_on(bleft, btop - p_size[1] * 2)
    add_pos_layout_on(bleft + (p_size[0]), btop - p_size[1] * 2)
    add_pos_layout_on(bleft + (p_size[0] * 2), btop - p_size[1] * 2)
    add_pos_layout_on(bleft + (p_size[0] * 3), btop - p_size[1] * 2)    
    add_pos_layout_on(bleft + (p_size[0] * 4), btop - p_size[1] * 2)         
  end  
  
  def add_pos_layout_on(x, y)
    l = MG::Layout.new
    l.size = p_size
    l.position = [x , y]
    
    add l, 7
    $board_positions << l
  end  
  
  # size of the positions on board
  def p_size
    [@board_bg.size.width / 5, @board_bg.size.height / 3]
  end  

end 
