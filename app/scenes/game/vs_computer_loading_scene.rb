class VsComputerLoadingScene < MG::Scene
  include SceneMethods
  
  def initialize
    @buttons = []
    GameHelper.add_background("main-bg.png", self)
    @screen_width = $main_view.frame.size.width
    @screen_height = $main_view.frame.size.height
    add_oponnets_selection
    add_labels
    $versus = "compu"
    $local_player = {:id => 1, :name => $gcm.display_name, :email => '', :points => $gcm.score}
    $opponent_player = {:id => 2, :name => 'Computer', :email => '', :points => 0}   
    $game = Game.new([$local_player, $opponent_player])
    App.run_after(2) { clean_and_start } 
  end
  
  def add_oponnets_selection
    add_vs_human
    add_vs_computer
  end  
  
  def add_vs_human
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(($main_view.frame.size.width / 2) - (ImageHelper.porcent_for(@screen_width, 35) / 2) - 8, ImageHelper.porcent_for(@screen_height, 60) - 3, ImageHelper.porcent_for(@screen_width, 37.5), ImageHelper.porcent_for(@screen_width, 37.5)))
    image_container.image = $gcm.local_player_photo
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = image_container.frame.size.width / 2
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    @buttons << [image_container, "StartScene-Clean"]  
    
    
    pic_frame = MG::Sprite.new(ImageHelper.name_for_phone("border.png"))
    pic_frame.position = [$visible_size.width / 2, ImageHelper.top_porcent(58) - (pic_frame.size.height / 2)]
    add pic_frame, 1
  end
  
  def add_vs_computer
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(ImageHelper.porcent_for(@screen_width, 30), ImageHelper.porcent_for(@screen_height, 14), ImageHelper.porcent_for(@screen_width, 40), ImageHelper.porcent_for(@screen_width, 40)))
    image_container.image = UIImage.imageNamed(ImageHelper.name_for_phone("computer.png"))
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = 25
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    @buttons << [image_container, "StartScene-Clean"]  
  end   
  
  def add_labels
    add_line_vs
    add_user_label
    add_computer_label
  end  

  def add_user_label
    title = MG::Text.new($gcm.alias, 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(85)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
    
    title = MG::Text.new("#{$gcm.score} pts", 'OpenSans-Light', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(89)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
  end
  
  def add_computer_label
    title = MG::Text.new('COMPUTER', 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(39)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
  end    

end