class WinnerScene < MG::Scene
  include SceneMethods  
  
  def initialize
    @buttons = []      
    @buttons_new_action3 = []
    GameHelper.add_background("main-bg.png", self)
    @screen_width = $main_view.frame.size.width
    @screen_height = $main_view.frame.size.height
    EM.cancel_timer($timeout_timer) if $timeout_timer
    check_archivements
    add_main_items
    send_game_center_info
    
    op_resumen = $game.opponent_player.player_points_resume.to_s.gsub("[", "").gsub("]", "").gsub('\\"', '').gsub('"', '')
    lp_resumen = $game.local_player.player_points_resume.to_s.gsub("[", "").gsub("]", "").gsub('\\"', '').gsub('"', '')
    op_resumen = " " if op_resumen == ""
    lp_resumen = " " if lp_resumen == ""
    
    ops = [$game.opponent_player.round_score, $game.opponent_player.filtered_cards_picked_up.count, "#{op_resumen}", $game.opponent_player.spades_count, $game.opponent_player.spades2? > 0]
    lps = [$game.local_player.round_score, $game.local_player.filtered_cards_picked_up.count, "#{lp_resumen}", $game.local_player.spades_count, $game.local_player.spades2? > 0]
        
    show_points_popup    
    
    on_touch_begin do |touch|
      if $grupo_visible
        check_menu_new_action3(touch) 
      else
        check_menu(touch)
      end  
    end        
  end
  
  def close_score
    mp "CLOSEEEE!!!"
    $grupo_visible = false
    $message_image_view_back2.removeFromSuperview if $message_image_view_back2
  end  
  
  def roman_round
    if $n_round == 1
      "I"
    elsif $n_round == 2
      "II"
    elsif $n_round == 3
      "III"      
    end
  end      
  
  def show_points_popup
    extra_i = [] 
    extra_i2 = [] 
    extra_top = 0
    extra_top2 = 0
    $message_image_view_back2 = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image_back2.image = UIImage.imageNamed(ImageHelper.name_for_phone("all_shadow.png"))        
   
   
    #Label Score
    label = UILabel.alloc.initWithFrame(CGRectMake(0, 1, @screen_width, 50))
    label.setText("SCORE ROUND #{roman_round}")
    label.setTextColor(UIColor.colorWithRed(181/255.0, green:51/255.0, blue:31/255.0, alpha:0.95))
    label.setTextAlignment(NSTextAlignmentCenter)
    label.setFont(UIFont.fontWithName("OpenSans-Light", size:18.0))
    
    
    #Label local player and opponent points
    label2 = UILabel.alloc.initWithFrame(CGRectMake(0, 32, @screen_width, 50))
    label2.setText("#{$game.local_player.round_score} - #{$game.opponent_player.round_score}")
    label2.setTextColor(UIColor.whiteColor)
    label2.setTextAlignment(NSTextAlignmentCenter)
    label2.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label2.font = label2.font.fontWithSize(30)
    
    #linea gris 1
    line1 = UIView.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 85, (@screen_width * 0.8), 1))
    line1.backgroundColor = UIColor.colorWithRed(73/255.0, green:73/255.0, blue:73/255.0, alpha:0.95)
    
    
    #Label stats
    label3 = UILabel.alloc.initWithFrame(CGRectMake(0, 100, @screen_width, 50))
    label3.setText("STATS")
    label3.setTextColor(UIColor.colorWithRed(181/255.0, green:51/255.0, blue:31/255.0, alpha:0.95))
    label3.setTextAlignment(NSTextAlignmentCenter)
    label3.setFont(UIFont.fontWithName("OpenSans-Light", size:18.0))
        
    
    
    ##### PLAYER 1 SECTION ######  
    
    player1_points = $game.local_player.player_points_resume
    
    #My Score title
    label4 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 135, @screen_width, 50))
    label4.setText("Your Score")
    label4.setTextColor(UIColor.whiteColor)
    label4.setTextAlignment(NSTextAlignmentLeft)
    label4.setFont(UIFont.fontWithName("OpenSans-Bold", size:18.0))
    label4.font = label4.font.fontWithSize(18)        
    
    label5 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 175, @screen_width, 50))
    label5.setText("Cards: #{$game.local_player.filtered_cards_picked_up.count}")
    label5.setTextColor(UIColor.whiteColor)
    label5.setTextAlignment(NSTextAlignmentLeft)
    label5.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label5.font = label5.font.fontWithSize(16)        
    
    
    pfirst = player1_points.first
    
    label6 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 210, @screen_width, 50))
    label6.setText("Points: #{pfirst}")
    label6.setTextColor(UIColor.whiteColor)
    label6.setTextAlignment(NSTextAlignmentLeft)
    label6.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label6.font = label6.font.fontWithSize(16)  
    
    player1_points.delete(pfirst)      

    if player1_points.count > 0
      label8 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 55), 230, @screen_width, 50))
      label8.setText(player1_points.last)
      label8.setTextColor(UIColor.whiteColor)
      label8.setTextAlignment(NSTextAlignmentLeft)
      label8.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label8.font = label8.font.fontWithSize(16)  
      extra_top += 15
      extra_i << label8
    end
        
    label7 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 245 + extra_top, @screen_width, 50))
    label7.setText("Spades: #{$game.local_player.spades_count}")
    label7.setTextColor(UIColor.whiteColor)
    label7.setTextAlignment(NSTextAlignmentLeft)
    label7.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label7.font = label7.font.fontWithSize(16)        
         
    if $game.local_player.spades2? > 0    
      label11 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 63), 265 + extra_top, @screen_width, 50))
      label11.setText("2 of Spades")
      label11.setTextColor(UIColor.whiteColor)
      label11.setTextAlignment(NSTextAlignmentLeft)
      label11.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label11.font = label11.font.fontWithSize(16)  
      extra_i << label11          
    end      
          
    #big total
    label9 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65), 210, @screen_width, 50))
    label9.setText("#{$game.local_player.round_score}")
    label9.setTextColor(UIColor.whiteColor)
    label9.setTextAlignment(NSTextAlignmentLeft)
    label9.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label9.font = label9.font.fontWithSize(30)
     
    label10 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65) + 35 , 215, @screen_width, 50))
    label10.setText("points")
    label10.setTextColor(UIColor.whiteColor)
    label10.setTextAlignment(NSTextAlignmentLeft)
    label10.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label10.font = label10.font.fontWithSize(16)    
      
    #linea gris 2
    line2 = UIView.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 330, (@screen_width * 0.8), 1))
    line2.backgroundColor = UIColor.colorWithRed(73/255.0, green:73/255.0, blue:73/255.0, alpha:0.95)
           
    
    ##### PLAYER 2 SECTION ######  
    
    pl2_top = 220
    
    
    player2_points = $game.opponent_player.player_points_resume
    
    #My Score title
    label42 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 135 + pl2_top, @screen_width, 50))
    label42.setText("Opponent Score")
    label42.setTextColor(UIColor.whiteColor)
    label42.setTextAlignment(NSTextAlignmentLeft)
    label42.setFont(UIFont.fontWithName("OpenSans-Bold", size:18.0))
    label42.font = label42.font.fontWithSize(18)        
    
    label52 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 175 + pl2_top, @screen_width, 50))
    label52.setText("Cards: #{$game.opponent_player.filtered_cards_picked_up.count}")
    label52.setTextColor(UIColor.whiteColor)
    label52.setTextAlignment(NSTextAlignmentLeft)
    label52.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label52.font = label52.font.fontWithSize(16)        
    
    
    pfirst2 = player2_points.first
    
    label62 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 210 + pl2_top, @screen_width, 50))
    label62.setText("Points: #{pfirst2}")
    label62.setTextColor(UIColor.whiteColor)
    label62.setTextAlignment(NSTextAlignmentLeft)
    label62.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label62.font = label62.font.fontWithSize(16)  

    player2_points.delete(pfirst2)      

    if player2_points.count > 0
 
      label82 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 55), 230 + pl2_top, @screen_width, 50))
      label82.setText(player2_points.last)
      label82.setTextColor(UIColor.whiteColor)
      label82.setTextAlignment(NSTextAlignmentLeft)
      label82.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label82.font = label82.font.fontWithSize(16)  
      extra_top2 += 15
      extra_i2 << label82
    end
        
    label72 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.1), 245 + extra_top2 + pl2_top, @screen_width, 50))
    label72.setText("Spades: #{$game.opponent_player.spades_count}")
    label72.setTextColor(UIColor.whiteColor)
    label72.setTextAlignment(NSTextAlignmentLeft)
    label72.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
    label72.font = label72.font.fontWithSize(16)        
          
          
    if $game.opponent_player.spades2? > 0     
      label112 = UILabel.alloc.initWithFrame(CGRectMake(((@screen_width * 0.1) + 63), 265 + extra_top2 + pl2_top, @screen_width, 50))
      label112.setText("2 of Spades")
      label112.setTextColor(UIColor.whiteColor)
      label112.setTextAlignment(NSTextAlignmentLeft)
      label112.setFont(UIFont.fontWithName("OpenSans-Regular", size:16.0))
      label112.font = label112.font.fontWithSize(16)  
      extra_i2 << label112          
    end         
          
    #big total
    label92 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65), 210 + pl2_top, @screen_width, 50))
    label92.setText("#{$game.opponent_player.round_score}")
    label92.setTextColor(UIColor.whiteColor)
    label92.setTextAlignment(NSTextAlignmentLeft)
    label92.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label92.font = label92.font.fontWithSize(30)
     
    label102 = UILabel.alloc.initWithFrame(CGRectMake((@screen_width * 0.65) + 35 , 215 + pl2_top, @screen_width, 50))
    label102.setText("points")
    label102.setTextColor(UIColor.whiteColor)
    label102.setTextAlignment(NSTextAlignmentLeft)
    label102.setFont(UIFont.fontWithName("OpenSans-Bold", size:40.0))
    label102.font = label102.font.fontWithSize(16)    
      
      
    @close_b = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, 50, 50))
    @close_b.image = UIImage.imageNamed("cancel-iphone6plus.png") 
    @close_b.position = [($main_view.frame.size.width * 0.87), ($main_view.frame.size.height * 0.92)]
    
    @buttons_new_action3 << [@close_b, "close_score-Call", self]      
          
    items = [$message_image_back2, label, label2, line1, line2, label3, label4, label5, label6, label7, label9, label10, extra_i, extra_i2, label42, label52, label62, label72, label92, label102, @close_b].flatten 
    items.each{|i| $message_image_view_back2.addSubview(i)}         
   
                  
    $main_view.addSubview($message_image_view_back2) 
    
    $grupo_visible = true   
  end  
    
  
  def add_main_items
    if is_winner?
      add_winner_items
    else
      add_loser_items
    end    
  end  
  
  def is_winner?
    if $opponent_score
      $local_score > $opponent_score
    else
      $game.winner? == $game.local_player
    end    
  end  
  
  def add_winner_items
    title = MG::Sprite.new(ImageHelper.name_for_phone("congrat.png"))
    title.position = [ImageHelper.left_porcent(0) + (title.size.width / 2), ImageHelper.top_porcent(20) + (title.size.height / 2)]
    add title, 1 

    score = MG::Text.new("You've scored #{$local_score ? $local_score : $game.local_player.points} points", 'OpenSans-Regular', 40)
    score.position = [$visible_size.width / 2, ImageHelper.top_porcent(26)]
    score.text_color = [1.0,1.0,1.0,1.0]
    add score, 1
    
    add_users
  end
  
  def add_loser_items
    title = MG::Sprite.new(ImageHelper.name_for_phone("sorry.png"))
    title.position = [ImageHelper.left_porcent(0) + (title.size.width / 2), ImageHelper.top_porcent(20) + (title.size.height / 2)]
    add title, 1 
    
    score = MG::Text.new("You've scored #{$local_score ? $local_score : $game.local_player.points} points", 'OpenSans-Regular', 40)
    score.position = [$visible_size.width / 2, ImageHelper.top_porcent(26)]
    score.text_color = [1.0,1.0,1.0,1.0]
    add score, 1
    
    add_users
  end     
  
  def add_users
    vs = MG::Sprite.new(ImageHelper.name_for_phone("vs.png"))
    vs.position = [ImageHelper.left_porcent(0) + (vs.size.width / 2), ImageHelper.top_porcent(49) + (vs.size.height / 2)]
    add vs, 1 
    
    local_player_img = UIImageView.alloc.initWithFrame(CGRectMake((ImageHelper.porcent_for(@screen_width, 30)) - 8, (ImageHelper.porcent_for(@screen_height, 48) - (ImageHelper.porcent_for(@screen_width, 7.5))), ImageHelper.porcent_for(@screen_width, 15), ImageHelper.porcent_for(@screen_width, 15)))
    local_player_img.image = $gcm.local_player_photo
    local_player_img.layer.masksToBounds = true
    local_player_img.layer.cornerRadius = local_player_img.frame.size.width / 2
    $main_view.addSubview(local_player_img)
    $main_view_objs << local_player_img    
    
    local_player_title = MG::Text.new($gcm.alias, 'OpenSans-Bold', 20)
    local_player_title.position = [ImageHelper.left_porcent(35.5), ImageHelper.top_porcent(54)]
    local_player_title.text_color = [1.0,1.0,1.0,1.0]
    add local_player_title, 1
    
    local_player_score = MG::Text.new("#{$local_score ? $local_score : $game.local_player.points} pts", 'OpenSans-Regular', 20)
    local_player_score.position = [ImageHelper.left_porcent(35.5), ImageHelper.top_porcent(56)]
    local_player_score.text_color = [1.0,1.0,1.0,1.0]
    add local_player_score, 1
        
    
    opponent_player_img = UIImageView.alloc.initWithFrame(CGRectMake((ImageHelper.porcent_for(@screen_width, 60)) - 8, (ImageHelper.porcent_for(@screen_height, 48) - (ImageHelper.porcent_for(@screen_width, 7.5))), ImageHelper.porcent_for(@screen_width, 15), ImageHelper.porcent_for(@screen_width, 15)))
    opponent_player_img.image = $versus == "user" ? $gcm.opponent_player_photo : UIImage.imageNamed("computer-iphone6.png")
    opponent_player_img.layer.masksToBounds = true
    opponent_player_img.layer.cornerRadius = opponent_player_img.frame.size.width / 2
    $main_view.addSubview(opponent_player_img)
    $main_view_objs << opponent_player_img     
    
    opponent_player_title = MG::Text.new( $versus == "user" ? $gcm.opponent_player.alias : "Computer", 'OpenSans-Bold', 20)
    opponent_player_title.position = [ImageHelper.left_porcent(65.5), ImageHelper.top_porcent(54)]
    opponent_player_title.text_color = [1.0,1.0,1.0,1.0]
    add opponent_player_title, 1
         
    opponent_player_score = MG::Text.new("#{$opponent_score ? $opponent_score : $game.opponent_player.points} pts", 'OpenSans-Regular', 20)
    opponent_player_score.position = [ImageHelper.left_porcent(65.5), ImageHelper.top_porcent(56)]
    opponent_player_score.text_color = [1.0,1.0,1.0,1.0]
    add opponent_player_score, 1         

    line = MG::Sprite.new(ImageHelper.name_for_phone("won_line.png"))
    line.position = [ImageHelper.left_porcent(0) + (line.size.width / 2), ImageHelper.top_porcent(66) + (line.size.height / 2)]
    add line, 1 
    
    play = MG::Sprite.new(ImageHelper.name_for_phone("play_againg.png"))
    play.position = [ImageHelper.left_porcent(0) + (play.size.width / 2), ImageHelper.top_porcent(100) + (play.size.height / 2)]
    add play, 1 

    action_button = MG::Text.new(' ', 'OpenSans-Bold', 29)
    action_button.area_size = [play .size.width, play .size.height]
    action_button.position = [play .position.x, play .position.y]
    add action_button, 3
    
    @buttons << [action_button, 'StartScene-Clean', self]
  end    
  
  def send_game_center_info
    $gcm.disconnect
  end
  
  def check_archivements
    update_archivements
    activate_archivements
  end    
  
  def update_archivements
    App::Persistence["#{$gcm.local_player_id}-games"] += 1
    App::Persistence["#{$gcm.local_player_id}-winsrow"] = is_winner? ? App::Persistence["#{$gcm.local_player_id}-winsrow"] + 1 : 0
    App::Persistence["#{$gcm.local_player_id}-trashrow"] = $game.local_player.points == 0 ? App::Persistence["#{$gcm.local_player_id}-trashrow"] + 1 : 0
  end
  
  def activate_archivements
    #Adiction 100
    if arch_active?("games") == false and arch_val("games") >= 100
      $gcm.complete_achivement("addiction01")
    end
    
    #Addiction 250
    if arch_active?("games2") == false and arch_val("games") >= 250
      $gcm.complete_achivement("addiction02")
    end   
    
    #Master Builder
    if arch_active?("winsrow") == false and arch_val("winsrow") >= 10
      $gcm.complete_achivement("master")
    end   
    
    #Deluxe Devastation
    if arch_active?("udestructions") == false and arch_val("udestructions") >= 10
      $gcm.complete_achivement("deluxe")
    end   
    
    #Card Hog
    if arch_active?("mostcardsrow") == false and arch_val("mostcardsrow") >= 5
      $gcm.complete_achivement("card_hog")
    end   
      
    #Card Hog
    if arch_active?("trashrow") == false and arch_val("trashrow") >= 10
      $gcm.complete_achivement("trash_can")
    end     
  end    
  
  def arch_val(arc)
    App::Persistence["#{$gcm.local_player_id}-#{arc}"]
  end
  
  def arch_active?(arc)
    App::Persistence["#{$gcm.local_player_id}-#{arc}-active"]
  end  
end  