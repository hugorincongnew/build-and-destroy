class VsUserLoadingScene < MG::Scene
  include SceneMethods
  
  def initialize
    @buttons = []
    GameHelper.add_background("main-bg.png", self)
    @screen_width = $main_view.frame.size.width
    @screen_height = $main_view.frame.size.height
    add_oponnets_selection
    add_labels
    $versus = "user" 
    App.run_after(3) { clean_and_start } 
  end
  
  def add_oponnets_selection
    add_vs_human1
    add_vs_human2
  end  
  
  def add_vs_human1
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(($main_view.frame.size.width / 2) - (ImageHelper.porcent_for(@screen_width, 35) / 2) - 8, ImageHelper.porcent_for(@screen_height, 10) - 3, ImageHelper.porcent_for(@screen_width, 37.5), ImageHelper.porcent_for(@screen_width, 37.5)))
    image_container.image = $gcm.opponent_player_photo
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = image_container.frame.size.width / 2
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    @buttons << [image_container, "StartScene-Clean"]  
    
    pic_frame = MG::Sprite.new(ImageHelper.name_for_phone("border.png"))
    pic_frame.position = [$visible_size.width / 2, ImageHelper.top_porcent(8) - (pic_frame.size.height / 2)]
    add pic_frame, 1
  end 
  
  def add_vs_human2
    image_container = UIImageView.alloc.initWithFrame(CGRectMake(($main_view.frame.size.width / 2) - (ImageHelper.porcent_for(@screen_width, 35) / 2) - 8, ImageHelper.porcent_for(@screen_height, 60) - 3, ImageHelper.porcent_for(@screen_width, 37.5), ImageHelper.porcent_for(@screen_width, 37.5)))
    image_container.image = $gcm.local_player_photo
    image_container.layer.masksToBounds = true
    image_container.layer.cornerRadius = image_container.frame.size.width / 2
    $main_view.addSubview(image_container)
    $main_view_objs << image_container
    @buttons << [image_container, "StartScene-Clean"]  
    
    pic_frame = MG::Sprite.new(ImageHelper.name_for_phone("border.png"))
    pic_frame.position = [$visible_size.width / 2, ImageHelper.top_porcent(58) - (pic_frame.size.height / 2)]
    add pic_frame, 1
  end   
  
  def add_labels
    add_line_vs
    add_user1_label
    add_user2_label
  end  

  def add_user1_label
    title = MG::Text.new($gcm.opponent_player.alias, 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(35)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
    
    title = MG::Text.new("#{$op_score} pts", 'OpenSans-Light', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(39)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
  end
  
  def add_user2_label
    title = MG::Text.new($gcm.alias, 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(85)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
    
    title = MG::Text.new("#{$gcm.score} pts", 'OpenSans-Light', 46)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(89)]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title
  end    

end