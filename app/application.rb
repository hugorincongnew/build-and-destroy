class Application < MG::Application
  def start
    
    BITHockeyManager.sharedHockeyManager.configureWithIdentifier("173ef35d17314963bf1c04e80dd222fa")
    BITHockeyManager.sharedHockeyManager.startManager 
    BITHockeyManager.sharedHockeyManager.authenticator.authenticateInstallation
    
    load_notifications
    $director = MG::Director.shared
    $visible_size = $director.size    
    $iphone_ver = iphone_model
    
    $gcm = GameCenterManager.new()
    $gcm.start_connect
    
    $main_view = $director._get_glview
    $root = $main_view.superview.rootViewController
    $main_view_objs = []
    $music = SoundHelper.main_background_music(true)
 
    $director.run(StartScene.new)
  end
  
  def load_notifications
    NSNotificationCenter.defaultCenter.addObserver(
      self,
      selector:"appWillResignActive:",
      name:UIApplicationWillResignActiveNotification,
      object:nil
    )
    NSNotificationCenter.defaultCenter.addObserver(
      self,
      selector:"appWillEnterForeground:",
      name:UIApplicationWillEnterForegroundNotification,
      object:nil
    )
  end  
  
  def appWillEnterForeground(notification)
    NSLog("!!!!!! WillEnterForeground")
    $director.resume
    $director.start_animation
  end

  def appWillResignActive(notification)
    NSLog("!!!!!! WillResignActive")
    $director.pause
    $director.stop_animation
  end  
  
  def iphone_model
    case MG::Director.shared.size.height.to_i
      when 960 #640x960
        "iphone4" 
      when 1136 #640x1136
        "iphone5"       
      when 1334 #750x1334
        "iphone6"          
      when 2208 #1242x2208
        "iphone6plus"
      else
        "iphone6plus"
    end        
  end 
  
end
