class GameCenterManager

  def initialize
    @reqSysVer = 4.1
    $score = nil
    $coins = nil
    @match = nil
    start_connect
    @userAuthenticated = false
    @lp_photo = nil
    @op_photo = nil
    if self.isGameCenterAvailable?
      self.authenticateLocalUser
    end  
  end  
  
  def local_player_photo
    @lp_photo || UIImage.imageNamed("user-default.png")
  end  
  
  def opponent_player_photo
    @op_photo || UIImage.imageNamed("user-default.png")
  end  
  
  def start_connect
    if isGameCenterAvailable?
      @auth_observer = App.notification_center.observe GKPlayerAuthenticationDidChangeNotificationName do |notification|
        authenticationChanged
      end
    end  
  end  
  
  def isGameCenterAvailable?
    gameCenterAvailableForSysVer(current_system_version)
  end  
  
  def player_logged?
    GKLocalPlayer.localPlayer.isAuthenticated
  end  
  
  def local_player
    GKLocalPlayer.localPlayer
  end  
  
  def opponent_player
    if @gmatch
      @gmatch.players.last
    else
      local_player
    end    
  end  
  
  def disconnect
    @gmatch.disconnect if @gmatch
  end  
  
  def leave(data)
    $game_scene.exit_message
  end  
  
  def timeout(data)
    $game_scene.exit_message_time_out
  end  
  
  def local_player_id
    pidd = GKLocalPlayer.localPlayer.playerID
    pidd ? pidd.split(":")[1] : "12345"
  end  
  
  def update_archivements
    GKAchievement.loadAchievementsWithCompletionHandler(proc{|achievements, error|
      if error
        puts "Error Getting Archivements from Game Center"
        puts "Error Code: #{error.code}"
        puts "Error Info: #{error.userInfo}"
      end  
      
      if achievements
        achievements.each do |a| 
          case a.identifier
          when "addiction01" 
            set_arch_val("games")
          when "addiction02" 
            set_arch_val("games2")
          when "master" 
            set_arch_val("winsrow")
          when "deluxe" 
            set_arch_val("udestructions")
          when "card_hog" 
            set_arch_val("mostcardsrow")
          when "trash_can" 
            set_arch_val("trashrow")
          end
        end  
      end  
    })
  end  
  
  def set_arch_val(arc)
    App::Persistence["#{$gcm.local_player_id}-#{arc}-active"] = true
  end
  
  def complete_achivement(name)
    a = GKAchievement.alloc.initWithIdentifier(name)
    a.showsCompletionBanner
    a.percentComplete = 100

    GKAchievement.reportAchievements([a], withCompletionHandler:proc{|error| 
      if error
        puts "Error Setting Archivements from Game Center"
        puts "Error Code: #{error.code}"
        puts "Error Info: #{error.userInfo}"
      else
        puts "Reported"
      end  
    })
  end  
  
  def authenticateLocalUser
    if !player_logged?
      GKLocalPlayer.localPlayer.authenticateWithCompletionHandler(proc{|error|
        if error
          puts "Error Authenticating on Game Center"
          puts "Error Code: #{error.code}"
          puts "Error Info: #{error.userInfo}"
        else
          puts "Authentication success on Game Center"
          check_base_score
          check_archivements
          update_archivements
          App::Persistence["#{$gcm.local_player_id}-coins"] = 50000 if App::Persistence["#{$gcm.local_player_id}-coins"].nil?
          GKLocalPlayer.localPlayer.registerListener(self)
          local_player.loadPhotoForSize(GKPhotoSizeSmall, withCompletionHandler:proc{|photo, error|  
            if photo
              @lp_photo = photo  
            end
            if error
              mp error 
            end  
          }) 
        end        
      })
    end  
  end  
  
  def check_archivements
    App::Persistence["#{local_player_id}-games"] = App::Persistence["#{local_player_id}-games"].nil? ? 0 : App::Persistence["#{local_player_id}-games"]
    App::Persistence["#{local_player_id}-winsrow"] = App::Persistence["#{local_player_id}-winsrow"].nil? ? 0 : App::Persistence["#{local_player_id}-winsrow"]
    App::Persistence["#{local_player_id}-udestructions"] = App::Persistence["#{local_player_id}-udestructions"].nil? ? 0 : App::Persistence["#{local_player_id}-udestructions"]
    App::Persistence["#{local_player_id}-mostcardsrow"] = App::Persistence["#{local_player_id}-mostcardsrow"].nil? ? 0 : App::Persistence["#{local_player_id}-mostcardsrow"]            
    App::Persistence["#{local_player_id}-trashrow"] = App::Persistence["#{local_player_id}-trashrow"].nil? ? 0 : App::Persistence["#{local_player_id}-trashrow"] 

    App::Persistence["#{local_player_id}-games-active"] = App::Persistence["#{$gcm.local_player_id}-games-active"].nil? ? false : App::Persistence["#{$gcm.local_player_id}-games-active"]
    App::Persistence["#{local_player_id}-games2-active"] = App::Persistence["#{$gcm.local_player_id}-games2-active"].nil? ? false : App::Persistence["#{$gcm.local_player_id}-games2-active"]
    App::Persistence["#{local_player_id}-winsrow-active"] = App::Persistence["#{$gcm.local_player_id}-winsrow-active"].nil? ? false : App::Persistence["#{$gcm.local_player_id}-winsrow-active"]
    App::Persistence["#{local_player_id}-udestructions-active"] = App::Persistence["#{$gcm.local_player_id}-udestructions-active"].nil? ? false : App::Persistence["#{$gcm.local_player_id}-udestructions-active"]
    App::Persistence["#{local_player_id}-mostcardsrow-active"] = App::Persistence["#{$gcm.local_player_id}-mostcardsrow-active"].nil? ? false : App::Persistence["#{$gcm.local_player_id}-mostcardsrow-active"]
    App::Persistence["#{local_player_id}-trashrow-active"] = App::Persistence["#{$gcm.local_player_id}-trashrow-active"].nil? ? false : App::Persistence["#{$gcm.local_player_id}-trashrow-active"]
  end  
  
  def check_base_score
    current_score
    timer = EM.add_periodic_timer 0.5 do
      if $score != nil 
        if $score == 0
          report_score(1) 
          $score = 1
        end  
        puts "Local Player Score #{$score}" 
        EM.cancel_timer(timer) 
        
      end  
    end
  end  
  
  def check_base_coins
    current_coins
    timer = EM.add_periodic_timer 0.5 do
      if $coins != nil 
        if $coins == 1
          report_coins(500000) 
          $coins = 1
        end  
        puts "Local Player Coins #{$coins}" 
        EM.cancel_timer(timer) 
        
      end  
    end
  end  
  
  def report_score(val)
    score =  GKScore.alloc.initWithLeaderboardIdentifier("global_score")
    score.value = val
    score.reportScoreWithCompletionHandler(proc{|error|
      if error
        puts 'Error reporting score' 
      else
        puts "Score Reported"
      end 
    })    
  end  
  
  def report_coins(val)
    coins =  GKScore.alloc.initWithLeaderboardIdentifier("Coins")
    coins.value = val
    score =  GKScore.alloc.initWithLeaderboardIdentifier("global_score")
    score.value = val
    GKScore.reportScores([coins], withCompletionHandler:proc{|error|
        if error
          puts 'Error reporting coins'
        else
          puts "Coins Reported (#{val})"  
        end
    })
    
    #coins.reportScoreWithCompletionHandler(proc{|error|
    #  if error
    #    puts 'Error reporting coins' 
    #  else
    #    puts "Coins Reported (#{val})"
    #  end 
    #})    
  end    
  
  def current_score
    lb = GKLeaderboard.alloc.init
    lb.identifier = "global_score"
    lb.loadScoresWithCompletionHandler(proc{|scores, error|
      if error
        puts 'Error getting score'
      else
        sc = lb.localPlayerScore
        $score = sc ? sc.value.to_i : 0
      end 
    })     
  end  
  
  def current_coins
    lb = GKLeaderboard.alloc.init
    lb.identifier = "Coins"
    lb.loadScoresWithCompletionHandler(proc{|coins, error|
      if error
        puts 'Error getting coins'
      else
        sc = lb.localPlayerScore
        $coins = sc ? sc.value.to_i : 0
      end 
    })     
  end   
  
  def display_name
    GKLocalPlayer.localPlayer.displayName
  end  
  
  def score
    $score
  end  
  
  def alias
    GKLocalPlayer.localPlayer.alias
  end  
  
  def local_foto
    GKLocalPlayer.localPlayer.loadPhotoForSize(GKPhotoSizeSmall, withCompletionHandler:(proc{|photo, error| 
      if error
        mp errr.code
        mp error.userInfo
      else
        mp photo
      end  
    }))
  end  
  
  
  #================= Matching  


  def player(player, didAcceptInvite:invite)
    NSLog("accepted invite #{player.playerID}");
 
    mmvc = GKMatchmakerViewController.alloc.initWithInvite(invite)
    mmvc.matchmakerDelegate = self
 
    showMatchmaker(mmvc)    
  end  
  
  def player(player, didRequestMatchWithPlayers:players)
    NSLog("request!!!!!")
  end  
  
  def findMatch(scene)
    @scene = scene
    request = GKMatchRequest.alloc.init
    request.minPlayers = 2
    request.maxPlayers = 2
 
    mmvc = GKMatchmakerViewController.alloc.initWithMatchRequest(request)
    mmvc.matchmakerDelegate = self
 
    showMatchmaker(mmvc)
  end  
  
  def showMatchmaker(ctrl)
    puts 'show'
    $my_num = Random.rand(999999)
    if @matchMakerController == nil
      $root.presentViewController(ctrl, animated:true, completion:nil)
      @matchMakerController = ctrl
    end
  end
  
  def hideMatchmaker 
    puts 'hide'
    if @matchMakerController
      @matchMakerController.delegate = nil
      @matchMakerController.dismissViewControllerAnimated(true, completion:nil)
      @matchMakerController = nil;
    end
  end
  
  def matchmakerViewController(viewController, didFindMatch:match)
    puts "didFindMatch"
    if @matchMakerController
      hideMatchmaker
      launchMatch(match)
    end
  end    
  
  def launchMatch(match)
    @gmatch = match
    if @gmatch.players.last
      $op_id = @gmatch.players.last.playerID
    else
      $op_id = "invalid"
    end    
    puts $op_id
    
    @gmatch.delegate = self
    
    opponent_player.loadPhotoForSize(GKPhotoSizeSmall, withCompletionHandler:proc{|photo, error|  
      if photo
        @op_photo = photo  
      end
      if error
        mp error 
      end  
      
      puts "launchMatch"
      send_data_to_opponent("start:::#{$my_num}:::#{$gcm.score}")
    }) 
  end  
  
  def matchmakerViewController(viewController, didFailWithError:error)
    puts "didFailWithError"
    mp error
    clean_match
  end
  
  def matchmakerViewControllerWasCancelled(viewController)
    mp "Match Was Cancelled"
    clean_match
  end
  
  
  def clean_match
    hideMatchmaker
    @gmatch = nil
  end  
  
  def send_data_to_opponent(data)
    $tmp_data = data
    data = data.nsdata
    EM.cancel_timer($receive_timer) if $receive_timer
    if data.to_s != "received:::true"
      $receive_timer = EM.add_timer(10) do
        resent_data
      end  
    end  
    @gmatch.sendData(data, toPlayers:@gmatch.players, dataMode:GKMatchSendDataReliable, error:nil)
  end  
  
  def send_confirm
    send_data_to_opponent("received:::true")
  end  
  
  def resent_data
     send_data_to_opponent($tmp_data)
  end  
  
  def received(data)
    EM.cancel_timer($receive_timer) if $receive_timer
  end
  
  def match(match, didReceiveData:data, fromRemotePlayer:player)
    NSLog("Incoming Data!!!")
    NSLog(data.to_s)
    if data.to_s != "received:::true"
      send_confirm 
    else
      received("")
    end    
    read_data(data)
  end  
  
  def read_data(data)
    data = data.to_s.split(':::')
    if data.count == 4
     self.send(data[0], data[1], data[2], data[3]) 
    elsif data.count == 3      
      self.send(data[0], data[1], data[2]) 
    elsif data.count == 2
      self.send(data[0], data[1]) 
    elsif data.count == 1  
      self.send(data[0])
    else
      NSLog("Error Start")  
      NSLog(data.to_s)
      NSLog("Error End")        
    end  
  end  
  
  def start(val, op_score)
    $op_score = op_score
    if $my_num.to_i > val.to_i
      NSLog("*"*80)
      NSLog("#{$my_num}")
      NSLog("#{val}")
      NSLog("Starting the game")
      NSLog("*"*80)  
      start_game 
    else
      NSLog("*"*80)
      NSLog("Opponent Starting the game")
      NSLog("#{$my_num.to_i}")      
      NSLog("*"*80)      
    end    
  end  
  
  def start_game
    $local_player = {:id => 1, :name => $gcm.display_name, :email => '', :points => $gcm.score}
    $opponent_player = {:id => 2, :name => $gcm.opponent_player.displayName, :email => '', :points => 0}   
    $game = Game.new([$local_player, $opponent_player])
    
    $main_view_objs.each{|o| o.removeFromSuperview}
    
    send_data_to_opponent("start_game_with_data:::#{game_data}")
    $director.replace(VsUserLoadingScene.new)
  end  
  
  def trinket(data)
    data = data.split("::")
    
    if data[0] == "local"
      $game_scene.add_trinket_to_local(data[1], false)
    else
      $game_scene.add_trinket_to_opponent(data[1], false)
    end    
  end  
  
  def opponent_action(data)
    data = data.split("::")
    NSLog(data[0].to_s) #action
    NSLog(data[1].to_s) #card_id
    NSLog(data[2].to_s) #position
    if data[6]
      $game_scene.send("o#{data[0]}", data[1].to_i, data[2].to_i, data[3],  data[4],  data[5].to_i,  data[6])
    elsif  data[5]
      $game_scene.send("o#{data[0]}", data[1].to_i, data[2].to_i, data[3],  data[4],  data[5].to_i)
    elsif data[4]
      $game_scene.send("o#{data[0]}", data[1].to_i, data[2].to_i, data[3],  data[4])      
    elsif data[3]
      $game_scene.send("o#{data[0]}", data[1].to_i, data[2].to_i, data[3])
    else  
      $game_scene.send("o#{data[0]}", data[1].to_i, data[2].to_i)
    end  
  end  
  
  def start_game_with_data(data)
    data = data.split("::")
  
    $local_player = {:id => 2, :name => $gcm.display_name, :email => '', :points => $gcm.score}
    $opponent_player = {:id => 1, :name => $gcm.opponent_player.displayName, :email => '', :points => 0}   
    $game = Game.new([$local_player, $opponent_player])
    
    if data[3] == "true"
      if $game.players.first == $game.local_player
        $game.players = $game.players.reverse
      end  
    else
      if $game.players.first == $game.opponent_player
        $game.players = $game.players.reverse
      end   
    end        
    
    update_game_cards(data)
      
    $main_view_objs.each{|o| o.removeFromSuperview}
    $director.replace(VsUserLoadingScene.new)
  end  
  
  def winner(data)
    data = data.split("::")
    $opponent_score = data[1].to_i
    $local_score = data[2].to_i    
    $main_view_objs.each{|o| o.removeFromSuperview}
    $director.replace(WinnerScene.new)
  end  
  
  def game_data
    my_hand = $game.local_player.hand.map{|h| h.id}
    my_cards_pu = $game.local_player.cards_picked_up.map{|board_card| board_card.card.id if board_card.card}
    my_cards_pu = my_cards_pu.count == 0 ? [0] : my_cards_pu
    o_cards_pu = $game.opponent_player.cards_picked_up.map{|board_card| board_card.card.id if board_card.card}
    o_cards_pu  = o_cards_pu .count == 0 ? [0] : o_cards_pu   
    opponent_hand = $game.opponent_player.hand.map{|h| h.id}
    deck_cards = $game.deck.cards.map{|h| h.id}
    deck_cards = "0" if deck_cards == []
    bocards = $game.board_cards.inject(""){|r, bc| r+= "#{bc.card.id}|#{bc.position}|#{bc.player == $game.local_player ? 0 : 1}?"}
    bocards = " " if bocards = ""
    "#{my_hand.to_s.gsub("[", "").gsub("]", "")}::#{opponent_hand.to_s.gsub("[", "").gsub("]", "")}::#{deck_cards.to_s.gsub("[", "").gsub("]", "")}::#{$game.players.first == $game.local_player}::#{my_cards_pu.to_s.gsub("[", "").gsub("]", "")}::#{o_cards_pu.to_s.gsub("[", "").gsub("]", "")}::#{bocards.to_s.gsub("[", "").gsub("]", "")}::#{$game.local_player.points}::#{$game.opponent_player.points}"
  end      
  
  def update_game_cards(data)  
    op_hand_ids = data[0].split(",").map{|f| f.to_i}
    lo_hand_ids = data[1].split(",").map{|f| f.to_i}
    deck_ids = data[2].split(",").map{|f| f.to_i}
    my_cards_pu = data[3].split(",").map{|f| f.to_i}
    o_cards_pu = data[4].split(",").map{|f| f.to_i} 
    b_cards = data[6].to_s.split("?")
    $game.opponent_player.points = data[7].to_i
    $game.local_player.points = data[8].to_i
     
    op_card = []
    op_hand_ids.each{|c| op_card << $all_cards.select{|ac| ac.id == c.to_i}.last}
    $game.opponent_player.hand = op_card
   
    op_card_pu = []
    if o_cards_pu != 0  
      o_cards_pu.each{|c| op_card_pu << BoardCard.new($all_cards.select{|ac| ac.id == c.to_i}.last, nil, nil, nil)}
      $game.opponent_player.cards_picked_up = op_card_pu
    else
      $game.opponent_player.cards_picked_up = []
    end
    
    lp_card_pu = []
    if my_cards_pu != 0
      my_cards_pu.each{|c| lp_card_pu <<  BoardCard.new($all_cards.select{|ac| ac.id == c.to_i}.last, nil, nil, nil)}
      $game.local_player.cards_picked_up = lp_card_pu    
    else
      $game.local_player.cards_picked_up = []
    end  
    lp_card = []
    lo_hand_ids.each{|c| lp_card << $all_cards.select{|ac| ac.id == c.to_i}.last}    
    $game.local_player.hand = lp_card
    
    board_cards = []  
    if b_cards == 0 or b_cards.nil?
      $game.board_cards = []
    else
      b_cards.each do |bc|  
        items = bc.split("|")
        player = items[2].to_i == 0 ? $game.opponent_player : $game.local_player
        board_cards << BoardCard.new($all_cards.select{|ac| ac.id == items[0].to_i}.last, player, items[1].to_i, nil)
      end 
    end
    
    deck_cards = [] 
    if deck_ids == 0        
      $game.deck.cards = []    
    else
      deck_ids.each{|c| deck_cards << $all_cards.select{|ac| ac.id == c.to_i}.last}        
      $game.deck.cards = deck_cards    
    end    
  end  

  def new_round(data)
    $round_count = 0 if $round_count.nil?
    data = data.split("::")
    
    mp "NEW ROUND!!!"
    $game.deal_cards?
    update_game_cards(data)
    $game_scene.add_base_cards
    
    $round_count += 1
    mp $round_count
    if $round_count == 4
      $round_count = 0
      $game_scene.show_flashing_message("cards")
      EM.add_timer 1.8 do
        $game_scene.hide_message
      end  
    end
  end
  
  def next_player
    $next_player = $game.players.first == $next_player ? $game.players.last : $game.players.first
  end    
  
  def show_points_popup(data)
    data = data.split("::")  
    lpc = data[0].split(",")
    opc = data[1].split(",")
    $game_scene.show_points_popup_data(lpc, opc)
  end  
  
  def new_deck(data)
    $next_player = next_player
    $turn = !$turn
    data = data.split("::")    
    
    update_game_cards(data)
    $game.end_game
    $game_scene.update_scores
    if $game_scene.is_a_winner?
      mp "Player: #{$game.winner?.id}"
    else
      $game.board_cards = []
      $o_player_cards_objs = []
      $o_player_cards_objs = []
      $o_player_cards = []
      $board_positions = []
      $o_player_back_cards = []
      $game_scene.add_board_positions
      $game_scene.update_turn
      
      $round += 1
      $game_scene.clear_all_positions
      $game_scene.set_round_title
      $game_scene.add_base_cards      
      
      if $turn
        $rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
        $rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for($main_view.frame.size.height, 100), $main_view.frame.size.width, ImageHelper.porcent_for($main_view.frame.size.height, 31))
        $rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      else
        $rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
        $rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)    
        $rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for($main_view.frame.size.height, 68.83), $main_view.frame.size.width, ImageHelper.porcent_for($main_view.frame.size.height, 31))
        $rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)     
      end  
      
    end 
  end  

  #================= Matching
  
  
  private
  
  def authenticationChanged
    if GKLocalPlayer.localPlayer.isAuthenticated && !@userAuthenticated
      @userAuthenticated = true
      puts "Event: User is logged on Game Center"
      puts GKLocalPlayer.localPlayer.displayName
      puts GKLocalPlayer.localPlayer.alias
    elsif !GKLocalPlayer.localPlayer.isAuthenticated && @userAuthenticated 
      @userAuthenticated = false
      puts "Event: User is not logged on Game Center"
    end  
  end  

  def gameCenterAvailableForSysVer(version)
    localP = (defined?(GKLocalPlayer) == "constant")
    ((version.to_f >= @reqSysVer.to_f) && localP)
  end
  
  def current_system_version
    UIDevice.currentDevice.systemVersion
  end    
  

  
  
end  