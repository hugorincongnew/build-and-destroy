class ImageHelper
  
  def self.name_for_phone(image_name)
    decompose_image = image_name.split('.')
    "#{decompose_image[0]}-#{$iphone_ver}.#{decompose_image[1]}"
  end  
  
  def self.top_porcent(porcent)
    $visible_size.height - (($visible_size.height / 100 ) * porcent)
  end  
  
  def self.top_porcent_for_height(porcent, height)
    height - ((height / 100 ) * porcent)
  end    
  
  def self.top_real_porcent(porcent)
    ($visible_size.height / 100 ) * porcent
  end    
  
  def self.left_porcent(porcent)
    ($visible_size.width / 100 ) * porcent
  end    
  
  def self.top_px(px)
    $visible_size.height - px
  end  
  
  def self.porcent_for(sizex, porcent)
    (sizex / 100) * porcent
  end  
  
end
