class TouchHelper
  
  def self.open_on_match_touch(menu_item, touch, convert=false, new_action=false)
    if convert
      if match_touch_x(menu_item[0], touch)
        self.touch_action(menu_item)
      end 
    else
      if match_touch(menu_item[0], touch)
        $new_action = true if new_action
        self.touch_action(menu_item)
      end 
    end  
  end
  
  def self.open_on_match_touch_layout(menu_item, touch, convert=false)
    if menu_item.class == Array
      if !menu_item[3].nil?
        if match_touch_layout(menu_item, touch)
          self.touch_action(menu_item)
        end     
      end  
    end     
  end  
  
  def self.touch_action(action)
    actions = action[1].split('-')
    if actions[1] == "Clean"
      SoundHelper.click
      $main_view_objs.each{|o| o.removeFromSuperview}
      open_new_scene(actions[0]) 
    elsif actions[1] == "Call"
      puts "call: #{actions[0]}"
      action[2].send(actions[0])
    elsif actions[1] == "GC"
      if $gcm.player_logged?
        SoundHelper.click
        open_new_scene(actions[0])     
      else
        GameHelper.alert("Game Center", "It is necessary to register on the Apple Game Center and log in in order to play")
        puts "Player not logged to Game Center"
      end    
    else
      SoundHelper.click
      open_new_scene(actions[0]) 
    end  
  end  

  
  def self.open_new_scene(scene)
    $director.replace(Kernel.const_get(scene).new)
  end      
  
  def self.match_touch(button, touch)   
    return true if match_x(touch.location.x, button) and match_y(touch.location.y, button)
  end  
  
  def self.match_touch_layout(items, touch)   
    return true if match_x_layout(touch.location.x, items[0], items[3]) and match_y_layout(touch.location.y, items[0], items[3])
  end    
  
  def self.match_touch_x(button, touch)   
    return true if match_x(touch.location.x, button) and match_y(touch.location.y, button)
  end    
  
  def self.match_x(x1, button) 
    if button.class.to_s == "UIImageView"   
      por = $visible_size.width / $main_view.frame.size.width 
      start_button_x = ((button.position.x * por) - (button.size.width * por) / 2)
      end_button_x = (start_button_x) + (button.size.width * por)   
    else
      start_button_x = (button.position.x - button.size.width / 2)
      end_button_x = start_button_x + button.size.width 
    end 

    x1 >= start_button_x and x1 <= end_button_x
  end
  
  def self.match_x_layout(x1, button, container) 
    addi = container.position.x
    start_button_x = ((button.position.x + addi) - button.size.width / 2) - 20
    end_button_x = start_button_x + button.size.width + 40
    
    NSLog("X")
    NSLog(x1.to_s)
    NSLog("#{start_button_x} - #{end_button_x}")    
    NSLog("")
    x1 >= start_button_x and x1 <= end_button_x
  end  
  
  def self.match_y(y1, button)
    if button.class.to_s == "UIImageView"   
      por = $visible_size.height / $main_view.frame.size.height 
      by = $main_view.frame.size.height - button.position.y 
      start_button_y = ((by * por) + ((button.size.height * por) / 2))
      end_button_y = start_button_y - (button.size.height * por)  
    else
      start_button_y = (button.position.y + (button.size.height / 2))
      end_button_y = start_button_y - button.size.height 
    end 
    y1 <= start_button_y and y1 >= end_button_y
  end 

  def self.match_y_layout(y1, button, container)
    addi = container.position.y
    start_button_y = ((button.position.y + addi) + (button.size.height / 2)) + 20
    end_button_y = start_button_y - button.size.height - 40
   
    NSLog("Y")
    NSLog(y1.to_s)
    NSLog("#{start_button_y} - #{end_button_y}")   
   
    y1 <= start_button_y and y1 >= end_button_y
  end   
  
end  