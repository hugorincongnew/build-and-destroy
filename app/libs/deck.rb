$all_cards = []
class Deck
  attr_accessor :cards
  RANKS = %w(2 3 4 5 6 7 8 9 10 J Q K A)
  SUITS = %w(Hearts Diamonds Clubs Spades)

  def initialize
    self.cards = []
    fill_deck
  end

  def fill_deck
    RANKS.each do |rank|
      SUITS.each do |suit|
        id = self.cards.count + 1
        tmp_card = Card.new(id, rank, suit)
        self.cards << tmp_card
        $all_cards << tmp_card
      end
    end

    self.cards = self.cards.shuffle
  end

  def remove_card
    self.cards.pop
  end

  def empty?
    self.cards.count == 0
  end

  def remaining_cards
    self.cards.size
  end
end
