
class BoardCard
  attr_accessor :card
  attr_accessor :player
  attr_accessor :position
  attr_accessor :play_id

  def initialize(card, player, position, play_id)
    self.card = card
    self.player = player
    self.position = position
    self.play_id = play_id
  end
end
