class Card
  attr_accessor :rank
  attr_accessor :suit
  attr_accessor :color
  attr_accessor :id
  attr_accessor :points
  VALUES = {'J' => 11, 'Q' => 12, 'K' => 13, 'A' => 1}

  def initialize(id, rank, suit)
    self.id = id
    self.rank = rank
    self.suit = suit
    self.color = suit_clubs_or_spades? ? 'black' : 'red'
    self.points = set_points
  end

  def suit_clubs_or_spades?
    self.suit == 'Clubs' || self.suit == 'Spades'
  end

  def value
    VALUES[self.rank] || self.rank.to_i
  end

  def set_points
    if self.rank == '10' and self.suit == 'Diamonds'
      2
    elsif self.rank == 'A' || (self.rank == '2' and self.suit == 'Spades')
      1
    else
      0
    end
  end

end
