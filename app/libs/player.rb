class Player
  ACHIEVEMENTS = {"Addiction 1" => [100,10000], "Addiction 2" =>  [250,50000], "Master Builder" =>  [10, 1000], "Deluxe Devastation" => [10, 2500], "Card Hog" =>  [5, 500], "Trash Can" =>  [10, 250], "Gold" =>  [10000, 0], "Silver" =>  [7500, 0], "Bronce" =>  [5000, 0]}
  PROPERTIES = [:id, :name, :email]
  PROPERTIES.each { |prop|
    attr_accessor prop
  }
  attr_accessor :hand
  attr_accessor :cards_picked_up
  attr_accessor :points
  attr_accessor :achievements
  attr_accessor :coins

  def initialize(attributes = {})
    attributes.each { |key, value|
      self.send("#{key}=", value) if PROPERTIES.member? key
    }
    self.hand = []
    self.cards_picked_up = []
    self.points = 0
    self.coins = 0
    define_player_achievements
  end

  def define_player_achievements
    self.achievements = []
    ACHIEVEMENTS.each do |name, detail|
      self.achievements << Achievement.new(name, detail)
    end
  end

  def find_achievement(text)
    self.achievements.find{ |a| a.name==text }
  end

  def drop_card(card)
    self.hand.delete_at(self.hand.find_index(card))
  end

  def filtered_cards_picked_up
    self.cards_picked_up.select { |board_card| board_card.card != nil }
  end  

  def most_spades?
    @spades = filtered_cards_picked_up.select { |board_card| board_card.card.suit=='Spades' }
    @spades.count > 6 ? 1 : 0
  end
  
  def spades_count
    self.filtered_cards_picked_up.select { |board_card| board_card.card.suit=='Spades' }.count
  end  

  def most_cards?
    self.filtered_cards_picked_up.count > 26 ? 3 : 0
  end

  def cards_value
    self.filtered_cards_picked_up.inject(0){|sum, board_card| sum += board_card.card.points }
  end
  
  def aces?
    self.filtered_cards_picked_up.inject(0){|sum, board_card| sum += (board_card.card.rank == "A" ? 1 : 0 ) }
  end  
  
  def diamond10?
    self.filtered_cards_picked_up.inject(0){|sum, board_card| sum += ((board_card.card.rank == "10" and board_card.card.suit == "Diamonds") ? 1 : 0 ) }
  end    
  
  def spades2?
    self.filtered_cards_picked_up.inject(0){|sum, board_card| sum += ((board_card.card.rank == "2" and board_card.card.suit == "Spades") ? 1 : 0 ) } 
  end  
  
  def player_points_resume
    resume = []
    resume << "#{aces?} Aces" if aces? > 0
    resume << "10 of Diamonds" if diamond10? > 0   
    resume
  end  

  def count_cards_picked_up
    self.filtered_cards_picked_up.count
  end

  def total_score
    self.points
  end

  def round_score
    ms = most_spades?
    mc = most_cards?
    cv = cards_value
    mp "========================"    
    mp "Scores Detail"
    mp "Most Spades: #{ms}"
    mp "Most Cards: #{mc}"
    mp "Cards Value: #{cv}"     
    mp "========================"  
    mp "========================"             
    ms + mc + cv
  end
end
