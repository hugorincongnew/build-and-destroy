class Achievement
  attr_accessor :name
  attr_accessor :coins
  attr_accessor :count
  attr_accessor :goal
  attr_accessor :active

  def initialize(name, detail)
    self.name = name
    self.goal = detail.first
    self.coins = detail.last
    self.count = 0
    self.active = false
  end

  def increase_count?
    self.count += 1 if !self.active && self.count < self.goal
  end

  def check_cagetory_points(num)
    self.count = num unless !self.active && self.count < self.goal
  end

  def reset_count
    self.count = 0
  end

  def accomplished?
    !self.active && self.count >= self.goal
  end

  def activate
    self.active = true
  end
end
