class Game
  attr_accessor :deck
  attr_accessor :players
  attr_accessor :board_cards
  attr_accessor :local_player
  attr_accessor :opponent_player
  $all_games_points = 0
  BUILD_NAME = {1=>"Build", 2=>"Double Build", 3=>"Triple Build", 4=>"Quadruple Build"}

  def initialize(users)
    self.local_player = Player.new(users[0])
    self.opponent_player =  Player.new(player_or_computer(users[1]))
    set_new_game
  end

  def player_or_computer(user)
    user ? user : {name: 'Computer'}
  end

  def new_round
    reset_players
    set_new_game
  end

  def set_new_game
    self.deck = Deck.new
    self.board_cards = []
    who_starts?
    deal_hand(6)
  end

  def reset_players
    self.players.each do |player|
      player.hand = []
      player.cards_picked_up = []
    end
  end

  def who_starts?
    @players = []
    @players << self.local_player
    @players << self.opponent_player
    self.players = @players.shuffle
  end

  def deal_cards?
    if self.local_player.hand.count == 0 && self.opponent_player.hand.count == 0 && self.deck.cards.count>=5
      deal_hand(5)
    end
  end

  def deal_hand(number)
    # TO set card by hand
    #self.players.first.hand = [self.deck.cards[4], self.deck.cards[5], self.deck.cards[8], self.deck.cards[9],  self.deck.cards[24], self.deck.cards[48]]
    #self.players.last.hand = [self.deck.cards[6], self.deck.cards[7],  self.deck.cards[10], self.deck.cards[11], self.deck.cards[25], self.deck.cards[49]]

    if self.deck.remaining_cards > 0
      1.upto(number) do |num|
        self.players.map { |player| player.hand << self.deck.remove_card }
      end
    end
  end

  def place_card(card, player, position, positions_highlight=nil)
    positions_highlight.nil? ? simple_place_card(card, player, position) : highlight_place_card(card, player, position, positions_highlight)
  end

  def simple_place_card(card, player, position)
    if cero_cards_in_position?(position) || can_place_same_rank_card_on_build?(card, player, position)
      place_1st_card(card, player, position)
    else
      build_number(card, player, position) ? "Building Number": is_stacking?(card, player, position) ? "Stacking Number" : is_stacking_facecards?(card, player, position) ? place_3rd_facecard(card, player, position) : invalid_play
    end
  end

  def highlight_place_card(card, player, position, positions_highlight)
    @sum_highlight = card.value
    positions_highlight.each { |ph| @sum_highlight += board_cards_by_position_and_last_play(ph).inject(0){ |sum, board_card| sum += board_card.card.value } }
    cards = check_highlighted_cards(card, player, @sum_highlight, positions_highlight)
    if cards.count == 0
      invalid_play
    else
      simple_place_card(card, player, position)
      return cards
    end
  end

  def check_highlighted_cards(card, player, sum, positions_highlight)
    cards = []
    player.hand.each do |hand_card|
      if (sum%hand_card.value == 0) && (card != hand_card) && (hand_card.value < 11) && (sum/hand_card.value <= positions_highlight.count) && bigger_number_on_build?(hand_card, positions_highlight)
        cards << hand_card
      end
    end
    return cards
  end

  def bigger_number_on_build?(card, positions)
    positions.each do |ph|
      return false if board_cards_by_position_and_last_play(ph).inject(0){ |sum, board_card| sum += board_card.card.value } > card.value
    end
    true
  end

  def group_builds_in_positions(card, positions_highlight)
    group_positions = []
    positions1 = positions_highlight
    positions1.delete_if do |pos1|
      sum1 = sum_of_board_cards_by_position_and_play(pos1)
      group_positions << [pos1] if sum1 == card.value

      positions2 = positions3 = positions4 = positions1
      positions2.each do |pos2|
        sum2 = sum_of_board_cards_by_position_and_play(pos2)
        group_positions << [pos1, pos2] if (sum1 + sum2) == card.value && pos1 != pos2 && !(group_positions.flatten & [pos1, pos2]).any?

        positions3.each do |pos3|
          sum3 = sum_of_board_cards_by_position_and_play(pos3)
          group_positions << [pos1, pos2, pos3] if (sum1 + sum2 + sum3) == card.value && ![pos1, pos2].include?(pos3) && !(group_positions.flatten & [pos1, pos2, pos3]).any?

          positions4.each do |pos4|
            sum4 = sum_of_board_cards_by_position_and_play(pos4)
            group_positions << [pos1, pos2, pos3, pos4] if (sum1 + sum2 + sum3 + sum4) == card.value && ![pos1, pos2, pos3].include?(pos3) && !(group_positions.flatten & [pos1, pos2, pos3, pos4]).any?
          end
        end
      end
      true
    end
    group_positions
  end

  def pick_up_build(card, player, position)
    if check_builded_number(card, player, position) && valid_build?(card, player, position)
      finish_build(card, player, position)
    else
      invalid_play
    end
  end

  def pick_up(card, player, position)
    if first_board_card_by_position(position).card.rank == card.rank
      place_card_in_board(card, player, position, 2)
      extract_cards_from_board(player, position)
      "Cards Picked Up"
    else
      invalid_play
    end
  end

  def create_stack(card, player, position)
    stack?(card, player, position) ? stack_number(card, player, position) : invalid_play
  end

  def invalid_play
    'Invalid play'
  end

  def place_1st_card(card, player, position)
    @play_id = count_board_cards_by_position(position) == 0 ? 1 : last_card_by_position(position).play_id + 1
    place_card_in_board(card, player, position, @play_id)
    "Card Placed"
  end

  def build_more?(board_card, player, builded_number)
    player.hand.each do |card|
      @num = card.value + board_card.value
      if @num == builded_number && check_number_to_build(@num , player.hand)
        return true
      end
    end
    false
  end

  def place_card_to_build?(card, player, position)
    last_card_by_position(position).play_id != 1 ? number_by_position_and_play(card, position, 1) >= card.value : true
  end

  def valid_build?(card, player, position)
    number_by_position_and_play(card, position, 1)  == card.value
  end

  def number_by_position_and_play(card, position, play)
    self.board_cards.select{|board_card| board_card.position==position && board_card.play_id == play}.inject(0){|sum, board_card| sum += board_card.card.value}
  end

  def check_builded_number(card, player, position)
    sum_of_board_cards_by_position_and_play(position) == card.value && different_cards_in_position?(card, position) && check_last_play_count(position)
  end

  def different_cards_in_position?(card, position)
    count_board_cards_by_position_and_play(position) == 1 ? true : count_distinct_board_cards_by_position(first_board_card_by_position(position).card, position) != 0
  end

  def check_last_play_count(position)
    last_card_by_position(position).play_id == 1 ? count_board_cards_by_position_and_play(position) > 1 : true
  end

  def check_number_to_build(number, player_hand)
    player_hand.each do |card_player|
      if (number == card_player.value && number <= 10)
        return true
      end
    end
    false
  end

  def build_number(card, player, position)
    if number_to_check(card, position) < 11 && check_new_number_to_build(card, player, position) && place_card_to_build?(card, player, position) && check_first_build_and_new_card(card, position)
      place_card_in_board(card, player, position, last_card_by_position(position).play_id)
      return true
    end
    false
  end

  def finish_build(card, player, position)
    play_id = number_of_the_build(position)
    @build_or_destroy = build_or_destroy?(player, position, play_id)
    place_card_in_board(card, player, position, play_id)
    extract_cards_from_board(player, position)
    @build_or_destroy
  end

  def build_or_destroy?(player, position, play_id)
    players_play?(player, position) ? BUILD_NAME[play_id] : play_id > 1 ? update_deluxe_devastation_count : "Destroy"
  end

  def number_of_the_build(position)
    last_card_by_position(position).play_id - count_single_cards_added_to_build(position)
  end

  def count_single_cards_added_to_build(position)
    uniq_board_cards_play_id_by_position(position).select { |board_card|  board_cards_by_position_and_play(position, board_card.play_id).count == 1 }.count
  end

  def uniq_board_cards_play_id_by_position(position)
    board_cards_by_position(position).uniq{ |board_card| board_card.play_id }
  end

  def update_deluxe_devastation_count
    increase_achivement_count?("Deluxe Devastation")
    "Ultimate Destruction"
  end

  def players_play?(player, position)
    @cards = board_cards_by_position(position)
    @cards[1].player == player if !@cards[1].nil?
  end

  def is_stacking?(card, player, position)
    if stacking_number(card, player, position)
      place_card_in_board(card, player, position, 1)
      return true
    end
    false
  end

  def stacking_number(card, player, position)
    first_board_card_by_position(position).card.rank == card.rank && ( count_player_cards_by_rank(card, player, position) ) != 0
  end

  def stack_number(card, player, position)
    @stack_or_steal = stack_or_steal?(player, position)
    place_card_in_board(card, player, position, 1)
    extract_cards_from_board(player, position)
    @stack_or_steal
  end

  def stack?(card, player, position)
    card.value > 10 ? stack_for_facecard?(card, player, position) : stack_for_number_card?(card, player, position)
  end

  def stack_for_number_card?(card, player, position)
    count_board_cards_by_rank_and_position(card, position).between?(2,3)
  end

  def stack_for_facecard?(card, player, position)
    count_board_cards_by_rank_and_position(card, position) == 3
  end

  def is_stacking_facecards?(card, player, position)
    count_board_cards_by_rank_and_position(card, position) == 2
  end

  def place_3rd_facecard(card, player, position)
    place_card_in_board(card, player, position, 1)
    "Stacking Number"
  end

  def stack_or_steal?(player, position)
    players_play?(player, position) ? "Stack" : "Steal"
  end

  def check_place_card_for_stack(card, player, position)
    card.value > 10 ? cards_by_rank_and_position(card, player, position)==4 : cards_by_rank_and_position(card, player, position).between?(3,4)
  end

  def check_staking_number(card, player, position)
    if first_board_card_by_position(position) && card.value > 10
      return first_board_card_by_position(position).card.value == card.value
    end
    true
  end

  def distict_board_cards_by_position?(card, player, position)
    count_distinct_board_cards_by_position(card, position) == 0
  end

  def players_same_card_by_rank?(card, player, position)
    player.hand.select{ |player_card| player_card != card && player_card.rank == card.rank }.count > 0
  end

  def place_card_in_board(card, player, position, play_id)
    self.board_cards.push(BoardCard.new(card, player, position, play_id))
    player.drop_card(card)
    keep_board_cards?(player)
  end

  def extract_cards_from_board(player, position)
    board_cards_by_position(position).map{ |card| player.cards_picked_up << self.board_cards.delete(card) }
  end

  def move_card(board_card, player, position)
    if check_new_number_to_build(board_card.card, player, position) || stack?(board_card.card, player, position)
      update_board_card(board_card, player, position)
      "Card Moved"
    else
      invalid_play
    end
  end

  def move_to_pick_up_build(positions, card, player)
    build_positions = positions_to_move(positions, player)
    if positions_builds_equal_to_card?(build_positions, card).include? false
      invalid_play
    else
      check_builds_on_positions(build_positions, card, player, positions)
    end
  end

  def positions_to_move(positions, player)
    positions.map { |p| p.is_a?(Array) }.include?(true) ? group_array_positions(positions, player) : positions
  end

  def group_array_positions(positions, player)
    builds = []
    positions.each do |build_positions|
      if build_positions.is_a?(Array)
        if build_positions.count != 1
          new_positions = group_cards_in_position(build_positions, player)
          builds << new_positions[1]
        else
          builds << build_positions.first
        end
      end
    end
    builds
  end

  def check_builds_on_positions(positions, card, player, previous_positions)
    new_position = board_new_position
    build = [previous_positions, new_position]
    move_cards_to_new_position(positions, player, new_position)
    build << pick_up_build(card, player, new_position)
  end

  def check_build_to_move_new(position, index, player, new_position)
    board_cards_by_position(position).map{ |board_card| update_board_card(board_card, player, new_position, index + 1) }
  end

  def move_cards_to_new_position(positions, player, new_position)
    positions.each_with_index do |position, index|
      check_build_to_move_new(position, index, player, new_position)
    end
  end

  def positions_builds_equal_to_card?(positions, card)
    positions.map { |position| sum_of_board_cards_by_position_and_play(position) == card.value }
  end

  def computer_move_to_pick_up_build(card, player, position)
    move_cards_to_position(card, player, position)
    pick_up_build(card, player, position)
  end

  def simple_move_to_pick_up_build(positions_highlight, card, player)
    previous_positions = positions_highlight.clone
    builds = group_builds_in_positions(card, positions_highlight)
    if builds.flatten.count == previous_positions.flatten.count
      simple_builds_on_positions(builds, card, player)
    else
      invalid_play
    end
  end

  def simple_builds_on_positions(builds, card, player)
    new_position = board_new_position
    buildt = [builds, new_position]
    builds.each_with_index do |build, index|
      build.each do |position|
        check_build_to_move_new(position, index, player, new_position)
      end
    end
    buildt << pick_up_build(card, player, new_position)
  end

  def move_to_create_stack(card, player, position)
    move_stack_cards_to_position(card, player, position)
    create_stack(card, player, position)
  end

  def move_to_start_build(positions, player)
    check_number_to_build_by_position(positions, player) unless single_positions?(positions).include? false
  end

  def check_number_to_build_by_position(positions, player)
    number = positions.inject(0){|sum, position| sum += last_card_by_position(position).card.value }
    check_number_to_build(number, player.hand) ? group_cards_in_position(positions, player) : nil
  end

  def group_cards_in_position(positions, player)
    new_position = board_new_position
    build = [positions, new_position]
    positions.each do |position|
      check_build_to_move_new(position, 1, player, new_position)
    end
    build
  end

  def single_positions?(positions)
    positions.map { |position| count_board_cards_by_position(position) == 1 }
  end

  def move_cards_to_position(card, player, position)
    available_builds_sorted_asc(card).each_with_index do |build, index|
      check_build_to_move(build, index, player, position)
    end
  end

  def check_build_to_move(build, index, player, position)
    build.map { |pos| board_cards_by_position(pos).map{ |board_card| update_board_card(board_card, player, position, index + 1) } }
  end

  def update_board_card(board_card, player, position, play_id=nil)
    self.board_cards << self.board_cards.delete(board_card)
    board_card.player = player
    board_card.position = position
    board_card.play_id = play_id ? play_id : last_card_by_position(position).play_id
  end

  def move_stack_cards_to_position(card, player, position)
    single_board_cards_by_rank(card).each_with_index do |board_card, index|
      update_board_card(board_card, player, position)
    end
  end

  def available_stacks?(card)
    single_board_cards_by_rank_positions(card) if stack_on_board?(card)
  end

  def stack_on_board?(card)
    card.value > 10 ? single_board_cards_by_rank_positions(card).count == 3 : single_board_cards_by_rank_positions(card).count.between?(2,3)
  end

  def single_board_cards_by_rank(card)
    single_board_cards.select { |board_card| board_card.card.rank == card.rank }
  end

  def single_board_cards_by_rank_positions(card)
    single_board_cards_by_rank(card).map { |board_card| board_card.position  }
  end

  def available_builds?(card)
    board_posible_builds(card).uniq{|b| b.sort { |x,y| y <=> x } }.take(4) if card.value < 11
  end

  def available_builds_sorted_asc(card)
    available_builds?(card).sort { |x,y| x.first <=> y.first }.take(4)
  end

  def board_posible_builds(card)
    @builds = builds_on_board?(card)
    @more_builds = more_builds_on_board?(card)
    if builds_only_single_cards?(@builds, @more_builds)
      @builds.concat @more_builds
    else
      [[]]
    end
  end

  def builds_only_single_cards?(builds, more_builds)
    more_builds.count== 0 ? single_cards_on_builds_on_board?(builds) : true
  end

  def single_cards_on_builds_on_board?(builds)
    builds.map { |build| count_board_cards_by_position(build.first) > 1 }.include? true
  end

  def builds_on_board?(card)
    positions = []
    board_cards_positions_sorted.each do |x|
      positions << [x[:position]] if sum_of_board_cards_by_position_and_play(x[:position]) == card.value && different_cards_in_position?(card, x[:position])
    end
    positions
  end

  def more_builds_on_board?(card)
    builds = []
    single_board_cards.each do |board_card1|
      single_board_cards.each do |board_card2|
        builds << [board_card1.position, board_card2.position] if board_card2 != board_card1 && board_card1.card.rank != board_card2.card.rank && card_to_more_builds?(builds, board_card1) && card_to_more_builds?(builds, board_card2) && (board_card1.card.value + board_card2.card.value) == card.value
      end
    end
    builds
  end

  def card_to_more_builds?(builds, board_card)
    builds.select { |build| build.include? board_card.position }.count == 0
  end

  def single_board_cards
    uniq_board_cards.select { |card| count_board_cards_by_position(card.position) == 1 }
  end

  def first_board_card_by_position(position)
    self.board_cards.find{ |card| card.position==position}
  end

  def count_board_cards_by_position(position)
    board_cards_by_position(position).count
  end

  def board_cards_by_position(position)
    self.board_cards.select { |card| card.position==position }
  end

  def count_distinct_board_cards_by_position(card, position)
    self.board_cards.select { |board_card| board_card.position==position && board_card.card.rank != card.rank }.count
  end

  def sum_of_board_cards_by_position_and_play(position)
    board_cards_by_position_and_last_play(position).inject(0){|sum, board_card| sum += board_card.card.value }
  end

  def count_board_cards_by_position_and_play(position)
    board_cards_by_position_and_last_play(position).count
  end

  def cards_by_rank_and_position(card, player, position)
    count_board_cards_by_rank_and_position(card, position) + count_player_cards_by_rank(card, player, position)
  end

  def board_cards_by_position_and_last_play(position)
    board_cards_by_position_and_play(position, last_play_id(position))
  end

  def board_cards_by_position_and_play(position, play_id)
    self.board_cards.select { |board_card| board_card.position==position && board_card.play_id == play_id }
  end

  def first_builded_number(position)
    board_cards_by_position_and_play(position, 1).inject(0){|sum, board_card| sum += board_card.card.value }
  end

  def verify_card_to_be_placed(card, player, position)
    number_to_check(card, position) <= first_builded_number(position)
  end

  def optional_card(card)
    card.rank == 'A' || (card.rank == 2 && can_place_card.suit == 'Spades')
  end

  def new_round_or_optional_card(card)
    its_a_new_round? ? true : optional_card(card)
  end

  def its_a_new_round?
    self.local_player.hand.count.between?(5,6) && self.opponent_player.hand.count.between?(5,6)
  end

  def build_in_position?(card, player, position)
    count_board_cards_by_position(position) > 1 ? last_card_by_position(position).player == player || new_round_or_optional_card(card) : true
  end

  def can_place_card_for_build?(card, player, position)
    !check_builded_number(card, player, position) && is_building_new_number?(card, player, position) && !build?(card, player, position) && build_in_position?(card, player, position) && is_building_three_cards?(card, player, position) && different_cards_in_position?(card, position)
  end

  def last_play_id(position)
    last_card_by_position(position) ? last_card_by_position(position).play_id : 0
  end

  def number_to_check(card, position)
    sum_of_board_cards_by_position_and_play(position) + card.value
  end

  def check_new_number_to_build(card, player, position)
    check_number_to_build(number_to_check(card, position), player.hand)
  end

  def cero_cards_in_position?(position)
    count_board_cards_by_position(position) == 0
  end

  def check_new_number_to_build_by_value(card, player, position)
    @num = number_to_check(card, position)
    @num < 11 ? check_number_to_build(@num, player.hand) : false
  end

  def is_building_new_number?(card, player, position)
    check_first_build_and_new_card(card, position) ? check_new_number_to_build_by_value(card, player, position) : false
  end

  def check_first_build_and_new_card(card, position)
    sum_of_board_cards_by_position_and_play(position) != card.value
  end

  def is_building_three_cards?(card, player, position)
    count_board_cards_by_position(position) == 2 ? optional_card(card) : true
  end

  def check_player_cards_for_stack(card, player, position)
    card.value > 10 ? count_player_cards_by_rank(card, player, position) != 3 : !count_player_cards_by_rank(card, player, position).between?(2,3)
  end

  def player_cards_to_pick_up?(card, player, position)
    count_player_cards_by_rank(card, player, position) == 1 ? true : check_player_cards_for_stack(card, player, position)
  end

  def last_card_by_position(position)
    board_cards_by_position(position).last
  end

  def count_board_cards_by_rank_and_position(card, position)
    self.board_cards.select { |board_card| board_card.card.rank==card.rank && board_card.position==position && board_card.play_id == last_card_by_position(position).play_id }.count
  end

  def count_player_cards_by_rank(card, player, position)
    player.hand.select { |player_card| player_card.rank==card.rank }.count
  end

  def last_to_pick_up?(player)
    self.local_player.hand.count == 0 && self.opponent_player.hand.count == 0 && self.deck.cards.count == 0
  end

  def find_player_achievement(text)
    self.local_player.find_achievement(text)
  end

  def keep_board_cards?(player)
    if last_to_pick_up?(player)
      1.upto(self.board_cards.count) do
        player.cards_picked_up << self.board_cards.delete_at(0)
      end
    end
  end

  def end_game
    increase_regular_counts
    check_categories
    check_other_achievements
    set_players_points
  end

  def increase_regular_counts
    increase_achivement_count?("Addiction 1")
    increase_achivement_count?("Addiction 2")
  end

  def check_categories
    category_goal?("Gold")
    category_goal?("Silver")
    category_goal?("Bronce")
  end

  def check_other_achievements
    check_most_cards_achievement("Card Hog")
    check_master_builder_achievement("Master Builder")
    check_trash_can_achievement("Trash Can")
  end

  def set_players_points
    $all_games_points += self.local_player.round_score
    mp "Local Player Scores On Round"
    self.local_player.points += self.local_player.round_score
    mp "Opponent Player Scores On Round"
    self.opponent_player.points += self.opponent_player.round_score
    $most_cards = self.local_player.most_cards?
  end

  def check_trash_can_achievement(text)
    self.local_player.round_score == 0 ? increase_achivement_count?(text) : reset_achivement_count(text)
  end

  def check_most_cards_achievement(text)
    self.local_player.most_cards? == 3 ? increase_achivement_count?(text) : reset_achivement_count(text)
  end

  def check_master_builder_achievement(text)
    winner? == self.local_player ? increase_achivement_count?(text) : reset_achivement_count(text)
  end

  def increase_achivement_count?(text)
    @achievement = find_player_achievement(text)
    @achievement.increase_count?
  end

  def category_goal?(text)
    @achievement = find_player_achievement(text)
    @achievement.check_cagetory_points($all_games_points)
  end

  def reset_achivement_count(text)
    @achievement = find_player_achievement(text)
    @achievement.reset_count
  end

  def any_achievements?
    @achievements = new_achievements?
    self.local_player.coins += achievements_sum_of_coins(@achievements)
    activate_achievements(@achievements)
    @achievements
  end

  def achievements_sum_of_coins(achievements)
    achievements.inject(0){|sum, a| sum += a.coins }
  end

  def activate_achievements(achievements)
    achievements.map { |a| a.activate }
  end

  def new_achievements?
    self.local_player.achievements.select{ |a| a if a.accomplished? }
  end

  def winner?
    if (self.local_player.points > 10 || self.opponent_player.points > 10) and (self.opponent_player.points != self.local_player.points)
      self.local_player.points > self.opponent_player.points ? self.local_player : self.opponent_player
    else
      "No Winner"
    end
  end

  def build?(card, player, position)
    check_builded_number(card, player, position) ? players_play?(player, position) : false
  end

  def is_a_build?(card, player, position)
    build?(card, player, position) && number_of_the_build(position) == 1
  end

  def destroy?(card, player, position)
    check_builded_number(card, player, position) ? (players_play?(player, position) ? false : true) : false
  end

  def is_a_destroy?(card, player, position)
    destroy?(card, player, position) && number_of_the_build(position) == 1
  end

  def is_a_double_build?(card, player, position)
    build?(card, player, position) && number_of_the_build(position) == 2
  end

  def is_a_triple_build?(card, player, position)
    build?(card, player, position) && number_of_the_build(position) == 3
  end

  def is_a_quadruple_build?(card, player, position)
    build?(card, player, position) && number_of_the_build(position) == 4
  end

  def is_an_ultimate_destruction?(card, player, position)
    destroy?(card, player, position) && number_of_the_build(position) > 1
  end

  def is_a_stack?(card, player, position)
    stack?(card, player, position) && players_play?(player, position)
  end

  def is_a_steal?(card, player, position)
    stack?(card, player, position) && !players_play?(player, position)
  end

  def is_a_pick_up?(card, player, position)
    count_board_cards_by_position(position) == 1 && first_board_card_by_position(position).card.rank == card.rank
  end

  def can_place_card?(card, player, position)
    cero_cards_in_position?(position) ? true : !is_a_pick_up?(card, player, position) && !stack?(card, player, position) && can_place_card_for_build?(card, player, position)
  end

  def can_place_card_for_stack?(card, player, position)
    check_place_card_for_stack(card, player, position) && check_staking_number(card, player, position) && distict_board_cards_by_position?(card, player, position) && players_same_card_by_rank?(card, player, position)
  end

  def can_place_same_rank_card_on_build?(card, player, position)
    build?(card, player, position) && players_same_card_by_rank?(card, player, position)
  end

  def available_actions(card, player, position)
    @actions = []
    @actions << :pick_up_build if is_a_build?(card, player, position)
    @actions << :create_destroy if is_a_destroy?(card, player, position)
    @actions << :create_double_build if is_a_double_build?(card, player, position)
    @actions << :create_triple_build if is_a_triple_build?(card, player, position)
    @actions << :create_quadruple_build if is_a_quadruple_build?(card, player, position)
    @actions << :create_ultimate_destruction if is_an_ultimate_destruction?(card, player, position)
    @actions << :create_stack if is_a_stack?(card, player, position)
    @actions << :create_steal if is_a_steal?(card, player, position)
    @actions << :pick_up if is_a_pick_up?(card, player, position)
    @actions << :place_card if can_place_card?(card, player, position) || can_place_card_for_stack?(card, player, position) || can_place_same_rank_card_on_build?(card, player, position)
    return @actions
  end

  def computers_turn
    action = board_cards_positions.count > 0 ? computers_play_by_position : [self.opponent_player.hand[0], :place_card, 0]
    action_to_call = actions_name(action[1])
    mp "Computer Action: #{action[1]},  #{action[0].rank} #{action[0].suit} in position #{action[2]}"
    result = self.send(action_to_call, action[0], self.opponent_player, action[2])
    mp "Action Result: #{result}"
    return action << result
  end

  def most_cards_last_hand?
    $most_cards
  end

  def actions_name(name)
    case name
    when :create_destroy
      :pick_up_build
    when :create_double_build
      :pick_up_build
    when :create_triple_build
      :pick_up_build
    when :create_quadruple_build
      :pick_up_build
    when :create_ultimate_destruction
      :pick_up_build
    when :create_steal
      :create_stack
    else
      name
    end
  end

  def computers_play_by_position
    board_cards_positions_sorted.each do |x|
      @play = computer_play_by_position?(x[:position])
      if includes_play?(@play[1])
        return play_positions(x, @play)
      end
    end
    return [self.opponent_player.hand[0], :place_card, board_new_position]
  end

  def play_positions(x, play)
    play << x[:position] unless play.count >= 3
    play << available_builds_sorted_asc(play[0]) unless play[1] != :computer_move_to_pick_up_build
    play << available_stacks?(play[0]) unless play[1] != :move_to_create_stack
    return play
  end

  def includes_play?(play)
    [:pick_up_build, :create_stack, :pick_up, :place_card, :computer_move_to_pick_up_build, :move_to_pick_up_build, :move_to_create_stack].include? play
  end

  def board_cards_positions_sorted
    board_cards_positions.sort { |x,y| y[:position] <=> x[:position] }.sort { |x,y| y[:cards_count] <=> x[:cards_count] }
  end

  def board_new_position
    (0..14).each{ |p| return p if cero_cards_in_position?(p) }
  end

  def board_max_position
    uniq_board_cards.map { |card| card.position }.max
  end

  def board_cards_positions
    uniq_board_cards.map { |card| { position: card.position, cards_count: count_board_cards_by_position(card.position) } }
  end

  def uniq_board_cards
    self.board_cards.uniq { |card| card.position }
  end

  def computer_play_by_position?(position)
    self.opponent_player.hand.each do |card|
      if can_build?(card, self.opponent_player, position)
        builds = available_builds_sorted_asc(card)
        return [card, :computer_move_to_pick_up_build, builds.first.first] unless builds.count == 1
        return [card, :pick_up_build]
      elsif can_move_cards_to_build?(card)
        return [card, :computer_move_to_pick_up_build, available_builds_sorted_asc(card).first.first]
      elsif can_move_cards_to_stack?(card)
        return [card, :move_to_create_stack, available_stacks?(card).first]
      elsif is_a_stack?(card, self.opponent_player, position) || is_a_steal?(card, self.opponent_player, position)
        return [card, :create_stack]
      elsif is_a_pick_up?(card, self.opponent_player, position)
        return [card, :pick_up]
      elsif can_place_card?(card, self.opponent_player, position) || can_place_card_for_stack?(card, self.opponent_player, position) || can_place_same_rank_card_on_build?(card, self.opponent_player, position)
        return [card, :place_card]
      else
        [card, nil]
      end
    end
  end

  def can_move_cards_to_stack?(card)
    stacks = available_stacks?(card)
    stacks.any? unless stacks.nil?
  end

  def can_move_cards_to_build?(card)
    builds = available_builds?(card)
    check_can_move_to_build(builds) unless builds.nil?
  end

  def check_can_move_to_build(builds)
    builds.any? ? move_to_build_count(builds) : false
  end
  def move_to_build_count(builds)
    builds.count > 1 ? true : builds.first.count > 1
  end

  def can_build?(card, player, position)
    build?(card, player, position) || destroy?(card, player, position)
  end

end
