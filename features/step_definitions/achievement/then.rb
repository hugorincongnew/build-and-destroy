Then(/^the achievement instance should be a Achievement class$/) do
  expect(@achievement.class).to be(Achievement)
end

Then(/^the (achievement|gold category) coins should be (\d+), goal (\d+), count (\d+), active false$/) do |text, coins, goal, count|
  expect(@achievement.goal).to eq(goal.to_i)
  expect(@achievement.coins).to eq(coins.to_i)
  expect(@achievement.count).to eq(count.to_i)
  expect(@achievement.active).to eq(false)
end
