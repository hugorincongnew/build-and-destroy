Given ("a new achievement instance") do
  @achievement = Achievement.new("Deluxe Devastation", [10, 2500])
end

Given ("a new gold category instance") do
  @achievement = Achievement.new("Gold", [5000, 0])
end
