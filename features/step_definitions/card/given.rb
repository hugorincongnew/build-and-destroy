#MAIN FEATURE
Given ("a new card instance") do
  @card = Card.new(1, 'A', 'Hearts')
end

# CARD POINT FEATURE
Given ("a card set the points") do
  @card_1 = Card.new(1, 'A', 'Hearts')
  @card_2 = Card.new(2, 10, 'Diamonds')
  @card_3 = Card.new(3, 2, 'Spades')
  @card_4 = Card.new(4, 3, 'Spades')
  @card_5 = Card.new(5, 2, 'Hearts')
end

# CARD POINT FEATURE
Given ("a card set the color") do
  @card_1 = Card.new(1, 'A', 'Hearts')
  @card_2 = Card.new(2, 'A', 'Diamonds')
  @card_3 = Card.new(3, 'A', 'Spades')
  @card_4 = Card.new(4, 'A', 'Clubs')
end
