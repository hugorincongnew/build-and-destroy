Before do
  @game = Game.new([{id:'1',name:'rafael camacho',email:'rafael@gmail.com'}, {id:'2',name:'Computer'}])
  @game.players[0] = @game.local_player
  @game.players[1] = @game.opponent_player
end

Given ("some separated 8 builds on the board and a 4 and 8 in the players hand") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 3, "Spades")
  @game.players[1].hand << Card.new(1, 4, "Hearts")
  @game.players[1].hand << Card.new(1, 4, "Clubs")

  @game.players[0].hand << Card.new(1, "A", "Diamonds")
  @game.players[0].hand << Card.new(1, 4, "Diamonds")
  @game.players[0].hand << Card.new(1, 8, "Diamonds")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")

  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 3)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 7)
end

Given ("cards to create a build highlighting cards for a 9") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 4, "Spades")
  @game.players[1].hand << Card.new(1, 5, "Hearts")
  @game.players[1].hand << Card.new(1, 7, "Clubs")
  @game.players[1].hand << Card.new(1, 9, "Clubs")

  @game.players[0].hand << Card.new(1, 9, "Diamonds")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Spades")

  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 2)

  @game.place_card(@game.players[0].hand[0], @game.players[0], 5)
end

Given ("cards to create a build highlighting cards for a 10") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 3, "Hearts")
  @game.players[1].hand << Card.new(1, 4, "Clubs")
  @game.players[1].hand << Card.new(1, 2, "Spades")
  @game.players[1].hand << Card.new(1, 9, "Clubs")
  @game.players[1].hand << Card.new(1, 7, "Clubs")

  @game.players[0].hand << Card.new(1, 8, "Diamonds")
  @game.players[0].hand << Card.new(1, 'A', "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Spades")

  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)

  @game.place_card(@game.players[1].hand[0], @game.players[1], 3)

  @game.place_card(@game.players[0].hand[0], @game.players[0], 6)
end


Given ("cards to create a build highlighting cards for a 8") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 6, "Hearts")
  @game.players[1].hand << Card.new(1, 2, "Clubs")
  @game.players[1].hand << Card.new(1, 8, "Spades")
  @game.players[1].hand << Card.new(1, 2, "Clubs")

  @game.players[0].hand << Card.new(1, 6, "Diamonds")
  @game.players[0].hand << Card.new(1, 8, "Diamonds")

  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 2)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 3)
end

Given ("a 9 build on the board move to pick up the 9") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 7, "Diamonds")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Diamonds")
end

Given ("a 9 build with 9 on the board") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 5, "Diamonds")
  @game.players[0].hand << Card.new(1, 4, "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Diamonds")

  @game.players[1].hand << Card.new(1, 9, "Spades")
  @game.players[1].hand << Card.new(1, 9, "Hearts")
end

Given ("two 9 builds with 5 cards pick up the 9s") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 4, "Diamonds")
  @game.players[0].hand << Card.new(1, 4, "Spades")
  @game.players[0].hand << Card.new(1, 3, "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Diamonds")

  @game.players[1].hand << Card.new(1, 'A', "Spades")
  @game.players[1].hand << Card.new(1, 6, "Hearts")
end
