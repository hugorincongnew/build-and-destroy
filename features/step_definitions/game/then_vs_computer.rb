#VS COMPUTER FEATURE
Then(/^vs computer 1st turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Building Number")
end

Then(/^vs computer 2nd turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up_build, 0])
  expect(@game.pick_up_build(@game.players[1].hand[0], @game.players[1], 0)).to eq("Build")
end

Then(/^vs computer 3rd turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Building Number")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
end

Then(/^vs computer 4th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:pick_up_build])
  expect(@game.pick_up_build(@game.players[0].hand[0], @game.players[0], 1)).to eq("Build")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
end

Then(/^vs computer 5th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 2)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 2])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 2)).to eq("Cards Picked Up")
end

Then(/^vs computer 6th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 2)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 3])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 3)).to eq("Card Placed")
end

Then(/^vs computer 7th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], 0)).to eq("Cards Picked Up")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :computer_move_to_pick_up_build, 2,  [[2, 3]]])
  expect(@game.computer_move_to_pick_up_build(@game.players[1].hand[0], @game.players[1], 2)).to eq("Build")
end

Then(/^vs computer 8th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[2], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[2], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[1], :pick_up, 0])
  expect(@game.pick_up(@game.players[1].hand[1], @game.players[1], 0)).to eq("Cards Picked Up")
end

Then(/^vs computer 9th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[1], :pick_up, 0])
  expect(@game.pick_up(@game.players[1].hand[1], @game.players[1], 0)).to eq("Cards Picked Up")
end

Then(/^vs computer 10th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[1], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[1], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 2])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 2)).to eq("Card Placed")
end

Then(/^vs computer 11th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 5)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 5)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 5])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 5)).to eq("Cards Picked Up")
end

Then(/^vs computer 12th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], 1)).to eq("Cards Picked Up")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 2])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 2)).to eq("Cards Picked Up")
end

Then(/^vs computer 13th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 4)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 4)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
end

Then(/^vs computer 14th turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 1])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 1)).to eq("Cards Picked Up")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")
end

Then(/^vs computer 15th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], 1)).to eq("Cards Picked Up")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
end

Then(/^vs computer 16th turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 5)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 5)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 2])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 2)).to eq("Card Placed")
end

Then(/^vs computer 6th turn (\d+) local player points and (\d+) computer points$/) do |player_points, computer_points|
  expect(@game.local_player.round_score).to eq(player_points.to_i)
  expect(@game.opponent_player.round_score).to eq(computer_points.to_i)
end

Then(/^vs place card action for the computer 1st turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 12)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 12)).to eq("Card Placed")
end

Then(/^vs vs place card action for the computer 2nd turn$/) do
  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :place_card, 12])
end

Then(/^vs vs create build action for the computer 1st turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Building Number")
end

Then(/^vs vs create build action for the computer 2nd turn$/) do
  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :start_build, 0])
end

Then(/^vs vs pick up cards action for the computer 1st turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 0])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 0)).to eq("Cards Picked Up")
end

Then(/^vs pick up cards action for the computer and the player 1st turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
end

Then(/^vs pick up cards action for the computer and the player 2nd turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], 0)).to eq("Cards Picked Up")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 1])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 1)).to eq("Cards Picked Up")
end

Then(/^vs create steal action for the computer 1st turn$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Building Number")
end

Then(/^vs create steal action for the computer 2nd turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 2])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 2)).to eq("Card Placed")
end

Then(/^vs pick up cards for the computer 1st turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], 0)).to eq("Cards Picked Up")
end

Then(/^vs create build for the computer 1st turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Building Number")
end

Then(/^vs create build for the computer 2nd turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up_build])
  expect(@game.pick_up_build(@game.players[0].hand[0], @game.players[0], 0)).to eq("Build")
end

Then(/^the computer shouldn't be able to place a card on top of the number to stack$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :create_stack, 0])
  expect(@game.create_stack(@game.players[1].hand[0], @game.players[1], 0)).to eq("Steal")
end

Then(/^the player should be able to start a new build$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Building Number")
end

Then(/^the computer should be able to pick up jacks on the board$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], 1)).to eq("Cards Picked Up")
end

Then(/^the computer shouldn't be able to create a build with three cards$/) do
expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :place_card, 0])
end

Then(/^the computer should be able to pick up the build and the cards on the board$/) do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Building Number")

  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2)).to eq("Card Placed")
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :computer_move_to_pick_up_build, 0, [[0], [2]]])
  expect(@game.computer_move_to_pick_up_build(@game.players[1].hand[0], @game.players[1], 0)).to eq("Build")
end

Then(/^the player should be able to place a card and pick up the build with their last card of that rank$/) do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")

  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Building Number")

  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up_build, :place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")

  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up_build])
  expect(@game.pick_up_build(@game.players[0].hand[0], @game.players[0], 0)).to eq("Build")
end

Then(/^the player should be able to pick up the ace on the board$/) do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 3)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 7)).to eq("Card Placed")

  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 3)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], 3)).to eq("Cards Picked Up")
end

Then ("the player shouldn't be able to build") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")

  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :place_card, 0])

  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).not_to eq([:place_card])

  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).not_to eq("Building Number")

  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).not_to eq([:place_card])
  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :pick_up_build, 0])
end

Then ("the computer shouln't be able to move to pick up build") do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 12)).to eq("Card Placed")

  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :computer_move_to_pick_up_build, 0, [[0], [12]]])
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :move_to_create_stack, 0, [0, 12]])
end


Then ("the computer should be able to create a stack for 8s") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2)).to eq("Card Placed")

  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :move_to_create_stack, 0, [0, 2]])
  expect(@game.move_to_create_stack(@game.players[1].hand[0], @game.players[1], 0)).to eq("Stack")
end

Then ("the computer shouldn't see the Stacking 2's message") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2)).to eq("Card Placed")

  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 0])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 0)).to eq("Cards Picked Up")
end

Then ("the computer should be able to create a stack for 5s") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 4)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 6)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 12)).to eq("Card Placed")

  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :move_to_create_stack, 4, [4, 12]])
  expect(@game.move_to_create_stack(@game.players[1].hand[0], @game.players[1], 4)).to eq("Stack")
end

Then ("the computer should be able to create a stack for Qs") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 4)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 7)).to eq("Card Placed")

  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :move_to_create_stack, 2, [2, 4, 7]])
  expect(@game.move_to_create_stack(@game.players[1].hand[0], @game.players[1], 2)).to eq("Stack")
end

Then ("the player shouldn't be able to build 2") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Building Number")

  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
end

Then ("the computer should be able to pick up the 9 build") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")

  expect(@game.simple_move_to_pick_up_build([0, 1], @game.players[0].hand[0], @game.players[0])).to eq([[[0, 1]], 2, "Build"])
end

Then ("the computer should be able to pick up the two 9 builds") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 3)).to eq("Card Placed")

  result = @game.simple_move_to_pick_up_build([0, 3, 1, 2], @game.players[0].hand[0], @game.players[0])

  expect(result).to eq([[[0, 1], [3, 2]], 4, "Double Build"])
end

Then ("the player should be able create a stack instead of a build") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Stacking Number")

  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([])
  expect(@game.available_actions(@game.players[0].hand[1], @game.players[0], 0)).to eq([:create_stack])
  expect(@game.create_stack( @game.players[0].hand[1], @game.players[0], 0)).to eq("Stack")
end

Then ("the player should be able to destroy the build") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Building Number")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Building Number")

  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([:create_destroy])
  expect(@game.pick_up_build(@game.players[1].hand[0], @game.players[1], 0)).to eq("Destroy")
end


Then ("the player shouldn't be able to create a build") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Stacking Number")
  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :place_card, 0])
end
