Before do
  @game = Game.new([{id:'1',name:'rafael camacho',email:'rafael@gmail.com'}, {id:'2',name:'Computer'}])
  @game.players[0] = @game.local_player
  @game.players[1] = @game.opponent_player
end

Given ("a new game, play vs computer to validate move cards with the same card") do
  @game.players.map{|player| player.hand=[]}
end

Given (/^local player with cards: (\d+) of (Diamonds), (\d+) of (Spades), (J) of (Spades), (A) of (Hearts), (Q) of (Diamonds)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5|
  @game.players[0].hand << Card.new(1, card1.to_i, suit1)
  @game.players[0].hand << Card.new(1, card2.to_i, suit2)
  @game.players[0].hand << Card.new(1, card3, suit3)
  @game.players[0].hand << Card.new(1, card4, suit4)
  @game.players[0].hand << Card.new(1, card5, suit5)
end

Given (/^opponent hand with cards: (\d+) of (Spades), (J) of (Diamonds), (J) of (Clubs), (\d+) of (Diamonds), (\d+) of (Hearts)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5|
  @game.players[1].hand << Card.new(1, card1.to_i, suit1)
  @game.players[1].hand << Card.new(1, card2, suit2)
  @game.players[1].hand << Card.new(1, card3, suit3)
  @game.players[1].hand << Card.new(1, card4.to_i, suit4)
  @game.players[1].hand << Card.new(1, card5.to_i, suit5)
end

Given (/^local player with cards: (\d+) of (Spades), (\d+) of (Diamonds), (\d+) of (Clubs), (\d+) of (Spades), (Q) of (Hearts)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5|
  @game.players[0].hand << Card.new(1, card1.to_i, suit1)
  @game.players[0].hand << Card.new(1, card2.to_i, suit2)
  @game.players[0].hand << Card.new(1, card3.to_i, suit3)
  @game.players[0].hand << Card.new(1, card4.to_i, suit4)
  @game.players[0].hand << Card.new(1, card5, suit5)
end

Given (/^opponent hand with cards: (\d+) of (Clubs), (\d+) of (Hearts), (\d+) of (Clubs), (\d+) of (Clubs), (\d+) of (Hearts)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5|
  @game.players[1].hand << Card.new(1, card1.to_i, suit1)
  @game.players[1].hand << Card.new(1, card2.to_i, suit2)
  @game.players[1].hand << Card.new(1, card3.to_i, suit3)
  @game.players[1].hand << Card.new(1, card4.to_i, suit4)
  @game.players[1].hand << Card.new(1, card5.to_i, suit5)
end
