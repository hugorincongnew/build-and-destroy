Then ("the player should have the posibility to create a 4 or an 8") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1, [1,3,7])).to eq([@game.players[0].hand[0]])
end

Then ("the player should be able to get the cards grouped on the board") do
  expect(@game.group_builds_in_positions(@game.players[0].hand[0], [1, 3, 7])).to eq([[1], [3],[7]])
  expect(@game.group_builds_in_positions(@game.players[0].hand[1], [1, 3, 7])).to eq([[1, 3]])
end

Then ("the player should be able to create a build with the previous highlighted cards") do
  expect(@game.move_to_pick_up_build([1, 3, 7], @game.players[0].hand[0], @game.players[0])).to eq([[1, 3, 7], 0, "Build"])
end

Then ("the player should be able to create a build given highlighting cards for a 9") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 2, [0,2,5])).to eq([@game.players[0].hand[0]])

  expect(@game.group_builds_in_positions(@game.players[0].hand[0], [0, 2, 5])).to eq([[0], [2],[5]])

  expect(@game.move_to_pick_up_build([0, 2, 5], @game.players[0].hand[0], @game.players[0])).to eq([[0, 2, 5], 1, "Double Build"])
end

Then ("the player should be able to create a build given highlighting cards for a 10") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 6, [0,3,6])).to eq([@game.players[0].hand[0]])

  expect(@game.group_builds_in_positions(@game.players[0].hand[0], [0, 3, 6])).to eq([[0], [3],[6]])

  expect(@game.move_to_pick_up_build([0, 3, 6], @game.players[0].hand[0], @game.players[0])).to eq([[0, 3, 6], 1, "Double Build"])
end

Then ("the player should be able to create a build given highlighting cards for a 8") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 3, [0,1,2,3])).to eq([@game.players[0].hand[0]])

  expect(@game.group_builds_in_positions(@game.players[0].hand[0], [0, 1, 2, 3])).to eq([[0, 1], [2], [3]])

  expect(@game.move_to_pick_up_build([[0, 1], [2], [3]], @game.players[0].hand[0], @game.players[0])).to eq([[[0, 1], [2], [3]], 0, "Double Build"])
end

Then ("the player should be able to pick up the 9 build") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")

  expect(@game.simple_move_to_pick_up_build([0, 1], @game.players[0].hand[0], @game.players[0])).to eq([[[0, 1]], 2, "Build"])
end

Then ("the player should be able to pick up the 9 build and the 9 on the board") do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 2)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 3)).to eq("Card Placed")

  expect(@game.simple_move_to_pick_up_build([0, 1, 2, 3], @game.players[0].hand[0], @game.players[0])).to eq([[[0], [1, 3], [2]], 4, "Build"])
end

Then ("the player should be able to pick up the 9s") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 4)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 2)).to eq("Card Placed")
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 3)).to eq("Card Placed")

  expect(@game.simple_move_to_pick_up_build([0, 1, 2, 3, 4], @game.players[0].hand[0], @game.players[0])).to eq([[[0, 1, 4], [2, 3]], 5, "Double Build"])
end
