Then(/^vs available bug, the player should be able to place a card on position (\d+). (1st|2nd|3rd|4th|5th|6th) turn$/) do |position, text|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Card Placed")
end

Then(/^vs available bug, the computer should be able to place a card on position (\d+). (2nd|3rd|6th) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Card Placed")
end

Then(/^vs available bug, the computer should be able to pick up cards on position (\d+). (1st) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, position.to_i])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Cards Picked Up")
end

Then(/^vs available bug, the computer should be able to place card to build a number on position (\d+). (4th) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Building Number")
end

Then(/^vs available bug, the computer should be able to move to create a build on position (\d+). (3rd) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :computer_move_to_pick_up_build, position.to_i, [[0, 10]]])
  expect(@game.computer_move_to_pick_up_build(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Build")
end

Then(/^vs available bug, the computer should be able to create a build on position (\d+). (5th) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up_build, position.to_i])
  expect(@game.pick_up_build(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Build")
end
