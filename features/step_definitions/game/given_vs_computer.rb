Before do
  @game = Game.new([{id:'1',name:'rafael camacho',email:'rafael@gmail.com'}, {id:'2',name:'Computer'}])
  @game.players[0] = @game.local_player
  @game.players[1] = @game.opponent_player
end

#VS COMPUTER FEATURE
Given ("a new game, play vs computer") do
  @game.players.map{|player| player.hand=[]}
  @game.players[0].hand << Card.new(1, 3, "Clubs")
  @game.players[0].hand << Card.new(1, 5, "Spades")
  @game.players[0].hand << Card.new(1, 2, "Spades")
  @game.players[0].hand << Card.new(1, 7, "Spades")
  @game.players[0].hand << Card.new(1, "K", "Spades")
  @game.players[0].hand << Card.new(1, "A", "Clubs")

  @game.players[1].hand << Card.new(1, 4, "Diamonds")
  @game.players[1].hand << Card.new(1, 7, "Hearts")
  @game.players[1].hand << Card.new(1, "Q", "Hearts")
  @game.players[1].hand << Card.new(1, "J", "Hearts")
  @game.players[1].hand << Card.new(1, "K", "Hearts")
  @game.players[1].hand << Card.new(1, 5, "Hearts")
end

Given (/^new cards for each player$/) do
  @game.players[0].hand << Card.new(1, "Q", "Clubs")
  @game.players[0].hand << Card.new(1, 10, "Clubs")
  @game.players[0].hand << Card.new(1, "K", "Clubs")
  @game.players[0].hand << Card.new(1, 4, "Clubs")
  @game.players[0].hand << Card.new(1, 6, "Clubs")

  @game.players[1].hand << Card.new(1, 6, "Diamonds")
  @game.players[1].hand << Card.new(1, 2, "Diamonds")
  @game.players[1].hand << Card.new(1, 4, "Hearts")
  @game.players[1].hand << Card.new(1, 10, "Diamonds")
  @game.players[1].hand << Card.new(1, "K", "Diamonds")
end

Given(/^new cards for each player, 2nd time$/) do
  @game.players[0].hand << Card.new(1, "J", "Diamonds")
  @game.players[0].hand << Card.new(1, 8, "Spades")
  @game.players[0].hand << Card.new(1, 9, "Clubs")
  @game.players[0].hand << Card.new(1, 9, "Diamonds")
  @game.players[0].hand << Card.new(1, 3, "Spades")

  @game.players[1].hand << Card.new(1, 2, "Hearts")
  @game.players[1].hand << Card.new(1, 9, "Spades")
  @game.players[1].hand << Card.new(1, 9, "Hearts")
  @game.players[1].hand << Card.new(1, 4, "Spades")
  @game.players[1].hand << Card.new(1, "J", "Clubs")
end

Given ("a new game, play vs place card action for the computer") do
  @game.players.map{|player| player.hand=[]}
  @game.players[1].hand << Card.new(1, "J", "Hearts")
  @game.players[1].hand << Card.new(1, "A", "Spades")
  @game.players[1].hand << Card.new(1, 4, "Clubs")
  @game.players[1].hand << Card.new(1, "J", "Diamonds")
  @game.players[1].hand << Card.new(1, 10, "Hearts")
  @game.players[1].hand << Card.new(1, "Q", "Spades")

  @game.players[0].hand << Card.new(1, 8, "Spades")
  @game.players[0].hand << Card.new(1, 6, "Diamonds")
  @game.players[0].hand << Card.new(1, 10, "Diamonds")
  @game.players[0].hand << Card.new(1, 8, "Hearts")
  @game.players[0].hand << Card.new(1, 4, "Spades")
  @game.players[0].hand << Card.new(1, 3, "Hearts")
end

Given ("a new game, play vs vs create build action for the computer") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 7, "Diamonds")
  @game.players[1].hand << Card.new(1, 5, "Clubs")
  @game.players[1].hand << Card.new(1, "A", "Spades")
  @game.players[1].hand << Card.new(1, "J", "Diamonds")
  @game.players[1].hand << Card.new(1, 10, "Hearts")
  @game.players[1].hand << Card.new(1, "Q", "Spades")

  @game.players[0].hand << Card.new(1, 3, "Hearts")
  @game.players[0].hand << Card.new(1, 6, "Diamonds")
  @game.players[0].hand << Card.new(1, 10, "Diamonds")
  @game.players[0].hand << Card.new(1, 8, "Hearts")
  @game.players[0].hand << Card.new(1, 4, "Spades")
  @game.players[0].hand << Card.new(1, 8, "Spades")
end

Given ("a new game, play vs vs pick up cards action for the computer") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 4, "Hearts")
  @game.players[1].hand << Card.new(1, 2, "Diamonds")
  @game.players[1].hand << Card.new(1, 8, "Clubs")
  @game.players[1].hand << Card.new(1, "A", "Spades")
  @game.players[1].hand << Card.new(1, 9, "Diamonds")

  @game.players[0].hand << Card.new(1, 4, "Spades")
  @game.players[0].hand << Card.new(1, "J", "Diamonds")
  @game.players[0].hand << Card.new(1, "J", "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Hearts")
  @game.players[0].hand << Card.new(1, 7, "Spades")
end

Given ("a new game, play vs pick up cards action for the computer and the player") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, "J", "Diamonds")
  @game.players[1].hand << Card.new(1, "J", "Spades")
  @game.players[1].hand << Card.new(1, 6, "Diamonds")
  @game.players[1].hand << Card.new(1, 6, "Spades")
  @game.players[1].hand << Card.new(1, "J", "Clubs")
  @game.players[1].hand << Card.new(1, 7, "Clubs")

  @game.players[0].hand << Card.new(1, "K", "Hearts")
  @game.players[0].hand << Card.new(1, "K", "Spades")
  @game.players[0].hand << Card.new(1, 3, "Hearts")
  @game.players[0].hand << Card.new(1, 3, "Clubs")
  @game.players[0].hand << Card.new(1, 6, "Hearts")
  @game.players[0].hand << Card.new(1, 8, "Spades")
end

Given ("a new game, play vs create steal action for the computer") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 3, "Diamonds")
  @game.players[1].hand << Card.new(1, 2, "Hearts")
  @game.players[1].hand << Card.new(1, "Q", "Clubs")
  @game.players[1].hand << Card.new(1, "Q", "Hearts")
  @game.players[1].hand << Card.new(1, 7, "Spades")

  @game.players[0].hand << Card.new(1, 3, "Hearts")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")
  @game.players[0].hand << Card.new(1, 5, "Hearts")
  @game.players[0].hand << Card.new(1, "J", "Clubs")
  @game.players[0].hand << Card.new(1, "Q", "Diamonds")
end

Given ("a new game, play vs pick up cards for the computer") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, "K", "Hearts")
  @game.players[1].hand << Card.new(1, 3, "Diamonds")
  @game.players[1].hand << Card.new(1, 2, "Hearts")
  @game.players[1].hand << Card.new(1, 7, "Spades")
  @game.players[1].hand << Card.new(1, 5, "Hearts")

  @game.players[0].hand << Card.new(1, "K", "Diamonds")
  @game.players[0].hand << Card.new(1, "K", "Clubs")
  @game.players[0].hand << Card.new(1, "J", "Clubs")
  @game.players[0].hand << Card.new(1, 3, "Hearts")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")
end

Given ("a new game, play vs create build for the computer") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 4, "Hearts")
  @game.players[1].hand << Card.new(1, 6, "Hearts")
  @game.players[1].hand << Card.new(1, "K", "Hearts")
  @game.players[1].hand << Card.new(1, "J", "Diamonds")
  @game.players[1].hand << Card.new(1, 7, "Spades")

  @game.players[0].hand << Card.new(1, "A", "Diamonds")
  @game.players[0].hand << Card.new(1, 5, "Hearts")
  @game.players[0].hand << Card.new(1, "K", "Clubs")
  @game.players[0].hand << Card.new(1, "J", "Clubs")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")
end

Given ("cards to pick up jack cards, validate computers action on pick up") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, "A", "Diamonds")
  @game.players[1].hand << Card.new(1, "J", "Diamonds")

  @game.players[0].hand << Card.new(1, 4, "Hearts")
  @game.players[0].hand << Card.new(1, "J", "Clubs")
  @game.players[0].hand << Card.new(1, 5, "Hearts")
end

Given ("3 cards to build for the computer") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 2, "Diamonds")
  @game.players[1].hand << Card.new(1, "A", "Diamonds")
  @game.players[1].hand << Card.new(1, 10, "Diamonds")

  @game.players[0].hand << Card.new(1, 7, "Hearts")
end

Given ("a build an other same card number on the board") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 6, "Diamonds")
  @game.players[1].hand << Card.new(1, 10, "Diamonds")

  @game.players[0].hand << Card.new(1, 4, "Hearts")
  @game.players[0].hand << Card.new(1, 10, "Hearts")
end

Given ("a build on the board and two cards of the same rank of the build") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 6, "Diamonds")

  @game.players[0].hand << Card.new(1, 4, "Hearts")
  @game.players[0].hand << Card.new(1, 10, "Hearts")
  @game.players[0].hand << Card.new(1, 10, "Spades")
end

Given ("two numbers of the same rank, they can't be build") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 5, "Diamonds")
  @game.players[1].hand << Card.new(1, 10, "Diamonds")

  @game.players[0].hand << Card.new(1, 5, "Hearts")
  @game.players[0].hand << Card.new(1, 5, "Clubs")
  @game.players[0].hand << Card.new(1, 10, "Clubs")
end

Given ("two cards on the board and the computer with the same card on the hand") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 8, "Hearts")
  @game.players[1].hand << Card.new(1, 8, "Spades")

  @game.players[0].hand << Card.new(1, 8, "Clubs")
end

Given (/^an (ace|Js|Qs|Ks) for each players and other cards$/) do  |text, data|
  @game.players.map{|player| player.hand=[]}
  values = data.rows_hash

  @game.players[1].hand << Card.new(1, values[:card_1_rank], values[:card_1_suit])
  @game.players[1].hand << Card.new(1, 5, "Diamonds")

  @game.players[0].hand << Card.new(1, 2, "Hearts")
  @game.players[0].hand << Card.new(1, values[:card_2_rank], values[:card_2_suit])
end

Given ("some 8 for each players check the stack function") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, "Q", "Diamonds")
  @game.players[1].hand << Card.new(1, 8, "Diamonds")

  @game.players[0].hand << Card.new(1, 8, "Hearts")
  @game.players[0].hand << Card.new(1, 8, "Clubs")
end

Given ("some 2 for each players check the stack function") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, "Q", "Diamonds")
  @game.players[1].hand << Card.new(1, 2, "Diamonds")

  @game.players[0].hand << Card.new(1, 2, "Hearts")
  @game.players[0].hand << Card.new(1, 8, "Clubs")
end

Given ("some 5 for each players check the stack function") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, "Q", "Diamonds")
  @game.players[1].hand << Card.new(1, 5, "Diamonds")

  @game.players[0].hand << Card.new(1, 5, "Hearts")
  @game.players[0].hand << Card.new(1, 5, "Clubs")
end

Given ("two Qs for each players check the stack function") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, "Q", "Diamonds")
  @game.players[1].hand << Card.new(1, "Q", "Spades")

  @game.players[0].hand << Card.new(1, "Q", "Hearts")
  @game.players[0].hand << Card.new(1, "Q", "Clubs")
end

Given ("two numbers of the same rank, they can't be build 2") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 7, "Diamonds")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Diamonds")

  @game.players[1].hand << Card.new(1, "A", "Hearts")
  @game.players[1].hand << Card.new(1, 10, "Clubs")
end

Given ("a 9 build on the board move to pick up the 9 again") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 7, "Diamonds")
  @game.players[0].hand << Card.new(1, 2, "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Diamonds")
end

Given ("a new 9 build on the board move to pick up the 9") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 3, "Diamonds")
  @game.players[0].hand << Card.new(1, 6, "Diamonds")
  @game.players[0].hand << Card.new(1, 4, "Diamonds")
  @game.players[0].hand << Card.new(1, 5, "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Diamonds")
end


Given ("a new play is a stack or a build") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 3, "Diamonds")
  @game.players[0].hand << Card.new(1, 3, "Clubs")
  @game.players[0].hand << Card.new(1, 6, "Diamonds")
  @game.players[0].hand << Card.new(1, 3, "Hearts")
end

Given ("a build on the board, create a destroy") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 3, "Diamonds")
  @game.players[0].hand << Card.new(1, 4, "Diamonds")
  @game.players[0].hand << Card.new(1, 7, "Diamonds")

  @game.players[1].hand << Card.new(1, "A", "Hearts")
  @game.players[1].hand << Card.new(1, 8, "Clubs")
end

Given ("a stack of number check the last action") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 3, "Diamonds")
  @game.players[0].hand << Card.new(1, 3, "Spades")
  @game.players[0].hand << Card.new(1, 3, "Clubs")

  @game.players[1].hand << Card.new(1, "A", "Hearts")
  @game.players[1].hand << Card.new(1, 7, "Clubs")
end
