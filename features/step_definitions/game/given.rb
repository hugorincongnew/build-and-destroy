Before do
  @game = Game.new([{id:'1',name:'rafael camacho',email:'rafael@gmail.com'}, {id:'2',name:'Computer'}])
  @game.players = []
  @game.players << @game.local_player
  @game.players << @game.opponent_player
end

#MAIN FEATURE
Given ("a new game instance") do
end

#ADD CARD TO BOARD FEATURE
Given ("a card of the player") do
  @game.place_card_in_board(@game.players[0].hand[0], @game.players[0], 0, 1)
end

#ADD CARD TO BOARD FEATURE
Given ("cards for a simple play") do
  @game.players[1].hand=[]
  @game.players[1].hand << Card.new(1, "K", "Clubs")
  @game.players[1].hand << Card.new(1, "K", "Clubs")
end

#EXTRACT A CARD FROM BOARD FEATURE
Given ("a player's play") do
  @game.place_card_in_board(@game.players[1].hand[0], @game.players[1], 0, 1)
  @game.extract_cards_from_board(@game.players[1], 0)
end

#BUILD NUMBER FEATURE
Given ("two cards to build a number") do
  @game.players.map{|player| player.hand=[]}
  1.upto(5) do |num|
    @game.players[0].hand << Card.new(num, num + 1, "Clubs")
    @game.players[1].hand << Card.new(num, num + 1, "Hearts")
  end
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
end

Given ("two cards on the board build a number") do
  @game.place_card(@game.players[1].hand[1], @game.players[1], 1)
  @game.move_card(@game.last_card_by_position(0), @game.players[0], 1)
end

#BUILDING A NUMBER FEATURE
Given ("a builded number, extract build") do
  @game.place_card(@game.players[1].hand[1], @game.players[1], 0)
  @game.pick_up_build(@game.players[1].hand[2], @game.players[1], 0)
end

#EXTRACT ADD CARD TO PICK UP FEATURE
Given ("a card to pick up") do
  @game.players.map{|player| player.hand=[]}
  @game.players[0].hand << Card.new(1, "K", "Clubs")
  @game.players[1].hand << Card.new(2, "K", "Hearts")
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.pick_up(@game.players[1].hand[0], @game.players[1], 0)
end

#DEAL CARDS FEATURE
Given ("a new turn deal cards") do
  @game.players.map{|player| player.hand=[]}
  @game.deal_hand(5)
end

#DEAL ALL CARDS FEATURE
Given ("five turns deal all cards") do
  1.upto(5) do
    @game.players.map{|player| player.hand=[]}
    @game.deal_hand(5)
  end
end

#BUILD OR DESTROY FEATURE
Given ("two builded numbers by each players") do
  #BUILD
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)

  #DESTROY
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
  @game.place_card(@game.players[1].hand[1], @game.players[1], 1)
end

#STARTING A DOUBLE FEATURE
Given ("a build, start a second build") do
  @game.players.map{|player| player.hand=[]}
  1.upto(2) do |num|
    @game.players[0].hand << Card.new(num, num + 1, "Clubs")
    @game.players[0].hand << Card.new(num, num + 1, "Spades")
    @game.players[1].hand << Card.new(num, num + 1, "Hearts")
    @game.players[1].hand << Card.new(num, num + 1, "Diamonds")
  end
  @game.players[0].hand << Card.new(1, 4, "Spades")
  @game.players[1].hand << Card.new(1, "A", "Hearts")
  @game.players[0].hand << Card.new(1, 5, "Spades")
  @game.players[1].hand << Card.new(1, 5, "Hearts")

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[0].hand[1], @game.players[0], 0)
end

#DOUBLE BUILD FEATURE
Given ("a new card, build a double") do
  @game.place_card(@game.players[0].hand[0], @game.players[0], 1)
end

#STARTING A TRIPLE FEATURE
Given ("a double, start a third build") do
  @game.place_card(@game.players[0].hand[0], @game.players[0], 1)
  @game.place_card(@game.players[0].hand[0], @game.players[0], 1)
end

#TRIPLE BUILD FEATURE
Given ("a new card, build a triple") do
  @game.place_card(@game.players[1].hand[0], @game.players[1], 2)
end

#STARTING A QUADRUPLE FEATURE
Given ("a triple, start a quadruple build") do
  @game.place_card(@game.players[1].hand[0], @game.players[1], 2)
  @game.place_card(@game.players[1].hand[1], @game.players[1], 2)
end

#QUADRUPLE BUILD FEATURE
Given ("a new card, build a quadruple") do
  @game.place_card(@game.players[1].hand[0], @game.players[1], 3)
end

Given ("the cards moved to position 0") do
  @game.move_cards_to_position(@game.players[0].hand[1], @game.players[0], 0)
end

#STACK A NUMBER FEATURE
Given ("a card, start a stack") do
  SUITS1 = %w(Clubs Diamonds Hearts Spades)
  @game.players.map{|player| player.hand=[]}

  SUITS1.each do |suit|
    @game.players[0].hand << Card.new(1, 3, suit)
    @game.players[1].hand << Card.new(1, "K", suit)
  end
  @game.players[0].hand << Card.new(1, 4, "Spades")
  @game.players[1].hand << Card.new(1, "A", "Hearts")
  @game.players[0].hand << Card.new(1, 5, "Spades")
  @game.players[1].hand << Card.new(1, 5, "Hearts")

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
end

Given ("two cards in the same position and with the same rank") do
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
end

Given ("two facecards in the same position and with the same rank") do
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
end

Given ("a card, start a steal") do
  SUITS2 = %w(Clubs Diamonds Hearts)
  @game.players.map{|player| player.hand=[]}

  SUITS2.each do |suit|
    @game.players[0].hand << Card.new(1, 3, suit)
    @game.players[1].hand << Card.new(1, "K", suit)
  end
  @game.players[0].hand << Card.new(1, "K", "Spades")
  @game.players[1].hand << Card.new(1, 3, "Spades")

  @game.players[0].hand << Card.new(1, 4, "Spades")
  @game.players[1].hand << Card.new(1, "A", "Hearts")
  @game.players[0].hand << Card.new(1, 5, "Spades")
  @game.players[1].hand << Card.new(1, 5, "Hearts")

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
end

Given ("a facecard from the other player") do
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
end

#FINAL RESULT FEATURE
Given ("a new game without a shuffled deck") do
  @game.new_round
  @game.deck.cards = []
  RANKS3 ||= [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K"]
  SUITS3 ||= %w(Clubs Hearts)
  SUITS4 ||= %w(Diamonds Spades)
  SUITS5 ||= %w(Diamonds Spades Clubs)
  SUITS6 ||= %w(Hearts)

  RANKS3.each do |rank|
    SUITS3.each do |suit|
      @game.players[0].cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[0], 0, 1)
    end
  end

  RANKS3.each do |rank|
    SUITS4.each do |suit|
      @game.players[1].cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[1], 0, 1)
    end
  end

  RANKS5 ||= %w(A)
  RANKS5.each do |rank|
    SUITS6.each do |suit|
      @game.players[0].cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[1], 0, 1)
    end
  end

  RANKS5.each do |rank|
    SUITS5.each do |suit|
      @game.players[1].cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[1], 0, 1)
    end
  end

  @game.end_game
end

#EMPTY HANDS DEAL CARDS FEATURE
Given ("the empty hands of both players") do
  @game.players.map{|player| player.hand=[]}
  @game.deal_cards?
end

#KEEP BOARD CARDS FEATURE
Given ("a last play") do
  @game.local_player = Player.new({id:'1',name:'rafael camacho',email:'rafael@gmail.com'})
  @game.opponent_player = Player.new({id:'2',name:'hugo rincon',email:'hugo@gmail.com'})
  @game.players = []
  @game.players << @game.opponent_player
  @game.players << @game.local_player

  @game.deck.cards = []
  @game.players.map{|player| player.hand=[]}
  1.upto(4) do |num|
    @game.players[0].hand << Card.new(num, num, "Hearts")
    @game.players[1].hand << Card.new(num + 3, num + 3, "Spades")
  end
  @game.players[0].hand << Card.new(1, "K", "Spades")
  @game.players[1].hand << Card.new(1, "K", "Hearts")

  @game.place_card(@game.players[1].hand[4], @game.players[1], 0)
  @game.pick_up(@game.players[0].hand[4], @game.players[0], 0)
  @game.place_card(@game.players[0].hand[3], @game.players[0], 0)
  1.upto(3) do |num|
    @game.place_card(@game.players[0].hand[0], @game.players[0], @game.board_cards.count)
    @game.place_card(@game.players[1].hand[1], @game.players[1], @game.board_cards.count)
  end
  @game.pick_up(@game.players[1].hand[0], @game.players[1], 0)
end


Given ("a new round") do
  @game.new_round
end

#TRASH CAN FEATURE
Given ("10 games with 0 points") do
    RANKS6 = %w(2 3 4 5 6 7 8 9 10 J Q K)
    SUITS7 = %w(Clubs Hearts)
    SUITS8 = %w(Diamonds Spades)
    SUITS9 = %w(Clubs Diamonds Hearts Spades)
    RANKS10 = %w(A)
  1.upto(10) do
    @game.new_round
    @game.deck.cards = []
    RANKS6.each do |rank|
      SUITS7.each do |suit|
        @game.local_player.cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[0], 0, 1)
      end
    end

    RANKS6.each do |rank|
      SUITS8.each do |suit|
        @game.opponent_player.cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[1], 0, 1)
      end
    end

    RANKS10.each do |rank|
      SUITS9.each do |suit|
        @game.opponent_player.cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[1], 0, 1)
      end
    end
    @game.end_game
  end
end

Given ("10 winned games with most cards") do
  RANKS11 = [2, 3, 4, 5, 6, 7, 8, 9, 10, "J", "Q", "K"]
  SUITS12 = %w(Clubs Hearts)
  SUITS13 = %w(Diamonds Spades)
  SUITS14 = %w(Clubs Diamonds Hearts Spades)
  RANKS15 = %w(A)
  1.upto(10) do
    @game.new_round
    @game.deck.cards = []
    RANKS11.each do |rank|
      SUITS12.each do |suit|
        @game.opponent_player.cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[0], 0, 1)
      end
    end

    RANKS11.each do |rank|
      SUITS13.each do |suit|
        @game.local_player.cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[1], 0, 1)
      end
    end

    RANKS15.each do |rank|
      SUITS14.each do |suit|
        @game.local_player.cards_picked_up << BoardCard.new(Card.new(1, rank, suit), @game.players[1], 0, 1)
      end
    end
    @game.end_game
  end
end

#MOVE STACK CARDS
Given ("three cards of the same rank on the board, stack a number") do
  SUITS2 = %w(Clubs Diamonds Hearts Spades)
  @game.players.map{|player| player.hand=[]}

  SUITS2.each do |suit|
    @game.players[0].hand << Card.new(1, "A", suit)
    @game.players[1].hand << Card.new(1, 3, suit)
  end

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[0].hand[0], @game.players[0], 1)
  @game.place_card(@game.players[0].hand[0], @game.players[0], 2)

  @game.place_card(@game.players[1].hand[0], @game.players[1], 3)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 4)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 5)
end

#FACECARDS STACK FOUR CARDS VALIDATION
Given ("a facecard in a board position, and only two cards of the same rank on the players hand") do
  SUITS2 = %w(Clubs Diamonds Hearts)
  @game.players.map{|player| player.hand=[]}

  SUITS2.each do |suit|
    @game.players[0].hand << Card.new(1, "K", suit)
  end
  @game.players[1].hand << Card.new(1, "K", "Spades")

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
end

Given ("a build of the player") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 4, "Hearts")
  @game.players[0].hand << Card.new(1, 2, "Spades")
  @game.players[0].hand << Card.new(1, 6, "Hearts")
  @game.players[0].hand << Card.new(1, "K", "Hearts")
  @game.players[0].hand << Card.new(1, "J", "Diamonds")

  @game.players[1].hand << Card.new(1, "A", "Diamonds")
  @game.players[1].hand << Card.new(1, 2, "Spades")
  @game.players[1].hand << Card.new(1, 7, "Hearts")
  @game.players[1].hand << Card.new(1, 8, "Clubs")
  @game.players[1].hand << Card.new(1, 6, "Clubs")

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
end


Given ("cards to stack, validate computers action on stack") do
  SUITS2 = %w(Clubs Diamonds Hearts)
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 10, "Clubs")
  @game.players[1].hand << Card.new(1, 7, "Spades")
  @game.players[0].hand << Card.new(1, "Q", "Spades")

  SUITS2.each do |suit|
    @game.players[0].hand << Card.new(1, 3, suit)
  end

  @game.place_card(@game.players[0].hand[0], @game.players[0], 10)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 5)
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 8)
  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)

  @game.players[1].hand << Card.new(1, 3, "Spades")
  @game.players[1].hand << Card.new(1, "A", "Spades")
  @game.players[1].hand << Card.new(1, 8, "Spades")
  @game.players[1].hand << Card.new(1, 6, "Hearts")
  @game.players[1].hand << Card.new(1, "K", "Spades")
  @game.players[1].hand << Card.new(1, 6, "Spades")

  @game.players[0].hand << Card.new(1, "A", "Diamonds")
  @game.players[0].hand << Card.new(1, 9, "Hearts")
  @game.players[0].hand << Card.new(1, 9, "Spades")
  @game.players[0].hand << Card.new(1, 7, "Diamonds")
  @game.players[0].hand << Card.new(1, "K", "Clubs")
end

Given ("a build and a card of the same rank of the build on the board") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 2, "Spades")
  @game.players[0].hand << Card.new(1, 5, "Hearts")

  @game.players[1].hand << Card.new(1, 3, "Diamonds")
  @game.players[1].hand << Card.new(1, 5, "Spades")

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
end

Given ("two cards on the board to start a build") do
  @game.players.map{|player| player.hand=[]}

  @game.players[0].hand << Card.new(1, 2, "Spades")
  @game.players[0].hand << Card.new(1, 5, "Hearts")

  @game.players[1].hand << Card.new(1, 3, "Diamonds")

  @game.place_card(@game.players[0].hand[0], @game.players[0], 0)
  @game.place_card(@game.players[1].hand[0], @game.players[1], 1)
end
