#MAIN FEATURE
Then(/^the player instance should be a Player class$/) do
  expect(@player.class).to be(Player)
end

Then(/^the selected card should be extracted from the players hand$/) do
  expect(@player.drop_card(@player.hand[1])).to eq(2)
end

Then(/^the player should have (-?\d+) new achivements with (-?\d+) count$/) do |num1, num2|
  expect(@player.achievements.count).to eq(num1.to_i)
  @player.achievements.map { |a| expect(a.count).to eq(num2.to_i) }
end

#POINT STRUCTURE FEATURE
Then(/^the cards picked up should have spades$/) do
  expect(@player.most_spades?).to eq(1)
end

Then(/^the cards picked up should have most cards$/) do
  expect(@player.most_cards?).to eq(3)
end
