Feature: The cards should have their suit color

  Scenario: Validating a card color
    Given a card set the color
    Then the cards should have the color of their suit
