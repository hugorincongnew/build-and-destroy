begin
  require 'bundler'
  Bundler.require
rescue LoadError
end

require './libs/game.rb'

RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end