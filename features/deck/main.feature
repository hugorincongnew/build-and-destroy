Feature: Registrating a new deck

  Background:
    Given a new deck instance
    Given extract a card from the deck

  Scenario: Validating deck instance
    Then the deck instance should be a Deck class

  Scenario: Extracting card from the Deck
    Then the deck should have one less card
