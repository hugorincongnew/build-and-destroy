Feature: Registrating a new achievement

  Scenario: Validating achievement instance
    Given a new achievement instance
    Then the achievement instance should be a Achievement class
    Then the achievement coins should be 2500, goal 10, count 0, active false

    Scenario: Validating gold category instance
      Given a new gold category instance
      Then the gold category coins should be 0, goal 5000, count 0, active false
