Feature: Validating the deal of cards

  Scenario: Validating the deal of cards at a new turn
    Given five turns deal all cards
    Then the game should have a deck with 0 remaining cards
    Then the players should have 0 cards each
