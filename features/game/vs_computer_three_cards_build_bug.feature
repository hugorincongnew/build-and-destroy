Feature: Validating actions of the computer in a vs play

  Scenario: Validating three cards build with the optional cards bug
    Given a new game, play vs computer to validate a three cards build
    Given opponent hand with cards: 7 of Clubs, J of Hearts, 5 of Spades, 5 of Diamonds, Q of Diamonds and 8 of Clubs
    Given local player with cards: J of Clubs, 10 of Spades, 10 of Hearts, 6 of Clubs, Q of Hearts, 8 of Diamonds
    Then computer 1st turn should be able to place a card on position 0
    Then player 1st turn should be able to place a card on position 11
    Then computer 2nd turn should be able to pick up cards on position 11
    Then player 2nd turn should be able to place a card on position 12
    Then computer 3rd turn should be able to place a card on position 1
    Then player 3rd turn should be able to pick up cards on position 12
    Then computer 4th turn should be able to pick up cards on position 1
    Then player 4th turn should be able to place a card on position 12
    Then computer 5th turn should be able to place a card on position 1
    Then player 5th turn should be able to pick up cards on position 1
    Then computer 6th turn should be able to place a card on position 1
    Then player 6th turn should be able to pick up cards on position 1
    Given opponent hand with cards: A of Spades, 3 of Hearts and 10 of Diamonds
    Given local player with cards: 2 of Clubs
    Then computer 7th turn should be able to place a card on position 0
    Then player 7th turn should be able to place a card on position 10
    Then computer 8th turn should be able to place a card to pick up build on position 0

  Scenario: given a vs computer play check the three cards build
    Given a new game, play vs computer to validate a three cards build
    Given opponent hand with cards in a vs computer play check the three cards build
    Given local player with cards in a vs computer play check the three cards build
    Then the computer shouldn't be able to create a three cards build

  Scenario: Validating 2 of spades bug
    Given an 8 build the player shouldnt be able to create a 10 with a 2 of spades
    Then the player shouldnt be able to create a 10 on top of the 8

  Scenario: Validating 2 of spades bug
    Given an 8 build the player should be able to create a 10 with a 2 of spades
    Then the player should be able to pick up a 10 build
