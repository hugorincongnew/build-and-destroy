Feature: Creating a stack with the cards on the board or with the cards on the players hand

  Scenario: Validating starting a stack
    Given a facecard in a board position, and only two cards of the same rank on the players hand
    Then the player should be able only to pick_up
    Then the player should be able to place a card to stack by moving the cards on the board
    Then the player should be able to create a stack with the moved cards
