Feature: Creating a quadruple build with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build
    And a double, start a third build
    And a triple, start a quadruple build

  Scenario: Validating a quadruple build
    Then it should be able to place a card to start a quadruple build
    Then it should return Card Placed for the quadruple build to start
