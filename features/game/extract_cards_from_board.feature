Feature: Validating that a card should be able to be extracted from the board

  Scenario: Validating extracting a card from the board
    Given a card of the player
    And a player's play
    Then the cards should be removed from the board
    Then the player should have 2 cards picked up
