Feature: Validating actions of the computer in a vs play

    Scenario: Validating available actions for a card
    Given a new game, play vs computer to validate available actions for a card
    Given local player with cards: J of Diamonds, 9 of Clubs, 2 of Clubs, A of Spades, A of Diamonds, 8 of Clubs
    Given opponent hand with cards: J of Hearts, 4 of Spades, 6 of Clubs, 7 of Hearts, 8 of Hearts, K of Clubs
    Then vs available bug, the player should be able to place a card on position 12. 1st turn
    Then vs available bug, the computer should be able to pick up cards on position 12. 1st turn
    Then vs available bug, the player should be able to place a card on position 11. 2nd turn
    Then vs available bug, the computer should be able to place a card on position 0. 2nd turn
    Then vs available bug, the player should be able to place a card on position 10. 3rd turn
    Then vs available bug, the computer should be able to move to create a build on position 0. 3rd turn
    Then vs available bug, the player should be able to place a card on position 12. 4th turn
    Then vs available bug, the computer should be able to place card to build a number on position 12. 4th turn
    Then vs available bug, the player should be able to place a card on position 13. 5th turn
    Then vs available bug, the computer should be able to create a build on position 12. 5th turn
    Then vs available bug, the player should be able to place a card on position 6. 6th turn
    Then vs available bug, the computer should be able to place a card on position 0. 6th turn
