Feature: Validating actions of the computer in a vs play

    Scenario: Validating move cards with the same card bug
    Given a new game, play vs computer to validate move cards with the same card
    Given local player with cards: 7 of Diamonds, 6 of Spades, J of Spades, A of Hearts, Q of Diamonds
    Given opponent hand with cards: 10 of Spades, J of Diamonds, J of Clubs, 4 of Diamonds, 2 of Hearts
    Then vs move card bug, the computer should be able to place a card on position 0. 1st turn
    Then vs move card bug, the player should be able to place a card on position 12. 1st turn
    Then vs move card bug, the computer should be able to place a card on position 1. 2nd turn
    Then vs move card bug, the player should be able to place a card on position 13. 2nd turn
    Then vs move card bug, the computer should be able to pick up cards on position 1. 3rd turn
    Then vs move card bug, the player should be able to place a card on position 11. 3rd turn
    Then vs move card bug, the computer should be able to place a card on position 1. 4th turn
    Then vs move card bug, the player should be able to place a card on position 6. 4th turn
    Then vs move card bug, the computer should be able to place a card on position 2. 5th turn
    Then vs move card bug, the player should be able to place a card on position 5. 5th turn
    Given local player with cards: 3 of Spades, 5 of Diamonds, 9 of Clubs, 9 of Spades, Q of Hearts
    Given opponent hand with cards: 6 of Clubs, 9 of Hearts, 4 of Clubs, 5 of Clubs, 4 of Hearts
    Then vs move card bug, the computer should be able to place a card on position 3. 6th turn
    Then vs move card bug, the player should be able to place a card on position 7. 6th turn
    Then vs move card bug, the player should be able to place a card on position 4. 7th turn
    Then vs move card bug, the computer should be able to place a card on position 8. 7th turn
    Then vs move card bug, the computer should be able to build a number on position 6. 8th turn
    Then vs move card bug, the computer should be able to place a card on position 9. 9th turn
    Then vs move card bug, the player should be able to see the available builds with the board cards
