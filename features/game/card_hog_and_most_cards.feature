Feature: Creating the card hog and master builder achievement

  Scenario: Validating the card hog and master builder achievement
    Given 10 winned games with most cards
    Then the local player should get the card hog and master builder achievement
