Feature: Creating a quadruple build with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build
    And a double, start a third build
    And a triple, start a quadruple build
    And a new card, build a quadruple

  Scenario: Validating a quadruple build
    Then the computer should be able to build a new number for the quadruple build
    Then it should be able to build a new number for the quadruple build
    Then it should return Building Number for the quadruple build
    Then it should be able to move the cards for the quadruple build
    Then the play it should be a quadruple build
