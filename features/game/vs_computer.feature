Feature: Validating actions of the computer in a vs play

  Background: Validating actions of the computer in a vs play

  Scenario: Validating a game plays for 3 deals of cards
    Given a new game, play vs computer
    Then vs computer 1st turn
    Then vs computer 2nd turn
    Then vs computer 3rd turn
    Then vs computer 4th turn
    Then vs computer 5th turn
    Then vs computer 6th turn
    Then vs computer 6th turn 1 local player points and 0 computer points
    Given new cards for each player
    Then vs computer 7th turn
    Then vs computer 8th turn
    Then vs computer 9th turn
    Then vs computer 10th turn
    Then vs computer 11th turn
    Then vs computer 6th turn 1 local player points and 3 computer points
    Given new cards for each player, 2nd time
    Then vs computer 12th turn
    Then vs computer 13th turn
    Then vs computer 14th turn
    Then vs computer 15th turn
    Then vs computer 16th turn
    Then vs computer 6th turn 1 local player points and 3 computer points

  Scenario: Validating place card action for the computer
    Given a new game, play vs place card action for the computer
    Then vs place card action for the computer 1st turn
    Then vs vs place card action for the computer 2nd turn

  Scenario: Validating create build action for the computer
    Given a new game, play vs vs create build action for the computer
    Then vs vs create build action for the computer 1st turn
    Then vs vs create build action for the computer 2nd turn

  Scenario: Validating pick up cards action for the computer
    Given a new game, play vs vs pick up cards action for the computer
    Then vs vs pick up cards action for the computer 1st turn

  Scenario: Validating pick up cards action for the computer and the player
    Given a new game, play vs pick up cards action for the computer and the player
    Then vs pick up cards action for the computer and the player 1st turn
    Then vs pick up cards action for the computer and the player 2nd turn

  Scenario: Validating create steal action for the computer
    Given a new game, play vs create steal action for the computer
    Then vs create steal action for the computer 1st turn
    Then vs create steal action for the computer 2nd turn

  Scenario: Validating pick up cards for the computer
    Given a new game, play vs pick up cards for the computer
    Then vs pick up cards for the computer 1st turn

  Scenario: Validating create build for the computer
    Given a new game, play vs create build for the computer
    Then vs create build for the computer 1st turn
    Then vs create build for the computer 2nd turn

  Scenario: Validating stack computer bug
    Given cards to stack, validate computers action on stack
    Then the computer shouldn't be able to place a card on top of the number to stack

  Scenario: Validating pick up jack cards for the computer
    Given cards to pick up jack cards, validate computers action on pick up
    Then the player should be able to start a new build
    Then the computer should be able to pick up jacks on the board

  Scenario: Validating computers 3 cards build
    Given 3 cards to build for the computer
    Then the computer shouldn't be able to create a build with three cards

  Scenario: Validating computers build pick up cards
    Given a build an other same card number on the board
    Then the computer should be able to pick up the build and the cards on the board

  Scenario: Validating place same rank cards on build
    Given a build on the board and two cards of the same rank of the build
    Then the player should be able to place a card and pick up the build with their last card of that rank

  Scenario: Validating multiple numbers to stack
    Given two numbers of the same rank, they can't be build
    Then the player shouldn't be able to build

  Scenario: Validating multiple numbers to stack
    Given two numbers of the same rank, they can't be build 2
    Then the player shouldn't be able to build 2

  Scenario: Validating move cards with the same rank to build bug
    Given two cards on the board and the computer with the same card on the hand
    Then the computer shouln't be able to move to pick up build

  Scenario: Validating pick up ace computer bug
    Given an ace for each players and other cards
    | card_1_rank | A      |
    | card_1_suit | Spades |
    | card_2_rank | A      |
    | card_2_suit | Hearts |
    Then the player should be able to pick up the ace on the board

  Scenario: Validating pick up Js computer bug
    Given an Qs for each players and other cards
    | card_1_rank | J        |
    | card_1_suit | Spades   |
    | card_2_rank | J        |
    | card_2_suit | Diamonds |
    Then the player should be able to pick up the ace on the board

  Scenario: Validating pick up Qs computer bug
    Given an Qs for each players and other cards
    | card_1_rank | Q      |
    | card_1_suit | Clubs  |
    | card_2_rank | Q      |
    | card_2_suit | Hearts |
    Then the player should be able to pick up the ace on the board

  Scenario: Validating pick up Ks computer bug
    Given an Ks for each players and other cards
    | card_1_rank | K        |
    | card_1_suit | Diamonds |
    | card_2_rank | K        |
    | card_2_suit | Clubs    |
    Then the player should be able to pick up the ace on the board

  Scenario: Validating Stack for the computers action 8s example
    Given some 8 for each players check the stack function
    Then the computer should be able to create a stack for 8s

  Scenario: Validating Stack for the computers action 2s example
    Given some 2 for each players check the stack function
    Then the computer shouldn't see the Stacking 2's message

  Scenario: Validating Stack for the computers action 5s example
    Given some 5 for each players check the stack function
    Then the computer should be able to create a stack for 5s

  Scenario: Validating Stack for the computers action Qs example
    Given two Qs for each players check the stack function
    Then the computer should be able to create a stack for Qs

  Scenario: Validating move to pick up build bug scenario
    Given a 9 build on the board move to pick up the 9 again
    Then the computer should be able to pick up the 9 build

  Scenario: Validating move to pick up build bug scenario2
    Given a new 9 build on the board move to pick up the 9
    Then the computer should be able to pick up the two 9 builds

  Scenario: Validating stack or build functionality
    Given a new play is a stack or a build
    Then the player should be able create a stack instead of a build

  Scenario: Validating destroy on build
    Given a build on the board, create a destroy
    Then the player should be able to destroy the build

  Scenario: Validating stack of number cards
    Given a stack of number check the last action
    Then the player shouldn't be able to create a build
