Feature: Validating the deal of cards when the hands of the players are empty

  Scenario: Validating the deal of cards
    Given the empty hands of both players
    Then the players should have 5 cards each
