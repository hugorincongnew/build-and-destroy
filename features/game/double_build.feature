Feature: Creating a double build with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build
    And a new card, build a double

  Scenario: Validating a double build
    Then it should be able to build a new number
    Then it should return Building Number
    Then it should be able to move the cards for the double build
    Then it should be able to create a double build
    Then the computer should be able to place a card to start a new build for a triple
    Then it should return Double Build
