Feature: Validating actions of the computer in a vs play

    Scenario: Validating the pick ups of the computer
    Given a new game, play vs computer to validate pick ups
    Given vs pick ups, local player with cards: 9 of Hearts, 10 of Hearts, 5 of Hearts, Q of Clubs, A of Diamonds, A of Clubs
    Given vs pick ups, opponent hand with cards: A of Spades, 5 of Diamonds, 10 of Diamonds, 6 of Spades, 7 of Spades, 8 of Clubs
    Then vs pick ups bug, the player should be able to place a card on position 11. 1st turn
    Then vs pick ups bug, the computer should be able to build a number on position 11. 1st turn
    Then vs pick ups bug, the player should be able to create a Destroy on position 11. 2nd turn
    Then vs pick ups bug, the computer should be able to place a card on position 0. 2nd turn
    Then vs pick ups bug, the player should be able to pick up cards on position 0. 3rd turn
    Then vs pick ups bug, the computer should be able to place a card on position 0. 3rd turn
    Then vs pick ups bug, the player should be able to place a card on position 11. 4th turn
    Then vs pick ups bug, the computer should be able to place a card on position 1. 4th turn
    Then vs pick ups bug, the player should be able to place a card on position 10. 5th turn
    Then vs pick ups bug, the computer should be able to pick up a build by moving cards on position 1. Positions 1 and 10 for 5th turn
    Then vs pick ups bug, the computer should be able to place a card on position 2. 5th turn
    Then vs pick ups bug, the player should be able to place a card on position 12. 6th turn
    Then vs pick ups bug, the computer should be able to pick up a build by moving cards on position 10. Positions 10 and 2 for 6th turn
    Then vs pick ups bug, the computer should be able to place a card on position 3. 6th turn
    Given vs pick ups, local player with cards: 4 of Hearts, 9 of Spades, 2 of Clubs, J of Diamonds, 5 of Clubs
    Given vs pick ups, opponent hand with cards: 5 of Spades, 2 of Diamonds, 2 of Spades, 7 of Clubs
    Then vs pick ups bug, the player should be able to build a number on position 12. 7th turn
    Then vs pick ups bug, the computer should be able to create a Destroy on position 12. 7th turn
    Then vs pick ups bug, the player should be able to place a card on position 12. 8th turn
    Then vs pick ups bug, the computer should be able to pick up a build by moving cards on position 2. Positions 1, 10, 2 for 8th turn
    Then vs pick ups bug, the computer should be able to place a card on position 4. 8th turn
    Then vs pick ups bug, the player should be able to pick up cards on position 4. 9th turn
    Then vs pick ups bug, the computer should be able to pick up a build by moving cards on position 2. Positions 1, 10, 2 for 9th turn
    Then vs pick ups bug, the computer should be able to place a card on position 4. 9th turn
    Then vs pick ups bug, the player should be able to place a card on position 6. 10th turn
    Then vs pick ups bug, the computer shouldn't be able to place a card on position 5. 10th turn
