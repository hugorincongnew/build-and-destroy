Feature: Creating a triple build with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build
    And a double, start a third build

  Scenario: Validating a triple build
    Then it should be able place a card to start a triple build
    Then it should return Card Placed for the triple build to start
