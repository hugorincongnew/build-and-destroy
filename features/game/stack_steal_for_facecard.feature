Feature: Creating a steal of a stack with the cards on the board or with the cards on the players hand

  Background:
    Given a card, start a steal
    And a facecard from the other player

  Scenario: Validating a stack
    Then it should return Steal from the other player
