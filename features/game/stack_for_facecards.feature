Feature: Creating a stack with the cards on the board or with the cards on the players hand

  Background:
    Given a card, start a stack
    And two facecards in the same position and with the same rank

  Scenario: Validating starting a stack
    Then the computer should be able to stack a number for facecard 2nd card
    Then it should be able to stack a number for facecard 2nd card
    Then it should return Stacking Number for facecard 2nd card
    Then the computer should be able to stack a number for facecard 3rd card
    Then it should be able to stack a number for facecard 3rd card
    Then it should return Stacking Number for facecard 3rd card
    Then the computer should be able to create a Stack for facecard
    Then it should be able to create a Stack for facecard
    Then it should return Stack for facecard
