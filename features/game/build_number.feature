Feature: Building a number with the cards on the board or with the cards on the players hand

  Scenario: Validating a number to build
    Given two cards to build a number
    Then the sum of the cards should be on the players hand
    Then the board should have 2 cards on the same position
