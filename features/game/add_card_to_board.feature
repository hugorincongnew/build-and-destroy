Feature: Validating that a card should be able to be placed to the board

  Scenario: Validating a card on the board
    Given a card of the player
    Then the card should be placed to the board
    Then the board should have a BoardCard
    Then the board should have a card
    Then the board should have an array with one card
    Then the card on the board should be able to be moved
