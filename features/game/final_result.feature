Feature: Generating game results at the end of the game

  Scenario: Validating the result of the game according to the point structure
    Given a new game without a shuffled deck
    Then it should be no winner, and the score should be the next one
      | p0_round_score | 1  |
      | p0_total_score | 1  |
      | p1_round_score | 10 |
      | p1_total_score | 10 |
    Given a new game without a shuffled deck
    Then it should be no winner, and the score should be the next one
      | p0_round_score | 1  |
      | p0_total_score | 2  |
      | p1_round_score | 10 |
      | p1_total_score | 20 |
