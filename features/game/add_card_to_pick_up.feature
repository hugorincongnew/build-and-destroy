Feature: Validating that a card should be able to be extracted from the board

  Scenario: Validating the extract of a card from the board
    Given a card to pick up
    Then the cards should be removed from the board
    Then the player should have 2 cards picked up
