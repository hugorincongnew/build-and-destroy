Feature: Registrating a new game

  Scenario: Validating game instance
    Given a new game instance
    Then the game instance should be a Game class
    Then the game should have players
    Then the game should have two players
    Then the game should have a deck
    Then the game should have a deck with 40 remaining cards
    Then the players should have 6 cards each
