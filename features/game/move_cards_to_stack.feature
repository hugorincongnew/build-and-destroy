Feature: Stacking a number by moving the cards on the board

  Scenario: Validating a number to stack
    Given three cards of the same rank on the board, stack a number
    Then it should be able to move the cards to create a stack with facecards
    Then it should be able create a stack with the moved facecards
    Then it should be able to move the cards to create a stack with number cards
    Then it should be able create a stack with the moved number cards
