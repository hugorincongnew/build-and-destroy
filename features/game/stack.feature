Feature: Creating a stack with the cards on the board or with the cards on the players hand

  Background:
    Given a card, start a stack
    And two cards in the same position and with the same rank

  Scenario: Validating starting a stack
    Then it should be able to create a Stack
    Then it should return Stack
