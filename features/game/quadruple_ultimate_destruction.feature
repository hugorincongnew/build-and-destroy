Feature: Creating an Ultimate Destruction with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build
    And a double, start a third build
    And a triple, start a quadruple build
    And a new card, build a quadruple

  Scenario: Validating an ultimate destruction for a quadruple build
    Then it should return Building Number for the quadruple build
    Then it should be able to move the cards for the quadruple build
    Given the cards moved to position 0
    Then the computer should be able to create an Ultimate Destruction for the quadruple build
    Then the player should be able to create a quadruple build
    Then it should return Ultimate Destruction for the quadruple build
    Then the local player should have the Achievement Deluxe Devastation count increased
