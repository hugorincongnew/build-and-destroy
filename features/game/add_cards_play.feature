Feature: Validating adding cards for a simple play

  Scenario: Validating a simple play
    Given cards for a simple play
    Then it should be able to place a card
    Then the computer should be able to place a card
    Then it should return Card Placed
    Then it should be able to pick up cards
    Then the computer should be able to pick up cards
    Then it should return Cards Picked Up
    Then the player should have 2 cards picked up
